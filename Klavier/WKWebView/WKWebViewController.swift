//
//  WKWebViewController.swift
//  Klavier
//
//  Created by mengyun on 9/2/18.
//  Copyright © 2018 mengyun. All rights reserved.
//

import UIKit
import WebKit
import Alamofire

class WKWebViewController: UIViewController, WKNavigationDelegate, WKUIDelegate {
    
    var webview: WKWebView!
    var progressView: UIProgressView!
    var bottomView: UIView!
    var slider: UISlider!
    var speedValue: CGFloat = 0
    var speed: CGFloat = 0{
        didSet{
            if (timer == nil){
                return;
            }
            if (speed>0){
                timer.fireDate = NSDate.distantPast
            }
            else{
                timer.fireDate = NSDate.distantFuture
            }
        }
    }
    var timer:Timer!
    var pdfName = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        self.webview = WKWebView(frame: view.frame)
        self.webview.navigationDelegate = self;
        self.webview.uiDelegate = self;
        //开了支持滑动返回
        self.webview.allowsBackForwardNavigationGestures = true;

        // webview.load(request as URLRequest)
        self.downloadPdf()
        view.addSubview(webview)
        
        view.backgroundColor = UIColor.white
        webview.backgroundColor = UIColor.white
        
        progressView = UIProgressView(frame: CGRect(x: 0, y: (self.navigationController?.navigationBar.frame.height)!, width: view.frame.width, height: 2))
//        progressView.tintColor = UIColor.green      // 进度条颜色
//        progressView.trackTintColor = UIColor.white // 进度条背景色
        view.addSubview(progressView)
        
        //webview.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
    
        
        self.timer = Timer.scheduledTimer(timeInterval: 0.15,target:self,selector:#selector(continuousMovement), userInfo:nil,repeats:true)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tap(_:)))
        self.webview.addGestureRecognizer(tapGesture);
        
        let settingBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        settingBtn.setImage(#imageLiteral(resourceName: "set"), for: .normal)
        settingBtn.addTarget(self, action: #selector(doSetting(sender:)), for: .touchUpInside)
        let settingItem = UIBarButtonItem(customView: settingBtn)
        self.navigationItem.rightBarButtonItem = settingItem
        
        let sliderHeight:CGFloat = 30
        bottomView = UIView(frame: CGRect(x: 0, y: view.frame.size.height - sliderHeight - (isIphoneX ? 10 : 0), width: view.frame.size.width, height: sliderHeight + (isIphoneX ? 10 : 0)))
        slider = UISlider()
        slider.minimumValue = 1
        slider.maximumValue = 100
        slider.value = 10
        slider.addTarget(self,action:#selector(sliderDidchange(_:)), for:UIControl.Event.valueChanged)
        view.addSubview(bottomView)
        view.bringSubviewToFront(bottomView)
        bottomView.addSubview(slider)
        bottomView.backgroundColor = UIColor(white: 0.8, alpha: 1)
        
        let leftLabel = UILabel()
        leftLabel.text = getInternationalizedStrings(from: "slow");
        let rightLabel = UILabel()
        rightLabel.text = getInternationalizedStrings(from: "fast");
        bottomView.addSubview(leftLabel)
        bottomView.addSubview(rightLabel)
        
        leftLabel.snp.makeConstraints{(make) -> Void in
            make.top.equalToSuperview()
            make.height.equalTo(sliderHeight)
            make.left.equalToSuperview().offset(isIphoneX ? 30 : 2)
        }
        rightLabel.snp.makeConstraints{(make) -> Void in
            make.top.equalToSuperview()
            make.height.equalTo(sliderHeight)
            make.right.equalToSuperview().offset(isIphoneX ? -30 : -2)
        }
        slider.snp.makeConstraints{(make) -> Void in
            make.top.equalToSuperview()
            make.height.equalTo(sliderHeight)
            make.left.equalTo(leftLabel.snp.right).offset(2)
            make.right.equalTo(rightLabel.snp.left).offset(-2)
        }
    }
    
    @objc func continuousMovement(){
        var offset = self.webview.scrollView.contentOffset
        offset.y = offset.y + self.speed
        // print("Timer fired!", offset.y, self.webview.scrollView.contentSize.height)
        if (offset.y + self.view.frame.size.height > self.webview.scrollView.contentSize.height){
            timer.fireDate = NSDate.distantFuture
            self.speed = 0
        }
        else{
            self.webview.scrollView.setContentOffset(offset, animated: true)
        }
    }
    
    @objc func sliderDidchange(_ slider:UISlider){
        speedValue = CGFloat(slider.value)
    }
    
    @objc func doSetting(sender: UIButton?){
        bottomView.isHidden = !bottomView.isHidden
    }
    
    override func viewWillAppear(_ animated: Bool) {
        speed = 0
        bottomView.isHidden = true
    }
    
    @objc func tap(_ sender:UITapGestureRecognizer ){
        if (speed == 0){
            speed = speedValue;
        }
        else{
            speed = 0;
        }
    }
//
//    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
//
//        //  加载进度条
//        if keyPath == "estimatedProgress"{
//            progressView.progress = Float(webview.estimatedProgress)
//            progressView.isHidden = progressView.progress == 1
//        }
//    }
    
//    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
//        print("start")
//    }
//
//    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//        print("finish")
//    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false);
    }
    
    deinit {
        self.webview?.uiDelegate = nil
        self.webview?.navigationDelegate = nil
    }
    
    func downloadPdf(){
        //指定下载路径和保存文件名
        let baseUrl = "https://yunyyyun.github.io"
        let link = "\(baseUrl)/assets/pdf/\(pdfName).pdf"
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let fileURL = documentsURL.appendingPathComponent("file1/\(link)")
            //两个参数表示如果有同名文件则会覆盖，如果路径中文件夹不存在则会自动创建
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        //开始下载
        Alamofire.download(link, to: destination)
            .downloadProgress { progress in
                DispatchQueue.main.async {
                    self.progressView.progress = Float(progress.completedUnitCount)/Float(progress.totalUnitCount)
                    self.progressView.isHidden = self.progressView.progress == 1
                }
                print("已下载：\(progress.completedUnitCount/1024)KB")
                print("总大小：\(progress.totalUnitCount/1024)KB")
            }
            .responseData { response in
                if response.result.value != nil {
                    print("下载完毕!")
                    //let image2 = UIImage(data: data)
                    
                    DispatchQueue.main.async {
                        self.progressView.isHidden = true
                        if let path = response.destinationURL?.path{
                            
                            let urlStr = URL.init(fileURLWithPath:path);
                            let data = try! Data(contentsOf: urlStr)
                            self.webview.load(data, mimeType: "application/pdf", characterEncodingName: "utf-8", baseURL: URL(string: baseUrl)!)
                        }
                    }
                }
        }
        
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
