//
//  TimingTableViewCell.swift
//  PianoPlayer
//
//  Created by mengyun on 2018/1/7.
//  Copyright © 2018年 mengyun. All rights reserved.
//

import UIKit

class TimingTableViewCell: UITableViewCell {
    
    let titleLabel = UILabel()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        contentView.addSubview(titleLabel)
        titleLabel.textAlignment = .center
        self.adjustLayers()
    }
    
    //添加约束
    func adjustLayers(){
        titleLabel.snp.makeConstraints{(make) -> Void in
            make.left.right.top.bottom.equalTo(contentView)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
