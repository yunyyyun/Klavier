//
//  SetViewController.swift
//  PianoPlayer
//
//  Created by mengyun on 2017/7/9.
//  Copyright © 2017年 mengyun. All rights reserved.
//

import UIKit

class SetViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate{    
    var setView=UIView()

    var keysWidthLabel=UILabel()
    var increaseButton=UIButton()
    var resetButton=UIButton()
    var reduceButton=UIButton()
    
    //var segmentLineView0=UIView()
    var speedControllerLabel=UILabel()
    var increaseSpeedButton=UIButton()
    var resetSpeedButton=UIButton()
    var reduceSpeedButton=UIButton()
    
    var scaleControllerLabel=UILabel()
    var increaseScaleButton=UIButton()
    var resetScaleButton=UIButton()
    var reduceScaleButton=UIButton()
    
    var timingClosureButton=UIButton()
    var timingClosureImageView = UIImageView()

//    var segmentLineView1=UIView()
//    var segmentLineView2=UIView()
//    var segmentLineView3=UIView()
    var bassStaffButton=UIButton()
    var chordButton=UIButton()
    var trebleStaffButton=UIButton()
    //var segmentLineView4=UIView()
    
    var topImageView=UIImageView()
    var topSegmentLineView=UIView()
    var bottomImageView=UIImageView()
    var bottomSegmentLineView=UIView()
    
    let timeArray = [0,5,10,20,30,60,90];
    var timeTableView = UITableView()
    var gcdDelay = GCDDelay();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.white;
    
        view.addSubview(topImageView);
        view.addSubview(topSegmentLineView);
        view.addSubview(bottomImageView);
        view.addSubview(bottomSegmentLineView);
        view.addSubview(setView);
        topImageView.image = UIImage(named: "topImage")
        topSegmentLineView.backgroundColor = UIColor.darkGray
        bottomImageView.image = UIImage(named: "bottomImage")
        bottomSegmentLineView.backgroundColor = UIColor.darkGray
        
        setView.addSubview(keysWidthLabel);
        setView.addSubview(increaseButton);
        setView.addSubview(resetButton);
        setView.addSubview(reduceButton);
        setButton(btn: increaseButton, title: "+")
        setButton(btn: resetButton, title: "R")
        setButton(btn: reduceButton, title: "-")
        increaseButton.addTarget(self, action: #selector(increaseKeyWidth(sender:)), for:
            .touchUpInside)
        resetButton.addTarget(self, action: #selector(resetKeyWidth(sender:)), for:
            .touchUpInside)
        reduceButton.addTarget(self, action: #selector(reduceKeyWidth(sender:)), for:
            .touchUpInside)
        
        setView.addSubview(speedControllerLabel);
        setView.addSubview(increaseSpeedButton);
        setView.addSubview(resetSpeedButton);
        setView.addSubview(reduceSpeedButton);
        setButton(btn: increaseSpeedButton, title: "+")
        setButton(btn: resetSpeedButton, title: "R")
        setButton(btn: reduceSpeedButton, title: "-")
        increaseSpeedButton.addTarget(self, action: #selector(increaseSpeed(sender:)), for:
            .touchUpInside)
        resetSpeedButton.addTarget(self, action: #selector(resetSpeed(sender:)), for:
            .touchUpInside)
        reduceSpeedButton.addTarget(self, action: #selector(reduceSpeed(sender:)), for:
            .touchUpInside)
        
        setView.addSubview(scaleControllerLabel);
        setView.addSubview(increaseScaleButton);
        setView.addSubview(resetScaleButton);
        setView.addSubview(reduceScaleButton);
        setButton(btn: increaseScaleButton, title: "+")
        setButton(btn: resetScaleButton, title: "R")
        setButton(btn: reduceScaleButton, title: "-")
        increaseScaleButton.addTarget(self, action: #selector(increaseScale(sender:)), for:
            .touchUpInside)
        resetScaleButton.addTarget(self, action: #selector(resetScale(sender:)), for:
            .touchUpInside)
        reduceScaleButton.addTarget(self, action: #selector(reduceScale(sender:)), for:
            .touchUpInside)
        
        setView.addSubview(timingClosureButton);
        setButton(btn: timingClosureButton, title: getInternationalizedStrings(from: "timedoff"))
        timingClosureButton.contentHorizontalAlignment = .right
        timingClosureButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 4);
        timingClosureButton.addTarget(self, action: #selector(timingClosure(sender:)), for:
            .touchUpInside)
        timingClosureImageView.image = UIImage(named: "timingClosure")
        timingClosureButton.addSubview(timingClosureImageView)
        
        setView.addSubview(bassStaffButton);
        setView.addSubview(chordButton);
        setView.addSubview(trebleStaffButton);
        setButton(btn: bassStaffButton, title: getInternationalizedStrings(from: "bass"))
        setButton(btn: chordButton, title: getInternationalizedStrings(from: "chords"))
        setButton(btn: trebleStaffButton, title: getInternationalizedStrings(from: "treble"))
        bassStaffButton.addTarget(self, action: #selector(onlyBass(sender:)), for:
            .touchUpInside)
        chordButton.addTarget(self, action: #selector(chord(sender:)), for:
            .touchUpInside)
        trebleStaffButton.addTarget(self, action: #selector(onlyTreble(sender:)), for:
            .touchUpInside)
        resetButtonStatus()
        
        timeTableView.isHidden = true;
        view.addSubview(timeTableView)
        timeTableView.dataSource = self
        timeTableView.delegate = self
        
        if isIpad {
            keysWidthLabel.font = UIFont.systemFont(ofSize: 24)
            speedControllerLabel.font = UIFont.systemFont(ofSize: 24)
            scaleControllerLabel.font = UIFont.systemFont(ofSize: 24)
        }
        else{
            keysWidthLabel.font = UIFont.systemFont(ofSize: 14)
            speedControllerLabel.font = UIFont.systemFont(ofSize: 14)
            scaleControllerLabel.font = UIFont.systemFont(ofSize: 14)
        }
        
        self.adjustLayers()
    }

    func setLabelsText(){
        keysWidthLabel.text = String(format: "%@ :%d",getInternationalizedStrings(from: "keywidth"),Int(pianoVC.keyBoardView.pianoKeysWidth))
        speedControllerLabel.text = String(format: "%@ x%.1lf",getInternationalizedStrings(from: "notetime"), pianoVC.noteDuration)
        scaleControllerLabel.text = String(format: "%@ x%.2lf",getInternationalizedStrings(from: "scale"), (newPlayer?.pitchRate)!)
    }
    //添加约束
    func adjustLayers(){
        let w = self.view.frame.width
        let h = self.view.frame.height

        let minScale = drawerVC.minScale+0.015
        let segmentLineWidth:CGFloat = 2.0
        let buttonHeight:CGFloat = 36.0*(isIpad ? 2.0:1)
        // print("asdasdsads",self.view.frame.width*(1-(drawerVC.minScale+0.015))+10)
        topImageView.snp.makeConstraints{(make) -> Void in
            make.top.right.left.equalToSuperview()
            make.height.equalTo(0.5*(1-minScale)*h)
        }
        topSegmentLineView.snp.makeConstraints{(make) -> Void in
            make.right.left.equalToSuperview()
            make.top.equalTo(topImageView.snp.bottom)
            make.height.equalTo(segmentLineWidth)
        }
        bottomImageView.snp.makeConstraints{(make) -> Void in
            make.bottom.right.left.equalToSuperview()
            make.height.equalTo(0.5*(1-minScale)*h)
        }
        bottomSegmentLineView.snp.makeConstraints{(make) -> Void in
            make.right.left.equalToSuperview()
            make.bottom.equalTo(bottomImageView.snp.top)
            make.height.equalTo(segmentLineWidth)
        }
        setView.snp.makeConstraints{(make) -> Void in
            make.right.equalToSuperview()
            make.top.equalToSuperview().offset(0.5*(1-minScale)*h + segmentLineWidth)
            make.bottom.equalToSuperview().offset(0 - segmentLineWidth - 0.5*(1-minScale)*h)
            make.left.equalToSuperview().offset(w*minScale)
        }
        keysWidthLabel.snp.makeConstraints{(make) -> Void in
            make.left.top.equalToSuperview()
            //make.top.equalToSuperview().offset(0.5*(1-minScale)*h + segmentLineWidth)
            make.width.equalTo(w*(1-minScale))
            make.height.equalTo(buttonHeight)
        }
        reduceButton.snp.makeConstraints{(make) -> Void in
            make.left.equalToSuperview()
            make.top.equalTo(keysWidthLabel.snp.bottom)
            make.width.equalTo(w*(1-minScale)*0.33)
            make.height.equalTo(buttonHeight)
        }
        resetButton.snp.makeConstraints{(make) -> Void in
            make.left.equalTo(reduceButton.snp.right)
            make.top.equalTo(keysWidthLabel.snp.bottom)
            make.width.equalTo(w*(1-minScale)*0.33)
            make.height.equalTo(buttonHeight)
        }
        increaseButton.snp.makeConstraints{(make) -> Void in
            make.left.equalTo(resetButton.snp.right)
            make.top.equalTo(keysWidthLabel.snp.bottom)
            make.width.equalTo(w*(1-minScale)*0.33)
            make.height.equalTo(buttonHeight)
        }
        speedControllerLabel.snp.makeConstraints{(make) -> Void in
            make.left.equalToSuperview()
            make.top.equalTo(increaseButton.snp.bottom)
            make.width.equalTo(w*(1-minScale))
            make.height.equalTo(buttonHeight)
        }
        reduceSpeedButton.snp.makeConstraints{(make) -> Void in
            make.left.equalToSuperview()
            make.top.equalTo(speedControllerLabel.snp.bottom)
            make.width.equalTo(w*(1-minScale)*0.33)
            make.height.equalTo(buttonHeight)
        }
        resetSpeedButton.snp.makeConstraints{(make) -> Void in
            make.left.equalTo(reduceSpeedButton.snp.right)
            make.top.equalTo(speedControllerLabel.snp.bottom)
            make.width.equalTo(w*(1-minScale)*0.33)
            make.height.equalTo(buttonHeight)
        }
        increaseSpeedButton.snp.makeConstraints{(make) -> Void in
            make.left.equalTo(resetSpeedButton.snp.right)
            make.top.equalTo(speedControllerLabel.snp.bottom)
            make.width.equalTo(w*(1-minScale)*0.33)
            make.height.equalTo(buttonHeight)
        }
        
        scaleControllerLabel.snp.makeConstraints{(make) -> Void in
            make.left.equalToSuperview()
            make.top.equalTo(increaseSpeedButton.snp.bottom)
            make.width.equalTo(w*(1-minScale))
            make.height.equalTo(buttonHeight)
        }
        reduceScaleButton.snp.makeConstraints{(make) -> Void in
            make.left.equalToSuperview()
            make.top.equalTo(scaleControllerLabel.snp.bottom)
            make.width.equalTo(w*(1-minScale)*0.33)
            make.height.equalTo(buttonHeight)
        }
        resetScaleButton.snp.makeConstraints{(make) -> Void in
            make.left.equalTo(reduceScaleButton.snp.right)
            make.top.equalTo(scaleControllerLabel.snp.bottom)
            make.width.equalTo(w*(1-minScale)*0.33)
            make.height.equalTo(buttonHeight)
        }
        increaseScaleButton.snp.makeConstraints{(make) -> Void in
            make.left.equalTo(resetScaleButton.snp.right)
            make.top.equalTo(scaleControllerLabel.snp.bottom)
            make.width.equalTo(w*(1-minScale)*0.33)
            make.height.equalTo(buttonHeight)
        }
        
        bassStaffButton.snp.makeConstraints{(make) -> Void in
            make.left.equalToSuperview()
            make.bottom.equalTo(bottomSegmentLineView.snp.top).offset(-2)
            make.width.equalTo(w*(1-minScale)*0.33)
            make.height.equalTo(buttonHeight)
        }
        chordButton.snp.makeConstraints{(make) -> Void in
            make.left.equalTo(bassStaffButton.snp.right)
            make.bottom.equalTo(bottomSegmentLineView.snp.top).offset(-2)
            make.width.equalTo(w*(1-minScale)*0.33)
            make.height.equalTo(buttonHeight)
        }
        trebleStaffButton.snp.makeConstraints{(make) -> Void in
            make.left.equalTo(chordButton.snp.right)
            make.bottom.equalTo(bottomSegmentLineView.snp.top).offset(-2)
            make.width.equalTo(w*(1-minScale)*0.33)
            make.height.equalTo(buttonHeight)
        }
        
        timingClosureButton.snp.makeConstraints{(make) -> Void in
            make.left.equalToSuperview()
            make.right.equalToSuperview().offset(-1.0)
            make.bottom.equalTo(trebleStaffButton.snp.top).offset(-2)
            make.height.equalTo(buttonHeight)
        }
        timingClosureImageView.snp.makeConstraints{(make) -> Void in
            make.left.top.bottom.equalToSuperview().inset(UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 0))
            make.width.equalTo(w*(1-minScale)*0.28)
        }
        
        timeTableView.snp.makeConstraints{(make) -> Void in
            make.right.equalToSuperview()
            make.top.equalTo(topSegmentLineView.snp.bottom)
            make.bottom.equalTo(bottomImageView.snp.top)
            make.width.equalTo(w*(1-minScale))
        }
    }
    
    func setButton(btn: UIButton,title: String){
        btn.backgroundColor = UIColor.white
        btn.setTitle(title, for: .normal)
//        if isIphoneX {
//            btn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
//        }
//        else if isIpad {
//            btn.titleLabel?.font = UIFont.systemFont(ofSize: 20)
//        }
//        else{
//            btn.titleLabel?.font = UIFont.systemFont(ofSize: 10)
//        }
        if isIpad {
            btn.titleLabel?.font = UIFont.systemFont(ofSize: 20)
        }
        else{
            btn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        }
        btn.titleLabel?.adjustsFontSizeToFitWidth = true
        
        btn.setTitleColor(UIColor.black, for: .normal)
        btn.layer.masksToBounds = true;
        btn.layer.borderColor = UIColor.darkGray.cgColor;
        btn.layer.borderWidth = 1;
        btn.layer.cornerRadius = 2;
    }
    
    @objc func increaseKeyWidth(sender: UIButton?){
        keyWidthAdd(w: 2)
    }
    @objc func resetKeyWidth(sender: UIButton?){
        keyWidthAdd(w: 0)
    }
    @objc func reduceKeyWidth(sender: UIButton?){
        keyWidthAdd(w: -2)
    }
    func keyWidthAdd(w: CGFloat){
        //let drawerVC = UIApplication.shared.keyWindow?.rootViewController as! DrawerMenuController
        let pvc = drawerVC.rootViewController as! PianoViewController
        let tmpPianoKeysWidth = pvc.keyBoardView.pianoKeysWidth
        if w==0 {
            pvc.keyBoardView.pianoKeysWidth = g_defaultKeysWidth*(isIpad ? 1.5:1.0)
        }
        else{
            pvc.keyBoardView.pianoKeysWidth += w
        }
        setUDataKeysWidth()
        keysWidthLabel.text = String(format: "%@ :%d",getInternationalizedStrings(from: "keywidth"),Int(pvc.keyBoardView.pianoKeysWidth))
        pvc.keyBoardView.offsetX = pvc.keyBoardView.offsetX*CGFloat(pvc.keyBoardView.pianoKeysWidth)/CGFloat(tmpPianoKeysWidth)
    }
    
    @objc func increaseSpeed(sender: UIButton?){
        noteDurationAdd(s: 0.1)
    }
    @objc func resetSpeed(sender: UIButton?){
        noteDurationAdd(s: 0.0)
    }
    @objc func reduceSpeed(sender: UIButton?){
        noteDurationAdd(s: -0.1)
    }
    func noteDurationAdd(s: Double){
        //let drawerVC = UIApplication.shared.keyWindow?.rootViewController as! DrawerMenuController
        let pvc = drawerVC.rootViewController as! PianoViewController
        if s==0.0 {
            pvc.noteDuration = 1.0
        }
        else{
            pvc.noteDuration = pvc.noteDuration + s
        }
        speedControllerLabel.text = String(format: "%@ x%.1lf",getInternationalizedStrings(from: "notetime"), pvc.noteDuration)
    }
    
    @objc func increaseScale(sender: UIButton?){
        //newPlayer?.setPitchAddTo(0.02)
        newPlayer?.pitchRate += 0.02;
        scaleControllerLabel.text = String(format: "%@ x%.2lf",getInternationalizedStrings(from: "scale"), (newPlayer?.pitchRate)!)
    }
    @objc func resetScale(sender: UIButton?){
        newPlayer?.pitchRate = 0.0;
        scaleControllerLabel.text = String(format: "%@ x%.2lf",getInternationalizedStrings(from: "scale"), (newPlayer?.pitchRate)!)
    }
    @objc func reduceScale(sender: UIButton?){
        newPlayer?.pitchRate -= 0.02;
        scaleControllerLabel.text = String(format: "%@ x%.2lf",getInternationalizedStrings(from: "scale"), (newPlayer?.pitchRate)!)
    }
    
    @objc func onlyBass(sender: UIButton?){
        setPlayStaff(ps: .bass)
    }
    @objc func chord(sender: UIButton?){
        setPlayStaff(ps: .chord)
    }
    @objc func onlyTreble(sender: UIButton?){
        setPlayStaff(ps: .treble)
    }
    func setPlayStaff(ps: Play_Staff){
        //let drawerVC = UIApplication.shared.keyWindow?.rootViewController as! DrawerMenuController
        let pvc = drawerVC.rootViewController as! PianoViewController
        if ps == .bass {
            pvc.playBassStaff = !pvc.playBassStaff
        }
        if ps == .treble {
            pvc.playTrebleStaff = !pvc.playTrebleStaff
        }
        if ps == .chord{
            pvc.playTrebleStaff = true
            trebleStaffButton.setTitleColor(UIColor.black, for: .normal)
            pvc.playBassStaff = true
            bassStaffButton.setTitleColor(UIColor.black, for: .normal)
        }
        resetButtonStatus()
    }
    
    func resetButtonStatus(){
        if pianoVC.playTrebleStaff {
            trebleStaffButton.setTitleColor(UIColor.black, for: .normal)
        }
        else{
            trebleStaffButton.setTitleColor(UIColor.lightGray, for: .normal)
        }
        if pianoVC.playBassStaff {
            bassStaffButton.setTitleColor(UIColor.black, for: .normal)
        }
        else{
            bassStaffButton.setTitleColor(UIColor.lightGray, for: .normal)
        }
    }
    
    @objc func timingClosure(sender: UIButton?){
        timeTableView.isHidden = false;
        _ = gcdDelay.numberOfSecondsBefore - (NSDate().timeIntervalSince1970-gcdDelay.startTimeInterval);
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return timeArray.count;
    }
    
    //table的cell的高度
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return view.frame.height/CGFloat(timeArray.count+3);
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseID = "CELLID\(indexPath.row)"
        var cell = tableView.dequeueReusableCell(withIdentifier: reuseID) as? TimingTableViewCell
        if (cell == nil) {
            cell = Bundle.main.loadNibNamed("TimingTableViewCell", owner: self, options: nil)?.first as? TimingTableViewCell;
            let t = timeArray[indexPath.row]
            if t==0 {
                cell?.titleLabel.text = getInternationalizedStrings(from: "notopen");
            }
            else{
                cell?.titleLabel.text = String(format: "%d %@",t, getInternationalizedStrings(from: "minuteslater"));
            }
        }
        return cell!;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let t = timeArray[indexPath.row]
        //self.dismiss(animated: true, completion: nil);
        timeTableView.isHidden = true;
        gcdDelay.cancel();
        if t==0 {
            return;
        }
        gcdDelay.delay(numberOfSeconds: 60*t, task: {
            pianoVC.isPause = true
        });
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
