//
//  AppDelegate+E.swift
//  Klavier
//
//  Created by mengyun on 2019/3/31.
//  Copyright © 2019 mengyun. All rights reserved.
//

import Foundation
import UIKit

extension AppDelegate{
    func firstInit(){
        udata = UData();
//        drawerVC = DrawerMenuController()
//        pianoVC = PianoViewController()
//        rightVC = SetViewController()
//        pianoVCTwoHandle = PianoViewController2()
//        drawerVC.currentKeyBoardMode = 1
//        drawerVC.rightViewController = rightVC
        let nav = RootNavigationController()
        nav.viewControllers = [MainViewController()];
        self.window?.rootViewController = nav;
    }
    
    // :-1: Undefined symbol: _swift_getFieldAt
}
