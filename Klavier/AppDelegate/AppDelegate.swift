//
//  AppDelegate.swift
//  PianoPlayer
//
//  Created by mengyun on 2017/6/18.
//  Copyright © 2017年 mengyun. All rights reserved.
//

import UIKit
import CoreData
import AVFoundation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var bgTaskId: UIBackgroundTaskIdentifier?;
    var umUserInfo: [AnyHashable : Any]?
    var notiFlag = false;

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //print("the call func is",#function);
        self.window = UIWindow.init(frame: UIScreen.main.bounds);
        
        isIphoneX = iPhoneNotchScreen();
        if UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiom.pad{
            isIpad = true;
        }
        
        self.window?.backgroundColor = UIColor.black;
        self.firstInit()
        
        //drawerVC.navigationController = UINavigationController()
        self.window?.makeKeyAndVisible();
        //ALPlayer?.setPitchAddTo(0.0); //触发初始化
        newPlayer?.pitchRate = 0.0;
        if UserDefaults.standard.bool(forKey: "everLaunched") == false {
            UserDefaults.standard.set(true, forKey: "everLaunched");
            UserDefaults.standard.set(true, forKey: "firstLaunch");
        }
        else{
            UserDefaults.standard.set(false, forKey: "firstLaunch");
        }
        //drawerVC.addDebugViewText(addContent: "df")
    
        
        let userInfo: [AnyHashable : Any]? = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [AnyHashable : Any]
        if (userInfo != nil){
            let gcdDelay = GCDDelay();
            gcdDelay.delay(numberOfMilliseconds: 200, task: {
                _ = self.handleRemoteNotification(userInfo: userInfo!);
            })
        }
        
        let infoDictionary = Bundle.main.infoDictionary!
        versionStr = infoDictionary["CFBundleShortVersionString"] as? String ?? ""
        
        return true
    }
    
    func handleRemoteNotification(userInfo: Dictionary<AnyHashable, Any>) -> Bool {
        let fid = userInfo["fid"] as! String
        let copyStr: String? = userInfo["copy"] as? String
        if (copyStr != nil && copyStr!.count > 1) {
            //copyStr = copyStr + "#W3cJOL16oh"
            UIPasteboard.general.string = copyStr
            notiFlag = true
        }
        if (fid.count > 1) {
            for (index, value) in pianoVC.list.enumerated() {
                let file = value.file
                if (file == fid){
                    let gcdDelay = GCDDelay();
                    gcdDelay.delay(numberOfMilliseconds: 100, task: {
                        pianoVC.playList = [index]
                        pianoVC.loadButtonIsHidden = true
                        pianoVC.isPause = false
                        pianoVC.mIndex = index
                        if ((leftVC) != nil){
                            leftVC.dissmissKeyBoard();
                        }
                    });
                }
            }
        }
        return true;
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if let readData = NSData(contentsOfFile: url.path) {
            //如果内容存在 则用readData创建文字列
            let loadXmlData = String(data:readData as Data ,encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
            if loadXmlData.count<minXmlDataCount {
                return false
            }
            
//            let xmlDataStr = (loadXmlData as NSString).utf8String
//            let xmlDataMutablePointer:UnsafeMutablePointer = UnsafeMutablePointer(mutating: xmlDataStr!)
//            let l = LuaFunc()
//            let rlt = l?.getNoteInfo(fromXMLFile: xmlDataMutablePointer) as! [String : Any]
//            loadMusicData.unittime = rlt["unittime"] as! String
//            loadMusicData.trebleStaff = rlt["trebleStaff"]as! String
//            loadMusicData.bassStaff = rlt["bassStaff"]as! String
//            loadMusicData.url = url
            
        } else {
            //nil的话，输出空
        }
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        isBackgroundMode = true;
        UIApplication.shared.beginReceivingRemoteControlEvents();
        // 注册后台播放
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setActive(true)
            try session.setCategory(AVAudioSession.Category.playback)
        } catch {
            print(error)
        }
        bgTaskId = self.backgroundPlayerID(backTaskId: bgTaskId)
    }
    
    func backgroundPlayerID(backTaskId: UIBackgroundTaskIdentifier?)->UIBackgroundTaskIdentifier{
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setActive(true)
            try session.setCategory(AVAudioSession.Category.playback)
        } catch {
            print(error)
        }
        var newTaskId: UIBackgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(expirationHandler: nil);
        newTaskId = UIBackgroundTaskIdentifier.invalid;
        if newTaskId != UIBackgroundTaskIdentifier.invalid && backTaskId != UIBackgroundTaskIdentifier.invalid{
            UIApplication.shared.endBackgroundTask(bgTaskId!)
        }
        return newTaskId
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        if !notiFlag {
            UIPasteboard.general.string = "W3cJOL16oh"
        }
        notiFlag = false
    }

    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        print("applicationWillEnterForeground..")
        isBackgroundMode = false;
        //ALPlayer?.resume()
        newPlayer!.resume()
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setActive(true)
            try session.setCategory(AVAudioSession.Category.playback)
        } catch {
            print(error)
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        isBackgroundMode = false;
        //print("the call func is",#function);
        //drawerVC.addDebugViewText(addContent: "ba")
        if let url = loadMusicData.url {
            let pathStr = (url.absoluteString as NSString)
            let index0 = pathStr.range(of: "Inbox/")
            let index1 = pathStr.range(of: ".xml")
            let startIndex = index0.location + 6
            let noteInfoStrLen = index1.location - startIndex
            let noteInfoStr = ((url.absoluteString as NSString).substring(with: NSMakeRange(startIndex,noteInfoStrLen))).removingPercentEncoding
            
            loadMusicData.name = noteInfoStr!
            let note = Notd()
            note.name = noteInfoStr!
            note.file = noteInfoStr!
            note.unittTime = loadMusicData.unittime
            note.trebleStaffArr = (loadMusicData.trebleStaff).components(separatedBy: ",")
            note.bassStaffArr = (loadMusicData.bassStaff).components(separatedBy: ",")
            var testSoundList = [Notd]()
            testSoundList.append(note)
            if drawerVC.currentKeyBoardMode==2{
                drawerVC.currentKeyBoardMode = 1;
            }
            drawerVC.currentType = .middleMenu;
            drawerVC.g_lockScreen = false;
//            if (leftVC==nil){
//                leftVC = PalyListViewController()
//                drawerVC.leftViewController = leftVC
//                leftVC.list = testSoundList
//            }
//
            pianoVC.uploadList = testSoundList
            pianoVC.mIndex=0
            pianoVC.loadButtonIsHidden = false
            pianoVC.isPause = true
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
            return .landscapeRight//UIInterfaceOrientationMask.all
    }

    // MARK: - Core Data stack

    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "PianoPlayer")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if #available(iOS 10.0, *) {
            let context = persistentContainer.viewContext
            if context.hasChanges {
                do {
                    try context.save()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
        } else {
            // Fallback on earlier versions
        }
    }
}

