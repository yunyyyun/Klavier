//
//  MusicModel.swift
//  PianoPlayer
//  每个谱的缓存对象
//  Created by mengyun on 2017/11/1.
//  Copyright © 2017年 mengyun. All rights reserved.
//

import UIKit

class MusicData: NSObject,NSCoding{
    var name = ""
    var author = ""
    var unittime = ""
    var duration = ""
    var trebleStaff = ""
    var bassStaff = ""
    var url:URL?
    
    //MARK: -序列化
    func encode(with aCoder: NSCoder) {
        //print("？=============序列化---------------------------",self.name,self.unittime)
        aCoder.encode(self.name as String, forKey: "name")
        aCoder.encode(self.unittime as String, forKey: "unittime")
        aCoder.encode(self.duration as String, forKey: "duration")
        aCoder.encode(self.trebleStaff as String, forKey: "trebleStaff")
        aCoder.encode(self.bassStaff as String, forKey: "bassStaff")
        aCoder.encode(self.author as String, forKey: "author")
    }
    //MARK: -反序列化
    required init?(coder aDecoder: NSCoder) {
        super.init();
        guard let name = aDecoder.decodeObject(forKey: "name") as? String else { return nil }
        guard let unittime = aDecoder.decodeObject(forKey: "unittime") as? String else { return nil }
        guard let duration = aDecoder.decodeObject(forKey: "duration") as? String else { return nil }
        guard let trebleStaff = aDecoder.decodeObject(forKey: "trebleStaff") as? String else { return nil }
        guard let bassStaff = aDecoder.decodeObject(forKey: "bassStaff") as? String else { return nil }
        guard let author = aDecoder.decodeObject(forKey: "author") as? String else { return nil }
        self.name = name
        self.author = author
        self.unittime = unittime
        self.duration = duration
        self.trebleStaff = trebleStaff
        self.bassStaff = bassStaff
        //print("？=============反序列化---------------------------",self.name,self.unittime)
    }
    
    override init() {
        super.init()
    }
    
    init(name: String, author: String) {
        super.init()
        self.name = name
        self.author = author
        //self.unittime = unittime
    }
}
