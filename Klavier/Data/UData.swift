//
//  data.swift
//  PianoPlayer
//  需要保存的数据类
//  Created by mengyun on 2017/7/6.
//  Copyright © 2017年 mengyun. All rights reserved.
//

import UIKit
import HandyJSON

enum classIDType:Int {      // 
    case CLASS_ALL=0,       // 全部
    CLASS_NEW,              // 新添加
    CLASS_CHINESE,          // 华语
    CLASS_COMIC,            // 动漫
    CLASS_GAME,             // 游戏
    CLASS_CHILDREN,         // 儿歌
    CLASS_CLASSIC,          // 经典
    CLASS_ENGLISH,          // 英文
    CLASS_POPULAR,          // 流行
    CLASS_NUM               // 播放类型个数
}

struct Controll: HandyJSON{
    var staff: String = ""
    var version: String = ""
}

class Notd: HandyJSON{
    var isliked: String = ""
    var name: String = ""
    var author: String = ""
    var duration: String = "1.0"
    var file: String = ""
    var type: String = "xml"
    var iscached: String = ""
    
    var unittTime: String = ""
    var trebleStaffArr: Array = [String]()
    var bassStaffArr: Array = [String]()
    
    required init() {}
}

class UData: NSObject,NSCoding{ // 用户缓存
    
    var localData = "[{\"name\":\"Fur Elise\",\"author\":\"Ludwig van Beethoven\",\"file\":\"furElise\",\"iscached\":\"reserved\"}, {\"name\":\"Grief and Sorrow\",\"author\":\"Naruto\",\"unittime\":\"0.08\",\"file\":\"griefAndSorrow\",\"iscached\":\"reserved\"},{\"name\":\"Omake-pfadlib\",\"author\":\"Attack on Titan\",\"file\":\"Omake\",\"iscached\":\"reserved\"},{\"name\":\"青鸟\",\"author\":\"みずの よしき\",\"file\":\"qingNiao\",\"iscached\":\"reserved\"},{\"name\":\"说好的幸福呢\",\"author\":\"Jay\",\"file\":\"shuoHaoDeXinFuNe\",\"iscached\":\"reserved\"},{\"name\":\"美しき残酷な世界\",\"author\":\"Hikasa Yōko\",\"file\":\"mleckdsj\",\"iscached\":\"reserved\"},{\"name\":\"sadness and sorrow\",\"author\":\"Naruto\",\"file\":\"s_a_s\",\"iscached\":\"reserved\"},{\"name\":\"MARIAGE D'AMOUR\",\"author\":\"Richard Clayderman\",\"file\":\"mzdhl\",\"iscached\":\"reserved\",}]"
    
    // 通过外部文件打开的 list
    // var uploadList = [Note]()
    // 内存中的 list
    var localList :[Notd]
    {
        get{
//            if let notes = [Notd].deserialize(from: localData) {
//                return notes as! [Notd];
//            }
            return [];
        }
    }
    
    var serverData = "";
    // 服务器的 list
    var serverList : [Notd] = []
    
    // 全部的 list
    var list :[Notd]
    {
        get{
            // return uploadList + localList + serverList
            return localList + serverList
        }
        set{
        }
    }

    // 上次播放的文件
    var lastFile:String = ""
    var lastIndex: Int{
        get{
            for (index, value) in list.enumerated() {
                let tmp = value.file as String
                //print("ssssss_\(tmp) == \(lastFile)")
                if (tmp == lastFile){
                    return index;
                }
            }
            return 2;
        }
    }
    var version = 0; // 服务端拉取列表的版本号
    var loopMode = LoopMode.default;
    var keysWidth:CGFloat = g_defaultKeysWidth;
    var upOffset:CGFloat = -650.0  // 双手模式上面键盘的偏移
    var downOffset:CGFloat = -1100.0 // 双手模式下面键盘的偏移
    var likedArr: Set<NSString> = []  // 用户喜欢的曲子 file 集合
    
    //MARK: -序列化
    func encode(with aCoder: NSCoder) {
        print("=============序列化---------------------------",self.lastFile)
        aCoder.encode(self.lastFile as NSString, forKey: "lastFile")
        aCoder.encode(self.version as NSNumber, forKey: "version")
        aCoder.encode(self.keysWidth as NSNumber, forKey: "keysWidth")
        aCoder.encode(self.upOffset as NSNumber, forKey: "upOffset")
        aCoder.encode(self.downOffset as NSNumber, forKey: "downOffset")
        aCoder.encode(self.loopMode.rawValue as NSNumber, forKey: "loopMode")
        aCoder.encode(self.serverData as NSString, forKey: "serverData")
        aCoder.encode(self.likedArr as Set<NSString>, forKey: "likedArr")
    }
    //MARK: -反序列化
    required init?(coder aDecoder: NSCoder) {
        super.init();
        guard let lastFile = aDecoder.decodeObject(forKey: "lastFile") as? NSString else { return nil }
        guard let version = aDecoder.decodeObject(forKey: "version") as? NSNumber else { return nil }
        guard let keysWidth = aDecoder.decodeObject(forKey: "keysWidth") as? NSNumber else { return nil }
        guard let upOffset = aDecoder.decodeObject(forKey: "upOffset") as? NSNumber else { return nil }
        guard let downOffset = aDecoder.decodeObject(forKey: "downOffset") as? NSNumber else { return nil }
        guard let loopModeRawValue = aDecoder.decodeObject(forKey: "loopMode") as? NSNumber else { return nil }
        guard let serverData = aDecoder.decodeObject(forKey: "serverData") as? NSString else { return nil }
        guard let likedArr = aDecoder.decodeObject(forKey: "likedArr") as? Set<NSString> else { return nil }
        self.lastFile = lastFile as String
        self.version = version.intValue
        self.keysWidth = CGFloat(truncating: keysWidth)
        self.upOffset = CGFloat(truncating: upOffset)
        self.downOffset = CGFloat(truncating: downOffset)
        self.loopMode = LoopMode(rawValue: loopModeRawValue.intValue)!
        self.serverData = serverData as String
        if let notes = [Notd].deserialize(from: self.serverData) {
            if (self.serverList.count<1){
                self.serverList = notes as! [Notd];
            }
        }
        self.likedArr = likedArr
        self.refreshStatus();
    }
    
    // 判断是否已经缓存
    func refreshStatus(){
        let count = list.count
        for index in 0..<count {
            let file = list[index].file
            MC.getObj(file){(obj) -> () in
                if obj == nil {
                    // return false;
                }
                else{
                    // print("yes iscached \(fileName!)")
                    self.list[index].iscached = "yes"
                }
            }
        }
    }
    
    // 获取指定类型的列表
    func getListWithClass(classID :classIDType) -> [[String : Any]]{
        let tmpList = [[String : Any]]()
//        for f in uList {
//            var classFlag = Int(f.value["class"]!)
//            if classFlag==nil {
//                classFlag = 0;
//            }
//            var divNum = 1
//            for _ in 1..<(classID.rawValue) {
//                divNum = divNum*10
//            }
//            let isVailed = (classFlag!%(divNum*10))>(divNum-1)
//            if isVailed {
//                var tmp: [String : Any] = [String : Any]()
//                tmp["name"] = f.value["name"]
//                let tmpfid = f.value["file"] as String?
//                tmp["file"] = tmpfid
//                //tmp["isliked"] = f.value["isliked"]
//                var isliked = "0"
//                for noteInfo in self.musicDataMap {
//                    if noteInfo["file"] == tmp["file"] as? String {
//                        isliked = "1"
//                    }
//                }
//                tmp["isliked"] = isliked
//                self.allMusicDataMap[tmpfid!]!["isliked"]=isliked
//                tmp["author"] = f.value["author"]
//                tmp["duration"] = f.value["duration"]
//                tmpList.append(tmp);
//            }
//        }
        return tmpList
    }
    
    func getLikedNum(file: String)-> Int{
//        guard let noteInfo = allMusicDataMap[file] else { return 18; }
//        if let num = noteInfo["likedNum"] {
//            //print("\(file) getLikedNum\(num)")
//            return Int(num)!
//         }
        return 15;
    }
    
    override init() {
        super.init()
    }
}
