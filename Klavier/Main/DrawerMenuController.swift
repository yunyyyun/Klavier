//
//  Note.swift
//  PianoPlayer
//
//  Created by mengyun on 2017/6/29.
//  Copyright © 2017年 mengyun. All rights reserved.
//

import UIKit
import WebKit

enum MenuDirection {
    case leftMenu
    case rightMenu
    case middleMenu
}

@objc protocol DrawerMenuControllerDelegate : NSObjectProtocol {
    //滑动变化
    @objc optional func CustomlayoutViewWithOffset(_ xoffset:CGFloat,menuController:DrawerMenuController)
}

class DrawerMenuController: UIViewController ,UIGestureRecognizerDelegate, UITabBarControllerDelegate, CAAnimationDelegate {
     var delegate:DrawerMenuControllerDelegate?
    //是否边界影子
    var showBoundsShadow:Bool =  true {
        willSet{
        }
        didSet{
        }
    }

    var pdfVC : WKWebViewController!
    
    //是否能滑动
    var needSwipeShowMenu:Bool = true{
        willSet{
            
        }
        didSet{
            if needSwipeShowMenu{
                //self.view.addGestureRecognizer(movePan!)
            }else{
                //self.view.removeGestureRecognizer(movePan!)
            }
        }
    }
    
    var currentType:MenuDirection = .middleMenu{
        didSet{
            if currentType == .rightMenu {
                changeModelButton.isHidden = true
                lockScreenButton.isHidden = true;
                staffButton.isHidden = true;
                switchButton.isHidden = true;
                shareButton.isHidden = true;
                self.showRightViewController(true)
            }
            else if currentType == .leftMenu{
                changeModelButton.isHidden = true
                lockScreenButton.isHidden = true;
                staffButton.isHidden = true;
                switchButton.isHidden = true;
                shareButton.isHidden = true;
                self.showLeftViewController(true)
            }
            else{
                self.hideSideViewController(true)
            }
        }
    }
    
    var currentKeyBoardMode = 1{
        didSet{
//            if currentKeyBoardMode==1{
//               //  drawerVC.rootViewController = AllViewController()
//                // drawerVC.rootViewController = PianoViewController()
//                changeModelButton.setImage(#imageLiteral(resourceName: "changeMode1"), for: .normal)
//                lockScreenButton.isHidden = false;
//                staffButton.isHidden = false;
//                switchButton.isHidden = false;
//            }
//            else{
//                drawerVC.rootViewController = pianoVCTwoHandle
//                changeModelButton.setImage(#imageLiteral(resourceName: "changeMode2"), for: .normal)
//                lockScreenButton.isHidden = true;
//                staffButton.isHidden = true;
//                switchButton.isHidden = true;
//                //lockScreenButton.setImage(#imageLiteral(resourceName: "nolock"), for: .normal);
//                pianoVC.isPause=true;
//            }
            currentType = .middleMenu
        }
    }
    
    var shareButton = UIButton()
    var commentButton = UIButton()
    var changeModelButton = UIButton()
    let lockScreenButton = UIButton()
    let staffButton = UIButton()
    let switchButton = UIButton()
    var btnSize:CGFloat = 38*(isIpad ? 2.0:1);
    
    var switchIndex = 0{
        didSet{
//            if (switchIndex > (newPlayer?.soundsCategory.count)! - 1){
//                return ;
//            }
//            UserDefaults.standard.set(switchIndex, forKey: "selectedMusicalInstrumentIndex");
//            let imageName = newPlayer?.soundsCategory[switchIndex] as! String
//            switchButton.setImage(UIImage(named: imageName), for: .normal);
            // newPlayer?.categoryIndex = switchIndex;
        }
    }
    var switchView = UIView()
    
    var minScale:CGFloat = 0.8                  //root 最小缩放
    var leftViewShowWidth:CGFloat!     //左边View 长度
    var leftSmoothWidth:CGFloat = 80          //滑动是缩放后平滑距离
    var rightViewShowWidth:CGFloat = 140*(isIpad ? 2.0:1.0)        //右边
    var rightSmoothWidth:CGFloat = 80
    var directionAnimationDuration:CGFloat =  0.8        //左右滑动动画时间
    var backAnimationDuration:CGFloat =  0.45            //返回动画时间
    var bounces:Bool = true                              //滑动边界回弹
    var springBack:Bool = true  //是否支持回弹效果 object—c 只有ios7 以上有效 swift ios7以下未知 没有测试过
    var springBackDuration:CGFloat = 0.5        //回弹时间
    var SpringVelocity:CGFloat = 0.5
    
    //var movePan:UIPanGestureRecognizer?
    var blackCoverPan:UIPanGestureRecognizer?
    var blackTapPan:UITapGestureRecognizer?
    
    var startPanPoint:CGPoint = CGPoint(x: 0, y: 0)
    var panMovingRightOrLeft:Bool = false
    
    var panRight:Bool = false
    var panLeft:Bool = false
    var isInAnimation:Bool = false
    
    var menuDirection:MenuDirection = MenuDirection.middleMenu
    
    var mainContentView:UIView?
    var leftSideView:UIView?
    var rightSideView:UIView?
    var blackCoverView:UIView?
    
    var lockImage: UIImage?
    var unlockImage: UIImage?
    var shareImage: UIImage?
//    var fpsLabel:YYFPSLabel?
//    var debugView:UILabel?
    
    @objc func lockScreen(sender: UIButton?){
        if drawerVC.g_lockScreen {  // 解锁
            if currentKeyBoardMode==1 {
                drawerVC.g_lockScreen=false
                lockScreenButton.setImage(unlockImage, for: .normal);
            }
        }
        else{                       // 锁屏
            if currentKeyBoardMode==1 {
                lockScreenButton.setImage(lockImage, for: .normal);
                //let drawerVC = UIApplication.shared.keyWindow?.rootViewController as! DrawerMenuController
                _ = drawerVC.rootViewController as! PianoViewController
                drawerVC.g_lockScreen=true
                drawerVC.g_lockVC.view.frame = (UIApplication.shared.keyWindow?.frame)!
            }
        }
    }
    
    var rootViewController:UIViewController? {
        willSet{
            if (newValue != nil) {
                rootViewController?.removeFromParent()
                rootViewController?.view.removeFromSuperview();
            }
        }
        didSet{
            if (rootViewController != nil){
                //rootViewController?.delegate = self
                self.addChild(rootViewController!)
                var frame:CGRect = CGRect.zero
                let transform:CGAffineTransform  = CGAffineTransform.identity
                frame = self.view.bounds;
                mainContentView!.addSubview(rootViewController!.view)
                mainContentView!.sendSubviewToBack(rootViewController!.view)
                rootViewController!.view!.transform =  transform
                rootViewController!.view!.frame = frame
                if ((((leftViewController?.view.superview) != nil) || ((rightViewController?.view.superview) != nil)) != false) {
                    self.showShadow(showBoundsShadow)
                }
            }
        }
    }
    
    var leftViewController:UIViewController?{
        willSet{
            if (newValue != nil) {
                leftViewController?.removeFromParent()
                leftViewController?.view.removeFromSuperview();
            }
            
        }
        didSet{
            if (leftViewController != nil){
                self.addChild(leftViewController!)
                leftViewController!.view.frame = CGRect(x: 0, y: 0, width: leftViewController!.view.frame.size.width,  height: leftViewController!.view.frame.size.height)
                leftSideView!.addSubview(leftViewController!.view)
            }
        }
    }
    
    var rightViewController:UIViewController?{
        willSet{
            if (newValue != nil) {
                rightViewController?.removeFromParent()
                rightViewController?.view.removeFromSuperview();
            }
        }
        didSet{
            if (rightViewController != nil){
                self.addChild(rightViewController!)
                rightViewController!.view.frame = CGRect(x: 0, y: 0, width: rightViewController!.view.frame.size.width,  height: rightViewController!.view.frame.size.height)
                rightSideView!.addSubview(rightViewController!.view)
            }
        }
    }
    
    let g_lockVC = OcclusionViewController(nibName: "OcclusionViewController", bundle: Bundle.main)
    var g_lockScreen = false{
        didSet{
            if g_lockScreen {
                view.addSubview(g_lockVC.view)
                view.bringSubviewToFront(lockScreenButton)
            }
            else{
                g_lockVC.Unlock(UIButton());
            }
            
        }
    }
    
    var activityIndicatorView = UIActivityIndicatorView()
    
    func startAnimation() {
        //activityIndicatorView.isAnimating
        pianoVC.startAnimation()
        self.activityIndicatorView.superview?.bringSubviewToFront(self.activityIndicatorView)
        self.activityIndicatorView.startAnimating();
    }
    
    func stopAnimation() {
        DispatchQueue.main.async {
            self.activityIndicatorView.stopAnimating();
            pianoVC.stopAnimation()
        }
    }
    
    func showShadow(_ show: Bool)
    {
        mainContentView!.layer.shadowOpacity = show ? 0.6 : 0.0
        if show {
            mainContentView!.layer.cornerRadius = 12
            mainContentView!.layer.shadowOffset = CGSize.zero;
            mainContentView!.layer.shadowRadius = 8.0;
            mainContentView!.layer.shadowPath = UIBezierPath(rect: mainContentView!.bounds).cgPath
            mainContentView!.layer.shadowColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.6).cgColor
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initSubviews()
        
        blackTapPan = UITapGestureRecognizer(target: self, action: #selector(DrawerMenuController.handleSingleFingerEvent(_:)))
        blackTapPan!.numberOfTouchesRequired = 1; //手指数
        blackTapPan!.numberOfTapsRequired = 1; //tap次数
        blackTapPan!.delegate = self
        blackCoverView!.addGestureRecognizer(blackTapPan!)
        blackTapPan!.isEnabled = false
        blackCoverPan = UIPanGestureRecognizer(target: self, action: #selector(DrawerMenuController.blackCoverPanGesture(_:)))
        blackCoverPan!.delegate  = self
        blackCoverView!.addGestureRecognizer(blackCoverPan!)
        
        leftViewShowWidth = tableViewWidth;
        if isIphoneX {
            leftSmoothWidth = 60
            rightViewShowWidth = self.view.frame.width*(1-(drawerVC.minScale+0.015))+10
        }
        
        activityIndicatorView = UIActivityIndicatorView(style: .white)
        //activityIndicatorView.center = self.view.center
        activityIndicatorView.center = CGPoint(x: self.view.center.x, y: self.view.frame.height*0.1)
        //activityIndicatorView.backgroundColor = .gray;
        self.view.addSubview(activityIndicatorView);

        btnSize = 38*(isIpad ? 2.0:1);
//        self.unlockImage = UIImage(named: "unlock")
//        self.lockImage = UIImage(named: "lock")
//        self.shareImage = UIImage(named: "share")    
//        shareButton.addTarget(self, action: #selector(onShare(sender:)), for:
//            .touchUpInside)
//        shareButton.setImage(#imageLiteral(resourceName: "share"), for: .normal);
//        view.bringSubviewToFront(shareButton)
//        view.addSubview(shareButton)
//        
//        commentButton.addTarget(self, action: #selector(onComment(sender:)), for:
//            .touchUpInside)
//        commentButton.setImage(UIImage(named: "comment"), for: .normal);
//        view.bringSubviewToFront(commentButton)
//        view.addSubview(commentButton)
//        
//        staffButton.addTarget(self, action: #selector(onStaff(sender:)), for:
//            .touchUpInside)
//        staffButton.setImage(#imageLiteral(resourceName: "onstaff"), for: .normal);
//        view.bringSubviewToFront(staffButton)
//        staffButton.layer.masksToBounds = true;
//        staffButton.layer.borderColor = UIColor.darkGray.cgColor;
//        staffButton.layer.borderWidth = 1;
//        staffButton.layer.cornerRadius = 4;
//        view.addSubview(staffButton)
//        
//        switchButton.addTarget(self, action: #selector(switchInstruments(sender:)), for:
//            .touchUpInside)
//        view.bringSubviewToFront(switchButton)
//        switchButton.layer.masksToBounds = true;
//        switchButton.layer.borderColor = UIColor.darkGray.cgColor;
//        switchButton.layer.borderWidth = 1;
//        switchButton.layer.cornerRadius = 4;
//        view.addSubview(switchButton)
//        
//        changeModelButton.addTarget(self, action: #selector(changeKeyboardModel(sender:)), for:
//            .touchUpInside)
//        view.bringSubviewToFront(changeModelButton)
//        changeModelButton.layer.masksToBounds = true;
//        changeModelButton.layer.borderColor = UIColor.darkGray.cgColor;
//        changeModelButton.layer.borderWidth = 1;
//        changeModelButton.layer.cornerRadius = 4;
//        view.addSubview(changeModelButton);

//        lockScreenButton.setImage(unlockImage, for: .normal);
//        lockScreenButton.addTarget(self, action: #selector(lockScreen(sender:)), for:
//            .touchUpInside)
//        view.bringSubviewToFront(lockScreenButton)
//        view.addSubview(lockScreenButton);
//        
//        changeModelButton.snp.makeConstraints{(make) -> Void in
//            make.right.equalToSuperview().offset(isIphoneX ? -87: -60);
//            make.top.equalToSuperview().offset(2);
//            make.height.width.equalTo(btnSize)
//        }
//        switchButton.snp.makeConstraints{(make) -> Void in
//            make.right.equalTo(changeModelButton.snp.left).offset(-6);
//            make.top.equalToSuperview().offset(2);
//            make.height.width.equalTo(btnSize)
//        }
//        staffButton.snp.makeConstraints{(make) -> Void in
//            make.right.equalTo(switchButton.snp.left).offset(-6);
//            make.top.equalToSuperview().offset(2);
//            make.height.width.equalTo(btnSize)
//        }
//        
//        
//        shareButton.snp.makeConstraints{(make) -> Void in
//            make.left.equalToSuperview().offset(isIphoneX ? 87: 60);
//            make.top.equalToSuperview().offset(2);
//            make.height.width.equalTo(btnSize)
//        }
//        lockScreenButton.snp.makeConstraints{(make) -> Void in
//            make.left.equalTo(shareButton.snp.right).offset(6);
//            make.top.equalToSuperview().offset(2);
//            make.height.width.equalTo(btnSize)
//        }
//        
//        switchIndex = UserDefaults.standard.integer(forKey: "selectedMusicalInstrumentIndex")
        
//        print("view in drawvc")
        
        // 一些测试
//        debugView = UILabel();
//        debugView?.frame = CGRect(x: 0, y: 30, width: view.frame.width, height: 40)
//        view.addSubview(debugView!)
//        //debugView?.backgroundColor = .red
//        debugView?.textColor = .red
//        view.bringSubview(toFront: debugView!)
//        debugView?.text = "|appName:pianoplayer|If there is any infringement, please contact me！|"
//        debugView?.textAlignment = .center
//
//        fpsLabel = YYFPSLabel.init(frame: CGRect(x: 60, y: 0, width: 60, height: 30));
//        view.addSubview(fpsLabel!);
//        switchView = UIView(frame: self.view.frame)
//        switchView.backgroundColor = UIColor(white: 0.2, alpha: 0.8)
//        view.addSubview(switchView)
        
//        let count: Int = (newPlayer?.soundsCategory.count)!
//        let x:CGFloat = isIphoneX ? 30:0
//        let w:CGFloat = (self.view.frame.size.width-x) / CGFloat(count)
//        let h:CGFloat = w + w*0.3 + (isIphoneX ? 30:0)
//        for index in 0...count-1 {
//            //print("key \(newPlayer?.soundsCategory[index])")
//            let contentView = UIView(frame: CGRect(x: x + w*CGFloat(index) + 2, y: self.view.frame.size.height - h, width: w-4, height: h));
//            contentView.backgroundColor = UIColor.black
//            contentView.layer.cornerRadius = 3
//            contentView.layer.masksToBounds = true;
//            switchView.addSubview(contentView)
//            
//            let btn = UIButton(frame: CGRect(x: 0, y: 0, width: w-4, height: w))
//            let imageName = newPlayer?.soundsCategory[index] as! String
//            btn.setImage(UIImage(named: imageName), for: .normal);
//            btn.addTarget(self, action: #selector(switchIndex(sender:)), for:
//                .touchUpInside)
//            btn.tag = index
//            contentView.addSubview(btn)
//            
//            let titleLabel = UILabel(frame: CGRect(x: 0, y: w+1, width: w-4, height: h-w-1))
//            titleLabel.text = getInternationalizedStrings(from: imageName)
//            titleLabel.backgroundColor = .white
//            titleLabel.textAlignment = .center
//            contentView.addSubview(titleLabel)
//        }
        switchView.isHidden = true
        
        let tapPan = UITapGestureRecognizer(target: self, action: #selector(hiddenSwitchView(_:)))
        tapPan.numberOfTouchesRequired = 1; //手指数
        tapPan.numberOfTapsRequired = 1; //tap次数
        tapPan.delegate = self
        switchView.addGestureRecognizer(tapPan)
        
        // self.getSwitch()
        getUData()
    }
    
//    func getSwitch(){
//        let urlString = "\(host)/checkNet.php?&version=\(versionStr)"
//        let url: URL = URL(string: urlString)!
//        let request:NSURLRequest=NSURLRequest(url: url)
//        let session = URLSession.shared
//        let dataTask = session.dataTask(with: request as URLRequest,completionHandler: {(data, response, error) ->Void in
//            if error != nil {
//            }else{
//                guard let jsonString = String(data: data!, encoding: String.Encoding.utf8) else {
//                    return
//                }
//                if let tmp = Controll.deserialize(from: jsonString) {
//                    con = tmp
//                    DispatchQueue.main.async {
//                        if (con.staff == "staff"){
//                            self.staffButton.snp.remakeConstraints{(make) -> Void in
//                                make.right.equalTo(self.switchButton.snp.left).offset(-6);
//                                make.top.equalToSuperview().offset(2);
//                                make.height.width.equalTo(self.btnSize)
//                            }
//                        }
//                    }
//
//                }
//            }
//
//        })as URLSessionTask
//        dataTask.resume()
//        return
//    }

    @objc func hiddenSwitchView(_ sender:UITapGestureRecognizer ){
        switchView.isHidden = true
    }
    
    @objc func changeKeyboardModel(sender: UIButton?){
        if currentKeyBoardMode==1 {
            currentKeyBoardMode=2;
        }
        else{
            currentKeyBoardMode=1;
        }
    }
    
    @objc func onShare(sender: UIButton?){
        let textToShare = "Enjoy the piano music with Klavier, and with Me "
        let imageToShare = UIImage(named: "AppIcon")
        let urlToShare = NSURL.init(string: "https://itunes.apple.com/cn/app/piano-player/id1360641074?mt=8")
        let items = [textToShare,imageToShare ?? "",urlToShare ?? ""] as [Any]
        let activityVC = UIActivityViewController(
            activityItems: items,
            applicationActivities: nil)
        self.present(activityVC, animated: true, completion: { () -> Void in
        })
    }
    
    @objc func switchIndex(sender: UIButton?){
        switchIndex = (sender?.tag)!
        switchView.isHidden = true
    }
    
    @objc func switchInstruments(sender: UIButton?){
        switchView.isHidden = false
    }
    
    @objc func onComment(sender: UIButton?){
        UIApplication.shared.openURL(URL(string: iOSAppLink)!);
    }
    
    @objc func onStaff(sender: UIButton?){

        //print("pianoVC.currentSound as! String",pianoVC.currentSound["file"] as! String)
        let file = pianoVC.currentScore.file
        guard file != "" else {
            return
        }

        // print("----pianoVC.currentSound: ", pianoVC.currentSound["file"] as! String)
        if (pdfVC==nil){
            pdfVC = WKWebViewController()
            pdfVC.pdfName = file;
        }
        else{
            pdfVC.pdfName = file;
            pdfVC.downloadPdf()
        }

        pdfVC.title = pianoVC.soundName
        self.navigationController?.setNavigationBarHidden(false, animated: false);

        let transition = CATransition()
        transition.duration = 1.4
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType(rawValue: "RippleEffect")
        transition.subtype = CATransitionSubtype.fromTop
        transition.delegate = self
        self.navigationController?.view.layer.add(transition, forKey: nil)
        // pianoVC.isPause = true
        self.navigationController?.pushViewController(pdfVC, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initSubviews(){
        leftSideView = UIView(frame: self.view.bounds)
        self.view.addSubview(leftSideView!)
        rightSideView = UIView(frame: self.view.bounds)
        self.view.addSubview(rightSideView!)
        mainContentView = UIView(frame: self.view.bounds)
        self.view.addSubview(mainContentView!)
        blackCoverView = UIView(frame: self.view.bounds)
        mainContentView!.addSubview(blackCoverView!)
        blackCoverView!.backgroundColor = UIColor.black
        blackCoverView!.alpha = 0
        blackCoverView!.isHidden = true
    }
    
    func willShowLeftViewController(){
        rightSideView!.isHidden = true
        leftSideView!.isHidden = false
        self.view.sendSubviewToBack(rightSideView!)
        blackCoverView!.isHidden = false
    }
    
    func willShowRightViewController(){
        rightSideView!.isHidden = false
        leftSideView!.isHidden = true
        self.view.sendSubviewToBack(leftSideView!)
         blackCoverView!.isHidden = false
    }
  
    @objc func handleSingleFingerEvent(_ sender:UITapGestureRecognizer ){
        self.hideSideViewController(true)
    }
    
    //在遮罩view上添加滑动手势
    @objc func blackCoverPanGesture(_ sender: UIPanGestureRecognizer){
        if (blackCoverPan!.state == UIGestureRecognizer.State.began) {
            startPanPoint = mainContentView!.frame.origin
            if mainContentView!.frame.origin.x == 0 {
                self.showShadow(showBoundsShadow)
            }
        }
        let currentPostion = blackCoverPan!.translation(in: self.view)
        let xoffset:CGFloat = startPanPoint.x + currentPostion.x
        
        // 滑动判断
        if xoffset < 0 {
            if (mainContentView?.frame.origin.x)! > CGFloat(0) {
                self.hideSideViewController(true)
            }
            return
        }
        
        if xoffset > 0 {
            if (leftViewController != nil) {
                if bounces{
                    self.layoutCurrentViewWithOffset(xoffset)
                }else {
                    self.layoutCurrentViewWithOffset(leftViewShowWidth < xoffset ? leftViewShowWidth : xoffset)
                }
            }
        }
        
        if (blackCoverPan!.state == UIGestureRecognizer.State.ended) {
            if mainContentView!.frame.origin.x == 0 {
                self.showShadow(false)
            }else {
                self.hideSideViewController(true)
            }
        }
    }
   
    func showLeftViewController(_ animated:Bool){
        if ( (leftViewController == nil)) {
            leftVC = PalyListViewController()
            self.leftViewController = leftVC
        }
        //if let tmpFid = pianoVC.currentSound["file"] as? String{
            leftVC.currentPlayingFid = pianoVC.currentScore.file;
        //}
        menuDirection = MenuDirection.leftMenu
        self.willShowLeftViewController()
        var animatedTime:TimeInterval  = 0
        let showWidth:CGFloat = leftViewShowWidth
        if (animated) {
            animatedTime = Double(abs(showWidth - mainContentView!.frame.origin.x) / showWidth * directionAnimationDuration)
            
        }
        blackTapPan!.isEnabled = true
        blackCoverPan!.isEnabled = true
        isInAnimation = true
        self.showAnimationEffects(animatedTime, ShowWidth: showWidth,pletion: {  (finish:Bool) -> Void in
            self.isInAnimation = false
            })
        
        leftVC.mListTableView.reloadData();
    }
    
    func showAnimationEffects(_ animatedTime:TimeInterval,ShowWidth:CGFloat, pletion:@escaping (_ finish:Bool)->Void){
        isInAnimation = true
        let Version:NSString = UIDevice.current.systemVersion as NSString
        if springBack &&   Version.floatValue >= 7.0 &&  ShowWidth != 0{
            UIView.animate(withDuration: animatedTime , delay: 0, usingSpringWithDamping: CGFloat( springBackDuration) , initialSpringVelocity: SpringVelocity, options: UIView.AnimationOptions.allowUserInteraction, animations: {
                self.layoutCurrentViewWithOffset(ShowWidth)
                }, completion:  {
                    (finish:Bool) -> Void in
                    self.isInAnimation = false
                    pletion(finish)
                })
            
        }else {
            UIView.setAnimationCurve(UIView.AnimationCurve.easeInOut)
            UIView.animate(withDuration: animatedTime, animations: {
                self.layoutCurrentViewWithOffset(ShowWidth)
                
                }, completion: {
                    (finish:Bool) -> Void in
                    
                    if  finish{
                        pletion(finish)
                    }
                })
        }
    }
    
    func showRightViewController(_ animated:Bool){
        if ( (rightViewController == nil)) {
            rightVC = SetViewController()
            self.rightViewController = rightVC
        }
        //rightViewController?.addjustLayers()
        menuDirection = MenuDirection.rightMenu
        self.willShowRightViewController()
        var animatedTime:TimeInterval  = 0
        if (animated) {
            animatedTime = Double(abs(rightViewShowWidth + mainContentView!.frame.origin.x) / rightViewShowWidth * directionAnimationDuration)
            
        }
        blackTapPan!.isEnabled = true
        blackCoverPan!.isEnabled = true
        isInAnimation = true
        self.showAnimationEffects(animatedTime, ShowWidth: -self.rightViewShowWidth,pletion: {  (finish:Bool) -> Void in
            self.isInAnimation = false
            })
    }
    
    //返回
    func hideSideViewController(_ animated:Bool){
        self.showShadow(false)
        if ((leftVC) != nil){
            leftVC.dissmissKeyBoard()
        }
        //禁用手势，防止滑出黑框
        self.blackTapPan!.isEnabled = false
        self.blackCoverPan!.isEnabled = false
        menuDirection = MenuDirection.middleMenu
        var animatedTime:TimeInterval  = 0      //回弹动画持续时间
        if (animated) {
            //animatedTime = Double(abs( mainContentView!.frame.origin.x / (mainContentView!.frame.origin.x > 0 ? leftViewShowWidth : rightViewShowWidth ) * backAnimationDuration))
            animatedTime = Double(backAnimationDuration)
        }
        isInAnimation = true
        self.showAnimationEffects(animatedTime, ShowWidth: 0, pletion: {(finish:Bool) -> Void in
            self.isInAnimation = false
            if finish {
                self.blackCoverView!.isHidden = true
                self.blackTapPan!.isEnabled = false
                self.blackCoverPan!.isEnabled = false
                self.panLeft = false
                self.panRight = false
                self.rightSideView!.isHidden = true
                self.leftSideView!.isHidden = true
                self.changeModelButton.isHidden = false
                self.shareButton.isHidden = false
                self.staffButton.isHidden = self.currentKeyBoardMode == 2
                self.switchButton.isHidden = self.currentKeyBoardMode == 2
                self.lockScreenButton.isHidden = self.currentKeyBoardMode == 2
            }
        })
     }
    
    func layoutCurrentViewWithOffset(_ xoffset:CGFloat){
        
        if  ((delegate?.CustomlayoutViewWithOffset) != nil) {
            delegate?.CustomlayoutViewWithOffset?(xoffset,menuController: self)
            return
        }
        self.mainCurrentViewWithOffset(xoffset)
       
    }
    
    //左右滑动的方法
    func mainCurrentViewWithOffset(_ xoffset:CGFloat){
        
//        blackCoverView!.alpha = abs(xoffset/leftViewShowWidth) * 0.5
        // 需要亮一点的效果，把黑色蒙版透明度设置很低
        blackCoverView!.alpha = abs(xoffset/leftViewShowWidth) * 0.15
        
        var scale:CGFloat = 0.0
        if xoffset > 0 {
            let leftMinScale = 1 as CGFloat// 左侧滑动不缩放
            scale = 1 - abs(xoffset/(rightViewShowWidth-rightSmoothWidth)) * (1-leftMinScale)
            //scale = 1 - abs(xoffset/(leftViewShowWidth-leftSmoothWidth)) * (1-minScale)
        }else if  xoffset < 0 {
//            let rightMinScale = 1 as CGFloat// 右侧滑动不缩放
//            scale = 1 - abs(xoffset/(rightViewShowWidth-rightSmoothWidth)) * (1-rightMinScale)
            scale = 1 - abs(xoffset/(rightViewShowWidth-rightSmoothWidth)) * (1-minScale)
        }else {
            scale = 1
        }
        scale = max(scale, minScale)
        mainContentView!.transform =  CGAffineTransform.identity.scaledBy(x: scale,y: scale)
        var newFrame = CGRect(x: xoffset,  y: (self.view.frame.size.height - self.view.frame.size.height * scale )/2 , width: self.view.frame.size.width * scale, height: self.view.frame.size.height * scale)
        
        if xoffset > 0 {
            newFrame.origin.x  = xoffset
        }else if xoffset < 0 {
            newFrame.origin.x  = xoffset +  (1.0 - scale) * self.view.frame.size.width
            
        }
        mainContentView!.frame = newFrame
    }
}

