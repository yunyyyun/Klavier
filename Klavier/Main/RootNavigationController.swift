//
//  RootNavigationController.swift
//  Klavier
//
//  Created by mengyun on 2019/11/28.
//  Copyright © 2019 mengyun. All rights reserved.
//

import UIKit

class RootNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.setNavigationBarHidden(true, animated: false);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

