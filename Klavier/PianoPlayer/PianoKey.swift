//
//  PianoKey.swift
//  PianoPlayer
//
//  Created by mengyun on 2017/6/24.
//  Copyright © 2017年 mengyun. All rights reserved.
//

import UIKit

class PianoKey: UIButton {
    enum KeyType {
        case white, black
    }
    
    let margin: CGFloat = 1.0
    let keyType: KeyType?
    //let midiNoteNumber: UInt8!
    
    var soundFileID = -1;
    var soundFileName:String = "";
    
    var backgroundImage:UIImage?
    var backgroundImageHighLight:UIImage?
    
    enum KeyStates {
        case `default`, pressed
    }
    
    var keyState: KeyStates = .default
    
    init(frame: CGRect, type: KeyType) {
        self.keyType = type
        //self.midiNoteNumber = midiNoteNumber
        super.init(frame: frame)
        if type == .white{
            self.backgroundImage = #imageLiteral(resourceName: "white_up")
            self.backgroundImageHighLight = #imageLiteral(resourceName: "white_down")
            self.layer.masksToBounds = true;
            self.layer.borderColor = UIColor.black.cgColor;
        }
        if type == .black{
            self.backgroundImage = #imageLiteral(resourceName: "black_up")
            self.backgroundImageHighLight = #imageLiteral(resourceName: "black_down")
            self.layer.masksToBounds = true;
            self.layer.borderColor = UIColor.black.cgColor;
        }
        adjustsImageWhenHighlighted = false;
        isUserInteractionEnabled = false
        
        self.setImage(backgroundImage, for: .normal)
        self.setImage(backgroundImageHighLight, for: .highlighted)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        // will never call this
        self.keyType = .white
        //self.midiNoteNumber = 60
        super.init(coder: aDecoder)
    }
    
    func getPathAtMargin() -> UIBezierPath {
        // set margin property if wanted
        let cornerRadius =  CGSize(width: self.bounds.width / 10.0, height:  self.bounds.width / 10.0)
        let marginRect = CGRect(x: margin, y: margin, width: self.bounds.width - (margin * 2.0), height: self.bounds.height - (margin * 2.0))
        let path = UIBezierPath(roundedRect: marginRect, byRoundingCorners: [UIRectCorner.bottomLeft, UIRectCorner.bottomRight], cornerRadii:  cornerRadius)
        path.lineWidth = 1.0
        
        return path
    }
    
    override func draw(_ rect: CGRect) {
        // print("---------call func draw")
        let path = getPathAtMargin()
        switch keyState {
        case .default:
            if (backgroundImage != nil){
                //self.setImage(backgroundImage, for: .normal)
                self.isHighlighted = false;
            }
        case .pressed:
            if (backgroundImageHighLight != nil){
                //self.setImage(backgroundImageHighLight, for: .normal)
                self.isHighlighted = true;
            }
        }
        
//        UIColor.black.setStroke()
//        path.fill()
//        path.stroke()
        
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
    
    // MARK: - Respond to key presses
    func pressed(immediately: Bool, play: Bool = true) -> Bool {
        if play {
            self.playSoundEffect(soundName: soundFileName, immediately: immediately)
        }
        if isBackgroundMode {
            return true
        }
        if keyState != .pressed {
            keyState = .pressed
            setNeedsDisplay()
            return true
        } else {
            return false
        }
    }
    
    func stop() -> Bool {
        self.stopSoundEffect()
        return true;
    }
    
    func released() -> Bool {
        if keyState != .default {
            keyState = .default
            setNeedsDisplay()
            return true
        } else {
            return false
        }
    }

    func playSoundEffect(soundName: String, immediately: Bool){
        if soundFileID == -1 {
            soundFileID = musicParser.getfileIDByNoteString(noteString: soundName)
        }
        // newPlayer?.needPlay = true;

        newPlayer?.pushNoteToQueue(on: Int32(soundFileID), gain: 1, duration: 1)
    }
    
    func stopSoundEffect(){
        //AudioServicesDisposeSystemSoundID(soundFileObject)
        if soundFileID == -1 {
            soundFileID = musicParser.getfileIDByNoteString(noteString: soundFileName)
        }
        newPlayer?.noteOff(Int32(soundFileID))
    }
    
}









