//
//  BadgeButton.swift
//  Klavier
//  自带小红点的按钮，第一次启动时显示小红点
//  Created by mengyun on 2018/3/23.
//  Copyright © 2018年 mengyun. All rights reserved.
//

import UIKit

class BadgeButton: UIButton {
    var badgeView:UIView?
    var isShowed = false;
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    */

    override func draw(_ rect: CGRect) {
        if appIsFirstLaunch(flag: 5){ //首次启动显示小红点
            badgeView = UIView();
            badgeView?.frame = CGRect(x: self.frame.width-10, y: self.frame.height-10, width: 10, height: 10);
            badgeView?.backgroundColor = .noticeColor();
            badgeView?.layer.borderWidth=1;
            badgeView?.layer.cornerRadius = 4;
            self.addSubview(badgeView!);
            badgeView?.bringSubviewToFront(self);
            isShowed = true;
        }
        
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if self.point(inside: point, with: event){
            if isShowed{
                if let view = badgeView as UIView? {
                    view.removeFromSuperview();
                }
            }
            return self;
        }
        return nil;
    }
}
