//
//  UploadView.swift
//  PianoPlayer
//
//  Created by mengyun on 2018/3/11.
//  Copyright © 2018年 mengyun. All rights reserved.
//

import UIKit
import Alamofire

class UploadView: UIView {
    
    var titleLabel = UILabel()
    var closeButton = UIButton()
    var nameTextFiled = UITextField()
    var authorTextFiled = UITextField()
    var briefIntroductionTextFiled = UITextField()
    var uploadButton = UIButton()
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame);
        self.backgroundColor = UIColor.darkBlueColor();
        self.layer.cornerRadius = 4
        self.layer.masksToBounds = true
        nameTextFiled.backgroundColor = .white;
        authorTextFiled.backgroundColor = .white;
        briefIntroductionTextFiled.backgroundColor = .white;
        titleLabel.text = getInternationalizedStrings(from: "addanewmusicscore")
        titleLabel.textAlignment = .center
        nameTextFiled.placeholder = "name";
        authorTextFiled.placeholder = "author";
        briefIntroductionTextFiled.placeholder = "brief introduction";
        uploadButton.setTitle(getInternationalizedStrings(from: "confrim"), for: .normal)
        uploadButton.setTitleColor(.blue, for: .normal);
        uploadButton.titleLabel?.textAlignment = .center
        closeButton.setTitle("x", for: .normal)
        closeButton.setTitleColor(.red, for: .normal);
        
        nameTextFiled.autocorrectionType = .no;
        authorTextFiled.autocorrectionType = .no;
        briefIntroductionTextFiled.autocorrectionType = .no;

        self.addSubview(titleLabel)
        self.addSubview(closeButton)
        self.addSubview(nameTextFiled)
        self.addSubview(authorTextFiled)
        self.addSubview(briefIntroductionTextFiled)
        self.addSubview(uploadButton)
        //return self;
    }
    
    override func layoutSubviews() {
        let width = frame.width;
        let height = frame.height;
        titleLabel.frame                    = CGRect(x: 4, y:0 ,            width: width-8, height: height*0.18)
        closeButton.frame                   = CGRect(x: width-33, y:0 ,     width: 30, height: 30)
        nameTextFiled.frame                 = CGRect(x: 4, y:height*0.2 ,   width: width-8, height: height*0.18)
        authorTextFiled.frame               = CGRect(x: 4, y:height*0.4 ,   width: width-8, height: height*0.18)
        briefIntroductionTextFiled.frame    = CGRect(x: 4, y:height*0.6 ,   width: width-8, height: height*0.18)
        uploadButton.frame                  = CGRect(x: 4, y:height*0.8 ,   width: width-8, height: height*0.18)
        uploadButton.addTarget(self, action: #selector(upload), for: .touchUpInside)
        closeButton.addTarget(self, action: #selector(close), for: .touchUpInside)
    }
    
    @objc func upload() {
        loadMusicData.name = nameTextFiled.text!
        loadMusicData.author = authorTextFiled.text!
        let fid = loadMusicData.name
        let name = loadMusicData.name
        let author = loadMusicData.author
        MC.saveObj(fid, value: loadMusicData)
        //udata.musicDataMap[fid] = name
        let newNote = Notd()
        newNote.file = fid
        newNote.name = name
        newNote.author = author
        newNote.iscached = "yes"
        // udata.uploadList.insert(newNote, at: 0)
        
        saveUData()
        // leftVC.mListTableView.reloadData()
        self.uploadXMl()
        self.close()
    }
    
    func uploadXMl(){
        let urlString = "\(host)/reciveFile.php&version=\(versionStr)"
        let fileName = "\(loadMusicData.name).xml"
        let xmlData = try! Data(contentsOf: loadMusicData.url!)

        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(xmlData, withName: "file", fileName: fileName, mimeType: "text/xml")
        },
            to: urlString,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        debugPrint(response)
                    }
                case .failure(let encodingError):
                    print(encodingError)
                }
        })
    }
    
    @objc func close() {
        self.endEditing(true)
        pianoVC.uploadView.isHidden = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
