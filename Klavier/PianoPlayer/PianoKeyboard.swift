//
//  PianoKeyboard.swift
//  PianoPlayer
//
//  Created by mengyun on 2017/6/24.
//  Copyright © 2017年 mengyun. All rights reserved.
//

import UIKit

class PianoKeyboard: UIView {
    
    var isAutoPlay = false // 在自动播放时不发声音也不stop声音
    var pianoKeysWhite = [PianoKey]()
    var pianoKeysBlack = [PianoKey]()
    var pressedKeys1 = Set<PianoKey>() //  自动播放的staff1 按键
    var pressedKeys2 = Set<PianoKey>() //  自动播放的staff2 按键
    var pressedKeys = Set<PianoKey>()
    var pianoKeysWidth:CGFloat=g_defaultKeysWidth{
        didSet{
            if pianoKeysWidth<20{
                pianoKeysWidth = 20
            }
            if pianoKeysWidth>100{
                pianoKeysWidth = 100
            }
            let frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: pianoKeysWidth*52, height: self.frame.height)
            self.createKeysWithWidth(frame: frame, pianoKeysWidth: pianoKeysWidth, offSet: offsetX)
            pianoVC.setKeyboardSlider()
        }
    }
    var offsetX:CGFloat=0{  //播放时，自动移动键盘，确保显示正在按的键
        didSet{
            let y = self.frame.origin.y
            let w = self.frame.width
            let h = self.frame.height
            if offsetX>0 {
                offsetX = 0
            }
            else{
                var boundsLength = UIScreen.main.bounds.size.width
                //boundsLength取整个UIScreen尺寸的长，由于转屏幕有延长，有可能取得宽，这里认为纠正一下
                if boundsLength<UIScreen.main.bounds.size.height {
                    boundsLength = UIScreen.main.bounds.size.height
                }
                if  boundsLength-offsetX > w {
                    offsetX = boundsLength - CGFloat(w)
                }
            }
            self.frame = CGRect(x: offsetX, y: y, width: w, height: h)
            // self.updateThumdsImage()
        }
    }

    let soundsWhite = ["a0","b0",
                  "c1","d1","e1","f1","g1","a1","b1",
                  "c2","d2","e2","f2","g2","a2","b2",
                  "c3","d3","e3","f3","g3","a3","b3",
                  "c4","d4","e4","f4","g4","a4","b4",
                  "c5","d5","e5","f5","g5","a5","b5",
                  "c6","d6","e6","f6","g6","a6","b6",
                  "c7","d7","e7","f7","g7","a7","b7",
                  "c8"]
    let soundsBlack = ["a0m",
                       "c1m","d1m","f1m","g1m","a1m",
                       "c2m","d2m","f2m","g2m","a2m",
                       "c3m","d3m","f3m","g3m","a3m",
                       "c4m","d4m","f4m","g4m","a4m",
                       "c5m","d5m","f5m","g5m","a5m",
                       "c6m","d6m","f6m","g6m","a6m",
                       "c7m","d7m","f7m","g7m","a7m"]
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.black
        isMultipleTouchEnabled = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func clearAllKeys(){
        for view in self.subviews {
            view.removeFromSuperview()
        }
        pianoKeysWhite = [PianoKey]()
        pianoKeysBlack = [PianoKey]()
        pressedKeys = Set<PianoKey>()
    }
    
    func createKeysWithWidth(frame: CGRect, pianoKeysWidth: CGFloat,offSet: CGFloat) {
        self.clearAllKeys()
        self.frame = frame;
        self.offsetX = offSet;
        
        let height = self.frame.height
        //print("------self.frame.width",self.frame.width)
        let whiteNum = 52;
        let pianoKeysHeight = height;
        
        let pianoBlackKeysWidth = pianoKeysWidth*0.8;
        let pianoBlackKeysHeight:CGFloat = height*0.6;
        var indexBlack = 0;
        
        for index in 0...whiteNum-1 {
            let xPos:CGFloat = CGFloat(index)*pianoKeysWidth
            let yPos:CGFloat = 0;
            let buttonWhite = PianoKey(frame: CGRect(x: xPos, y: yPos, width: pianoKeysWidth, height: pianoKeysHeight),type: .white);
            //buttonWhite.setImage(UIImage(named:"keyWhite"), for: .normal)
            buttonWhite.tag = 100 + index;
            //print("index:",index,buttonWhite.tag)
            buttonWhite.soundFileName = (soundsWhite[index] as NSString) as String;
            pianoKeysWhite.append(buttonWhite)
            self.addSubview(buttonWhite);
            
            
            if soundsWhite[index]=="c1" ||
                soundsWhite[index]=="c2" ||
                soundsWhite[index]=="c3" ||
                soundsWhite[index]=="c4" ||
                soundsWhite[index]=="c5" ||
                soundsWhite[index]=="c6" ||
                soundsWhite[index]=="c7" ||
                //soundsWhite[index] != "c8" ||
                soundsWhite[index]=="c8" {
            
                let labelC = UILabel(frame: CGRect(x: 0, y: pianoKeysHeight-pianoKeysWidth, width: pianoKeysWidth, height: pianoKeysWidth))
                labelC.text = soundsWhite[index].capitalized
                labelC.textColor = UIColor.gray
                labelC.textAlignment = .center
                let size:CGFloat = 10 + 0.2*(pianoKeysWidth-20.0)
                labelC.font = UIFont(name:"TimesNewRomanPSMT", size:CGFloat(size))
                buttonWhite.addSubview(labelC);
            }
            
            if (index == 1 || (index-3)%7 == 0 || (index-4)%7 == 0 || (index-6)%7 == 0 || (index-7)%7 == 0 || (index-8)%7 == 0) && (index != 0){
                let xPos:CGFloat = CGFloat(index)*pianoKeysWidth - pianoBlackKeysWidth/2
                let buttonBlack = PianoKey(frame: CGRect(x: xPos, y: 1, width: pianoBlackKeysWidth, height: pianoBlackKeysHeight), type: .black);
                //buttonBlack.setImage(UIImage(named:"keyBlack"), for: .normal)
                buttonBlack.tag = 152 + indexBlack;
                buttonBlack.soundFileName = soundsBlack[indexBlack];
                //print("indexBlack:",indexBlack,buttonBlack.tag)
                indexBlack = indexBlack+1
                buttonBlack.bringSubviewToFront(self)
                pianoKeysBlack.append(buttonBlack)
                self.addSubview(buttonBlack);
                
            }
        }
    }
    
    func getBtn(soundName:String) ->PianoKey! {
        for v in self.subviews {
            if let key = v as? PianoKey, key.soundFileName == soundName {
                return key
            }
        }
        return nil//PianoKey(frame: CGRect(), type: .white)
    }
    
    //MARK: - 单个按键点击的保留方法，现弃用
    @objc func press(sender: UIButton?){
        let btn:PianoKey = sender as! PianoKey
        if btn.pressed(immediately: true){
            //print("pressed:",btn.soundFileName)
        }
    }
    @objc func release(sender: UIButton?){
        let btn:PianoKey = sender as! PianoKey
        if btn.released(){
            //print("released:",btn.soundFileName)
        }
    }
    
    //当前手指下的音键
    fileprivate func getKeyFromLocation(_ loc: CGPoint) -> PianoKey? {
        var selection: PianoKey?
        var selectedKeys = [PianoKey]()
        for key in pianoKeysWhite {
            if key.frame.contains(loc) {
                selectedKeys.append(key)
            }
        }
        for key in pianoKeysBlack {
            if key.frame.contains(loc) {
                selectedKeys.append(key)
            }
        }
        
        // if only one key, must be white
        if selectedKeys.count == 1 {
            selection = selectedKeys[0]
        } else {
            // if multiple keys (b/c keys overlap white), only select black key
            for key in selectedKeys {
                if key.keyType == PianoKey.KeyType.black {
                    selection = key
                    break
                }
            }
        }
        //print("-------",selection?.soundFileName,pianoKeysBlack.count,pianoKeysWhite.count,selectedKeys.count)
        return selection
    }
    
    // 获取被选中的音键集
    fileprivate func getNoteSetFromTouches(_ touches: Set<UITouch>) -> Set<PianoKey> {
        var touchedKeys = Set<PianoKey>()
        for touch in touches {
            if let key = getKeyFromLocation(touch.location(in: self)) {
                touchedKeys.insert(key)
            }
        }
        return touchedKeys
    }
    
    fileprivate func refreshTouches(_ touches: Set<UITouch>) {
        let notesFromTouches:Set<PianoKey> = getNoteSetFromTouches(touches)
        let invalidKeys = pressedKeys.subtracting(notesFromTouches)
        if !invalidKeys.isEmpty {
            for key in invalidKeys {
                pressRemoveKey(key)
            }
        }
    }
    
    // 自动播放按下键
    func autoPressKey(_ newKey: PianoKey, index: Int) {
        if index == 1 {
            pressedKeys1.insert(newKey)
        } else if index == 2 {
            pressedKeys2.insert(newKey)
        }
        if newKey.pressed(immediately: false, play: false){
        }
    }
    
    // 自动播放松开键
    func releaseAllKeys(index: Int) {
        if index == 1 {
            for key in pressedKeys1 {
                pressRemoveKey(key)
            }
        } else if index == 2 {
            for key in pressedKeys2 {
                pressRemoveKey(key)
            }   
        }
    }
    
    // MARK: - 选中状态的音键集合操作
    func pressAddKey(_ newKey: PianoKey, immediately: Bool) {
        pressedKeys.insert(newKey)
        if newKey.pressed(immediately: immediately){
            //print("pressed():",newKey.soundFileName)
        }
    }
    
    func pressRemoveKey(_ newKey: PianoKey) {
        pressedKeys.remove(newKey)
        if newKey.released(){
            //print("released():",newKey.soundFileName)
            if !isAutoPlay {
                _ = newKey.stop()
            }
        }
    }
    
    func pressRemoveALLKeys() {
        for key in pressedKeys {
            pressRemoveKey(key)
        }
    }
    
    func pressRemoveKeys(keys: Set<PianoKey>) {
        for key in keys {
            pressRemoveKey(key)
        }
    }
    
    func pressStopKeys(keys: Set<PianoKey>) {
        for key in keys {
            if !isAutoPlay {
                key.stopSoundEffect()
            }
            
            pressRemoveKey(key)
        }
    }
    
    // MARK: - 琴键滑动手势
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            print("------touchesBegan",touch.location(in: self))
            let touchLoaction:CGPoint = CGPoint(x: touch.location(in: self).x, y: touch.location(in: self).y)
            if let key:PianoKey = getKeyFromLocation(touchLoaction) {
                if self.frame.height - touchLoaction.y<50 {
                    pressedKeys.insert(key)
                }
                else{
                    pressAddKey(key, immediately: true)
                }
                refreshTouches(event?.allTouches ?? Set<UITouch>())
            }
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("------touchesMoved")
        for touch in touches {
            if !self.frame.contains(touch.location(in: self)) && self.frame.contains(touch.location(in: self)) {
                //verifyTouches(event?.allTouches ?? Set<UITouch>())
            } else {
                if let key = getKeyFromLocation(touch.location(in: self)) ,
                    key != getKeyFromLocation(touch.previousLocation(in: self)) {
                    pressAddKey(key, immediately: true)
                    refreshTouches(event?.allTouches ?? Set<UITouch>())
                }
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("------touchesEnded")
        for touch in touches {
            if let key = getKeyFromLocation(touch.location(in: self)) {
                if var allTouches = event?.allTouches {
                    allTouches.remove(touch)
                    pressRemoveKey(key)
                }
            }
        }
        
        refreshTouches(event?.allTouches ?? Set<UITouch>())
    }
    
    // MARK: -自动调整offset
    func adjustSelfWithCurrentPressKey(key: PianoKey) -> Bool {
        if screenCanDisplayKey(key: key) || isBackgroundMode{
            return true
        }
        else{
            let pos = key.frame.origin.x
            offsetX = UIScreen.main.bounds.size.height*0.5 - pos
            // pianoVC.setKeyboardSliderWithOffset(offsetX: offsetX)
            //print("--------pos::::",pos)
        }
        return false;
    }
    
    func screenCanDisplayKey(key: PianoKey) -> Bool {
        let pos = key.frame.origin.x
        //print(":::",pos,offsetX,UIScreen.main.bounds.size.width)
        if (pos+offsetX>0 && pos+offsetX-UIScreen.main.bounds.size.width<0){
            return true;
        }
        return false;
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let inside = super.point(inside: point, with: event)
        if let key:PianoKey = getKeyFromLocation(point) {
            if self.frame.height - point.y<50 { // 屏幕边缘按钮延迟响应的问题
                print("------point inside in edage!")
                key.isHighlighted = true
                _ = key.pressed(immediately: true)
            }
        }
        return inside;
    }
    
    //
//    func updateThumdsImage() {
//        if (self.bounds.size.width<10){
//            return
//        }
//
//        UIGraphicsBeginImageContext(self.bounds.size)
//        self.layer.render(in: UIGraphicsGetCurrentContext()!)
//        let imgae = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        pianoVC.keyboardSlider.setThumbImage(imgae, for: UIControlState.normal);
//    }
}
