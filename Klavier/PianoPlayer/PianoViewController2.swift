//
//  PianoViewController2.swift
//  PianoPlayer
//  双排模式
//  Created by mengyun on 2018/3/3.
//  Copyright © 2018年 mengyun. All rights reserved.
//

import UIKit

class PianoViewController2: UIViewController {
    
    enum MoveType {                 //琴键移动
        case `default`, right, left
    }
    enum PianoKeyboardType {
        case up, down
    }
    var moveType:MoveType = .default
    var currentMove:PianoKeyboardType = .up
    
    let upKeyboardSlider = UISlider()
    let downKeyboardSlider = UISlider()
    
    var upKeyBoardView = PianoKeyboard();     //钢琴键盘
    var downKeyBoardView = PianoKeyboard();     //钢琴键盘
    var keyBoardViewWidth:CGFloat = g_defaultKeysWidth
    {
        didSet{
            upKeyBoardView.pianoKeysWidth = keyBoardViewWidth
            downKeyBoardView.pianoKeysWidth = keyBoardViewWidth
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.customDarkGray();
        // Do any additional setup after loading the view.
        
        let navHeight:CGFloat = 44*(isIpad ? 2.0:1)
        let height:CGFloat = self.view.frame.height/2 - navHeight;
        let pianoKeysWidth:CGFloat = keyBoardViewWidth*(isIpad ? 1.5:1)
        let width = pianoKeysWidth * 52;
        upKeyBoardView.createKeysWithWidth(frame: CGRect(x: 0, y: navHeight, width: width, height: height), pianoKeysWidth: pianoKeysWidth, offSet: keyBoardViewOffsetX*(isIpad ? 1.5:1))
        self.view.addSubview(upKeyBoardView)
        downKeyBoardView.createKeysWithWidth(frame: CGRect(x: 0, y: navHeight*2.0 + height, width: width, height: height), pianoKeysWidth: pianoKeysWidth, offSet: keyBoardViewOffsetX*(isIpad ? 1.5:1))
        upKeyBoardView.offsetX = udata.upOffset
        downKeyBoardView.offsetX = udata.downOffset
        self.view.addSubview(downKeyBoardView)
        
        upKeyboardSlider.setMinimumTrackImage(UIImage(named: "thick"), for: UIControl.State.normal);
        upKeyboardSlider.setMaximumTrackImage(UIImage(named: "thick"), for: UIControl.State.normal);
        upKeyboardSlider.setThumbImage(UIImage(named: "thumb"), for: UIControl.State.normal);
        upKeyboardSlider.addTarget(self, action: #selector(onThumbUp(sender:)), for: .valueChanged)
        upKeyboardSlider.addTarget(self, action: #selector(endSlider(sender:)), for: .touchUpInside)
        upKeyboardSlider.layer.masksToBounds = true
        upKeyboardSlider.layer.cornerRadius = 3.0;
        upKeyboardSlider.layer.borderWidth = 2.0;
        upKeyboardSlider.minimumValue = 0.0;
        upKeyboardSlider.maximumValue = Float(upKeyBoardView.pianoKeysWidth*52 - self.view.frame.width);
        upKeyboardSlider.setValue(Float(0 - upKeyBoardView.offsetX), animated: false)
        upKeyboardSlider.layer.borderColor = UIColor.customDarkGray().cgColor
        self.view.addSubview(upKeyboardSlider)
        
        downKeyboardSlider.setMinimumTrackImage(UIImage(named: "thick"), for: UIControl.State.normal);
        downKeyboardSlider.setMaximumTrackImage(UIImage(named: "thick"), for: UIControl.State.normal);
        downKeyboardSlider.setThumbImage(UIImage(named: "thumb"), for: UIControl.State.normal);
        downKeyboardSlider.addTarget(self, action: #selector(onThumbDown(sender:)), for: .valueChanged)
        downKeyboardSlider.addTarget(self, action: #selector(endSlider(sender:)), for: .touchUpInside)
        downKeyboardSlider.layer.masksToBounds = true
        downKeyboardSlider.layer.cornerRadius = 3.0;
        downKeyboardSlider.layer.borderWidth = 2.0;
        downKeyboardSlider.minimumValue = 0.0;
        downKeyboardSlider.maximumValue = Float(downKeyBoardView.pianoKeysWidth*52 - self.view.frame.width);
        downKeyboardSlider.setValue(Float(0 - downKeyBoardView.offsetX), animated: false)
        downKeyboardSlider.layer.borderColor = UIColor.customDarkGray().cgColor
        self.view.addSubview(downKeyboardSlider)
        
        self.adjustLayers();
    }
    
    @objc func onThumbUp(sender: UIButton?){
        upKeyBoardView.offsetX = 0 - CGFloat(upKeyboardSlider.value)
    }
    @objc func onThumbDown(sender: UIButton?){
        downKeyBoardView.offsetX = 0 - CGFloat(downKeyboardSlider.value)
    }
    @objc func endSlider(sender: UIButton?){
        setUDataOffset();
    }
    
    //添加约束
    func adjustLayers(){
        upKeyboardSlider.snp.makeConstraints{(make) -> Void in
            make.top.equalToSuperview()
            make.bottom.equalTo(upKeyBoardView.snp.top)
            make.centerX.equalToSuperview()
            make.width.equalTo(self.view.frame.width*0.6)
        }
        downKeyboardSlider.snp.makeConstraints{(make) -> Void in
            make.top.equalTo(upKeyBoardView.snp.bottom)
            make.bottom.equalTo(downKeyBoardView.snp.top)
            make.centerX.equalToSuperview()
            make.width.equalTo(self.view.frame.width*0.6)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
