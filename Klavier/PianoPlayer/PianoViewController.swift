//
//  PianoViewController.swift
//  PianoPlayer
//
//  Created by mengyun on 2017/6/18.
//  Copyright © 2017年 mengyun. All rights reserved.
//

import UIKit
import SnapKit
import MediaPlayer

enum LoopMode:Int {                             //播放类型：顺序，单曲循环和随机播放
    case `default`=0, one, shuffle
}
enum Play_Staff:Int {                           //播放模式（和弦，只高音，只低音）
    case chord=0,bass,treble
}

class PianoViewController: UIViewController, callBackDelegate {
    
    let request = HttpRequest()
    
    let menueBtnLeft = BadgeButton()        //左边选项
    let menueBtnRight = BadgeButton()
    let keyboardSlider = UISlider()         //控制键盘移动
    
    var playControllerView = UIImageView()      //播放控制
    var preMusicButton = UIButton();            //上一曲
    var playOrPauseButton = UIButton();         //播放和暂停
    var activityIndicatorView: UIActivityIndicatorView?
    var nextMusicButton = UIButton();           //下一曲
    var loopModeButton = UIButton();            //循环模式
    
   
    
    let loadButton = UIButton();
    var uploadView = UploadView();              // 用户自己的琴谱添加
    var loadButtonIsHidden = true{
        didSet{
            //print("-------->loadButtonIsHidden",loadButtonIsHidden)
            loadButton.isHidden = self.loadButtonIsHidden
            if loadButtonIsHidden{
                loadButton.snp.updateConstraints{(make) -> Void in
                    make.right.bottom.equalToSuperview()
                    make.width.equalTo(1)
                    make.height.equalTo(1)
                }
                
            }
            else{
                loadButton.snp.updateConstraints{(make) -> Void in
                    make.right.bottom.equalToSuperview()
                    make.width.equalTo(38)
                    make.height.equalTo(38)
                }
            }
        }
    }
    var songSlider = UISlider();                //进度条
    var timeLabel = UILabel();                  //时间显示
    var loopMode:LoopMode = .default {          //顺序，单曲循环和随机播放
        didSet{
            if loopMode == .default{
                loopModeButton.setImage(#imageLiteral(resourceName: "loopNor"), for: .normal);
            }
            if loopMode == .one{
                loopModeButton.setImage(#imageLiteral(resourceName: "loopOne"), for: .normal);
            }
            if loopMode == .shuffle{
                loopModeButton.setImage(#imageLiteral(resourceName: "loopShuffle"), for: .normal);
            }
        }
    }
   
    var musicNameLabel = UILabel()              //当前曲名
    
    func callbackDelegatefuc(backMsg: String) {
        // dosth
    }
    
    var currentScore = Notd()         //当前播放的曲谱内容
    {
        didSet{                                 //懒加载
            if  currentScore.trebleStaffArr.count < 1 {
                if (addXmlData(data: &currentScore) > 0){
                    list[mIndex] = currentScore
                }
                else{
                    print("error to add sound")
                }
                
                if drawerVC.currentType == .leftMenu{
                    leftVC.currentPlayingFid = currentScore.file
                    leftVC.mListTableView.reloadData()
                }
            }
        }
    }

    var trebleStaff:[String]!                   //高音谱表
    var pressedKeysTreble = Set<PianoKey>()    //当前已按键集合（高音）
    var playTrebleStaff=true                    //需要演奏高音
    var bassStaff:[String]!                     //低音谱表
    var pressedKeysBass = Set<PianoKey>()       //当前已按键集合（低音）
    var playBassStaff=true                      //需要演奏低音
    
    var isPause=true{                           //播放和暂停
        didSet{
            UIApplication.shared.isIdleTimerDisabled = !isPause
            if isPause{
                playOrPauseButton.setImage(UIImage(named: "pauseMsc"), for: .normal);
            }
            else{
                playOrPauseButton.setImage(UIImage(named: "playMsc"), for: .normal);
            }
        }
    }
    
    var soundName:String=""                 //当前曲名
    var author:String=""                    //当前曲名
    var noteDuration:Double=1.0{            //播放速度
        didSet{
            //let tmp = tickIndex
            if noteDuration>20.0 {
                noteDuration = 20.0
            }
            if noteDuration<0.1 {
                noteDuration = 0.1
            }

            var tmpDuration = 1.0;
            if currentScore.duration != "" {
                tmpDuration = (currentScore.duration as NSString).doubleValue
            }
            
            unitTime = (currentScore.unittTime as NSString).doubleValue*noteDuration * tmpDuration
            //mIndex=mIndex*1
            //tickIndex=tmp
        }
    }
    var keyBoardView = PianoKeyboard();     //钢琴键盘
    var keyBoardViewWidth:CGFloat = g_defaultKeysWidth
    {
        didSet{
            keyBoardView.pianoKeysWidth = keyBoardViewWidth
        }
    }
    
    //*********************************************定时器（高音）q

    var tickIndex = 0                           //播放进度
    {
        didSet{
            songSlider.value=Float(tickIndex)
            //let oldTimeStr = timeLabel.text;
            let newTimeStr = String(format: "%d/%d",tickIndex,soundLen);
//            if newTimeStr.count != oldTimeStr?.count {
//                timeLabel.font = UIFont.systemFont(ofSize: (CGFloat(22-newTimeStr.count))*(isIpad ? 1.5:1) );
//            }
            timeLabel.text = newTimeStr;
        }
    }
    var soundLen=0;                             //当前琴曲长度
    
    var gcdTimerVailed = true;
    var gcdTimer: DispatchSourceTimer?
    var ti: Double = 1.0                         //定时器周期
    {
        didSet{
            gcdTimer?.cancel()        // 取消上一个定时器
            var count = 0
            let queue = DispatchQueue(label: "gcdTimer", attributes: .concurrent)
            gcdTimer = DispatchSource.makeTimerSource(queue: queue)

            gcdTimer?.schedule(deadline: .now(), repeating: ti, leeway: .milliseconds(10))
            gcdTimer?.setEventHandler { [weak self] in  // 防止强引用
                if count == 0 || self?.gcdTimerVailed == false {
                    // 定时器初始化时不执行
                }
                else{
                    DispatchQueue.main.async {
                        self?.tickDown(); // UI刷新放主线程
                    }
                }
                count = count+1;
            }
            gcdTimer?.resume()
        }
    }
    var duration=1.0{                           //一个音持续时间
        didSet{
            ti = duration*unitTime
        }
    }
    //*********************************************定时器（高音）d
    var unitTime=1.0{                            //最小时间单位
        didSet{
            ti = duration*unitTime
            //ti_2 = duration_2*unitTime
        }
    }
    
    var soundCount: Int                             //琴曲数
    {
        get{
            return list.count
        }
    }
    var playList = [0]                          //记录播放顺序
    var playIndex = 0 {                         //记录的播放历史
        didSet{
            // print("==============playList:",playList)
            mIndex = playList[playIndex]
        }
    }
    
    // var mList = [[String : Any]]()              //正在播放的列表
    var uploadList : [Notd]? // 以文件形式打开的
    var list: [Notd] {
        get{
            if ((uploadList) != nil){
                return uploadList!;
            }
            if (leftVC != nil) {
                return leftVC.list
            }
            else{
                return udata.list
            }
        }
        set{}
    }
    var mIndex = 0{                            //曲目索引（当前曲目在总播放列表里的位置）
        didSet{
            if soundCount==0 {
                return
            }
            gcdTimerVailed = false
            //print(mIndex,soundCount)
            //self.loadButtonIsHidden = true
            while mIndex>=soundCount || mIndex<0 {
                if mIndex>=soundCount {
                    self.mIndex = self.mIndex - soundCount
                }
                if mIndex<0 {
                    self.mIndex = self.mIndex + soundCount
                }
            }

            currentScore = list[mIndex]
            trebleStaff = currentScore.trebleStaffArr
            bassStaff = currentScore.bassStaffArr
            
            if trebleStaff.count<1 && bassStaff.count<1 {  //读曲失败则跳下一曲
                //print("trebleStaff==nil")
                self.nextPlay(sender: nil)
                return
            }
            deleteDirtyChar(strArray: &trebleStaff)
            deleteDirtyChar(strArray: &bassStaff)
            setDuration(noteStr: trebleStaff[0],duration: &duration)
            let tmpDuration = (currentScore.duration as NSString).doubleValue
            unitTime = (currentScore.unittTime as NSString).doubleValue*noteDuration*tmpDuration
            soundName = currentScore.name
            soundLen = trebleStaff.count
            musicNameLabel.text = "\(soundName)";//String(format: "|%d/%d|%@|",mIndex,mList.count,soundName)
            songSlider.minimumValue = 0.0
            songSlider.maximumValue=Float(soundLen)
            tickIndex=0
            setUDataMindex()
            gcdTimerVailed = true
        }
    }
    
    enum MoveType {                 //琴键移动
        case `default`, right, left
    }

    init() {
        super.init(nibName: nil, bundle: nil)
        //getUData()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //print("-----------viewDidLoad()")
        // Do any additional setup after loading the view.
//        NotificationCenter.default.addObserver(self, selector: #selector(audioRouteChangeListenerCallback(notification:)), name: NSNotification.Name.AVAudioSession.routeChangeNotification, object: nil)
        setSessionActive()  //实现耳机线控
//        NotificationCenter.default.addObserver(self, selector:
//            #selector(AVAudioSessionInterruptionNotification(notification:)),name: NSNotification.Name.AVAudioSession.interruptionNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.kbFrameChanged(_:)), name: .UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        self.view.backgroundColor = UIColor.customDarkGray();
        self.view.layer.masksToBounds = true;
        self.view.layer.borderColor = UIColor.black.cgColor;
        self.view.layer.borderWidth = 1;
        self.view.layer.cornerRadius = 3;
        self.view.clipsToBounds = true;
        
        _ = self.view.frame.width
        let frameHeight = self.view.frame.height
        
        //左右页面弹出按钮
        menueBtnRight.setImage(#imageLiteral(resourceName: "rightMenu"), for: .normal)
        menueBtnRight.addTarget(self, action: #selector(showMenueRight), for: UIControl.Event.touchUpInside)
        self.view .addSubview(menueBtnRight);
        
        menueBtnLeft.setImage(#imageLiteral(resourceName: "leftMenu"), for: .normal)
        menueBtnLeft.addTarget(self, action: #selector(showMenueLeft), for: UIControl.Event.touchUpInside)
        self.view.addSubview(menueBtnLeft);
        
        
        musicNameLabel.textAlignment = .center
        if isIpad{
            musicNameLabel.font = UIFont.systemFont(ofSize: 28)
        }
        else{
            musicNameLabel.font = UIFont.systemFont(ofSize: 20)
        }
        musicNameLabel.textColor = UIColor.fontWhiteColor()
        musicNameLabel.numberOfLines = 2;
        musicNameLabel.adjustsFontSizeToFitWidth = true;
        musicNameLabel.minimumScaleFactor = 0.6;
        
        //controllerView.backgroundColor = UIColor.red;
        keyboardSlider.frame = CGRect(x: 50, y: 100, width: 100, height: 20)
        //keyboardSlider.value = Float(keyBoardView.pianoKeysWidth) * 52;
        keyboardSlider.setMinimumTrackImage(UIImage(named: "thick"), for: UIControl.State.normal);
        keyboardSlider.setMaximumTrackImage(UIImage(named: "thick"), for: UIControl.State.normal);
        keyboardSlider.setThumbImage(UIImage(named: "thumb"), for: UIControl.State.normal);
        keyboardSlider.addTarget(self, action: #selector(onThumb(sender:)), for: .valueChanged)
        keyboardSlider.layer.masksToBounds = true
        keyboardSlider.layer.cornerRadius = 3.0;
        keyboardSlider.layer.borderWidth = 2.0;
        keyboardSlider.minimumValue = 0.0;
        keyboardSlider.maximumValue = Float(keyBoardView.pianoKeysWidth*52 - self.view.frame.width);
        keyboardSlider.setValue(Float(0 - keyBoardViewOffsetX), animated: false)
        keyboardSlider.layer.borderColor = UIColor.customDarkGray().cgColor
        
        playControllerView.backgroundColor = UIColor.customDarkGray()
        playControllerView.image = UIImage(named: "bg")
        //playControllerView.backgroundColor = UIColor.red
        
        preMusicButton.setImage(UIImage(named: "preMsc"), for: .normal);
        preMusicButton.addTarget(self, action: #selector(prePlay(sender:)), for:
            .touchUpInside)
        playOrPauseButton.setImage(UIImage(named: "pauseMsc"), for: .normal);
        playOrPauseButton.addTarget(self, action: #selector(pauseOrPlay(sender:)), for:
            .touchUpInside)
        nextMusicButton.setImage(UIImage(named: "nextMsc"), for: .normal);
        nextMusicButton.addTarget(self, action: #selector(nextPlay(sender:)), for:
            .touchUpInside)
        songSlider.setMinimumTrackImage(UIImage(named: "thumb"), for: UIControl.State.normal)
        songSlider.setThumbImage(#imageLiteral(resourceName: "jdt"), for: .normal)
        songSlider.tintColor=UIColor.black
        songSlider.addTarget(self, action: #selector(valueChanged(sender:)), for: .valueChanged)
        songSlider.transform =  CGAffineTransform.init(scaleX: 1.0, y: 2.0)
        songSlider.isUserInteractionEnabled=false
        timeLabel.textAlignment = .right
        //timeLabel.font =
        timeLabel.adjustsFontSizeToFitWidth = true
        timeLabel.textColor = UIColor.fontWhiteColor()
        loopModeButton.setImage(#imageLiteral(resourceName: "loopNor"), for: .normal);
        loopModeButton.addTarget(self, action: #selector(changeLoopMode(sender:)), for:
            .touchUpInside)
        loadButton.setImage(#imageLiteral(resourceName: "add"), for: .normal);
        loadButton.addTarget(self, action: #selector(saveCurrentSound(sender:)), for:
            .touchUpInside)
        playControllerView.addSubview(preMusicButton);
        playControllerView.addSubview(playOrPauseButton);
        playControllerView.addSubview(nextMusicButton);
        playControllerView.addSubview(timeLabel);
        playControllerView.addSubview(loopModeButton);
        playControllerView.addSubview(loadButton);
        playControllerView.addSubview(keyboardSlider);
        playControllerView.isUserInteractionEnabled = true;
        playControllerView.layer.masksToBounds = true;
        self.view.addSubview(musicNameLabel);
        //self.view.addSubview(leftButton);
        self.view.addSubview(playControllerView);
        //self.view.addSubview(rightButton);
        
        self.view.addSubview(songSlider);
        
        let pianoKeysWidth:CGFloat = keyBoardViewWidth*(isIpad ? 2.0:1);
        let width = pianoKeysWidth * 52;
//        getUData()
        keyBoardView.createKeysWithWidth(frame: CGRect(x: 0, y: frameHeight*0.25, width: width, height: frameHeight*0.75), pianoKeysWidth: pianoKeysWidth, offSet: keyBoardViewOffsetX)
        self.view.addSubview(keyBoardView)
        self.view.addSubview(uploadView);
        self.adjustLayers()
        
        
    
        
        self.loadButtonIsHidden = true
        //self.view.bringSubview(toFront: uploadView)
        uploadView.isHidden = true;
        
//        getUData()
        
        self.request.delegate = self;
    }
    
    
    //添加约束
    func adjustLayers(){
        let buttonSize:CGFloat = 44*(isIpad ? 2.0:1)
        let spacing:CGFloat = 2*(isIpad ? 2.0:1)
        menueBtnLeft.snp.makeConstraints{(make) -> Void in
            make.left.equalToSuperview().offset(isIphoneX ? 30:0)
            make.top.equalToSuperview().offset(isIpad ? 30:0)
            make.height.width.equalTo(buttonSize)
        }
        menueBtnRight.snp.makeConstraints{(make) -> Void in
            make.right.equalToSuperview().offset(isIphoneX ? -30:0)
            make.top.equalToSuperview().offset(isIpad ? 30:0)
            make.height.width.equalTo(buttonSize)
        }

        musicNameLabel.snp.makeConstraints{(make) -> Void in
            make.top.equalToSuperview()
            make.left.equalToSuperview().offset(buttonSize*5)
            make.right.equalToSuperview().offset(0-buttonSize*5)
            make.height.equalTo(buttonSize)
        }
        
        songSlider.snp.makeConstraints{(make) -> Void in
            make.left.equalToSuperview()//.offset(isIphoneX ? 30:0)
            make.right.equalToSuperview()//.offset(isIphoneX ? -30:0)
            make.bottom.equalTo(keyBoardView.snp.top)
            make.height.equalTo(1)
        }
        
        playControllerView.snp.makeConstraints{(make) -> Void in
            make.left.equalToSuperview()//.offset(isIphoneX ? 30:0)
            make.right.equalToSuperview()//.offset(isIphoneX ? -30:0)
            make.bottom.equalTo(keyBoardView.snp.top).offset(-2)
            make.height.equalTo(buttonSize)
        }
        // 播放按钮居中
        playOrPauseButton.snp.makeConstraints{(make) -> Void in
            make.bottom.equalToSuperview().offset(-spacing)
            make.centerX.equalToSuperview()
            make.width.height.equalTo(buttonSize-2*spacing)
        }
        // 播放按钮左边
        preMusicButton.snp.makeConstraints{(make) -> Void in
            make.left.equalTo(keyboardSlider.snp.right).offset(spacing)
            make.right.equalTo(playOrPauseButton.snp.left).offset(-spacing)
            make.bottom.equalToSuperview().offset(-spacing)
            make.width.height.equalTo(buttonSize-2*spacing)
        }
        keyboardSlider.snp.makeConstraints{(make) -> Void in
            make.top.equalToSuperview().offset(spacing)
            make.left.equalToSuperview().offset(spacing*2)
            make.right.equalTo(preMusicButton.snp.left).offset(-spacing)
            make.bottom.equalToSuperview().offset(-spacing)
        }
        // 播放按钮右边
        nextMusicButton.snp.makeConstraints{(make) -> Void in
            make.bottom.equalToSuperview().offset(-spacing)
            make.left.equalTo(playOrPauseButton.snp.right).offset(spacing)
            make.width.height.equalTo(buttonSize-2*spacing)
        }
        loadButton.snp.makeConstraints{(make) -> Void in
            make.right.bottom.equalToSuperview().offset(-spacing)
            make.width.height.equalTo(buttonSize-2*spacing)
        }
        loopModeButton.snp.makeConstraints{(make) -> Void in
            make.right.equalTo(loadButton.snp.left).offset(-spacing)
            make.bottom.equalToSuperview().offset(-spacing)
            make.width.height.equalTo(buttonSize-2*spacing)
        }
        timeLabel.snp.makeConstraints{(make) -> Void in
            make.bottom.centerY.equalToSuperview()
            make.right.equalTo(loopModeButton.snp.left)
            make.width.equalTo(75)
        }
    
        uploadView.snp.makeConstraints{(make) -> Void in
            make.top.equalToSuperview()
            make.right.equalToSuperview().offset(-200*(isIpad ? 2.0:1))
            make.left.equalToSuperview().offset(200*(isIpad ? 2.0:1))
            make.bottom.equalToSuperview().offset(-180)
        }
    }
    
    func startAnimation() {
        if (self.activityIndicatorView == nil){
            activityIndicatorView = UIActivityIndicatorView(frame: self.playOrPauseButton.bounds)
            activityIndicatorView?.isUserInteractionEnabled = true
            activityIndicatorView?.style = .whiteLarge
            activityIndicatorView?.backgroundColor = UIColor(red: 50/255.0, green: 40/255.0, blue: 40/255.0, alpha: 0.9);
            activityIndicatorView?.layer.cornerRadius = (activityIndicatorView?.frame.size.width)!/2.0
            let transform = CGAffineTransform(scaleX: 0.7, y: 0.7);
            activityIndicatorView?.transform = transform;
            self.playOrPauseButton.addSubview(activityIndicatorView!);
        }
        
        self.activityIndicatorView!.superview?.bringSubviewToFront(self.activityIndicatorView!)
        self.activityIndicatorView!.startAnimating();
        
    }
    
    func stopAnimation() {
        DispatchQueue.main.async {
            self.activityIndicatorView!.stopAnimating();
        }
    }
    
    func newUserGuide(){
        let frame = view.frame;
        let bgView = UIView();
        bgView.frame = frame;
        bgView.backgroundColor = .maskColor();
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(closeNewUserGuide(_:)))
        bgView.addGestureRecognizer(tapGesture);
        
        let lineView1 = UIView(frame: CGRect(x: menueBtnLeft.center.x, y: menueBtnLeft.center.y, width: 80.0, height: 1.0));
        let lineView2 = UIView(frame: CGRect(x: menueBtnLeft.center.x+80.0, y: menueBtnLeft.center.y, width: 1.0, height: 25.0));
        lineView1.backgroundColor = .noticeColor();
        lineView2.backgroundColor = .noticeColor();
        let noticeLabel = UILabel(frame: CGRect(x: menueBtnLeft.center.x+40, y: menueBtnRight.center.y+20, width: 200.0, height: 25.0));
        noticeLabel.textColor = .noticeColor();
        noticeLabel.text = getInternationalizedStrings(from: "noticemenueleft");
        
        UIApplication.shared.keyWindow?.addSubview(bgView);
        bgView.addSubview(lineView1);
        bgView.addSubview(lineView2);
        bgView.addSubview(noticeLabel);
        let path=UIBezierPath(rect: frame);
        path.append(UIBezierPath(rect: menueBtnLeft.frame).reversing());
        
        let lineView3 = UIView(frame: CGRect(x: menueBtnRight.center.x, y: menueBtnRight.center.y, width: 1.0, height: 30.0));
        let lineView4 = UIView(frame: CGRect(x: menueBtnRight.center.x-100.0, y: menueBtnRight.center.y+30.0, width: 101.0, height: 1.0));
        let lineView5 = UIView(frame: CGRect(x: menueBtnRight.center.x-100.0, y: menueBtnRight.center.y+30.0, width: 1.0, height: 20.0));
        lineView3.backgroundColor = .noticeColor();
        lineView4.backgroundColor = .noticeColor();
        lineView5.backgroundColor = .noticeColor();
        let noticeLabel2 = UILabel(frame: CGRect(x: menueBtnRight.center.x-200.0, y: menueBtnRight.center.y+40, width: 200.0, height: 30.0));
        noticeLabel2.textColor = .noticeColor();
        noticeLabel2.text = getInternationalizedStrings(from: "noticemenueright");
        bgView.addSubview(lineView3);
        bgView.addSubview(lineView4);
        bgView.addSubview(lineView5);
        bgView.addSubview(noticeLabel2);
        path.append(UIBezierPath(rect: menueBtnRight.frame).reversing());
        
        
        let shape = CAShapeLayer();
        shape.path = path.cgPath;
        bgView.layer.mask = shape;
    }
    
    @objc func closeNewUserGuide(_ sender:UITapGestureRecognizer ){
        let view = sender.view;
        view?.removeFromSuperview();
        _ = view?.subviews.map {
            $0.removeFromSuperview();
        }
        view?.removeGestureRecognizer(sender);
    }

    @objc func prePlay(sender: UIButton?){
        if loopMode == .default{
            mIndex = mIndex-1
        }
        else if loopMode == .one{ //单曲循环点击上一曲也要到上一曲
            mIndex = mIndex-1
        }
        else{
            if playIndex==0{
                let newIndex = Int(arc4random())%soundCount
                playList.insert(newIndex, at: 0)
                playIndex = 0
            }
            else{
                playIndex = playIndex-1
            }
        }
        isPause=false
    }
    
    @objc func pauseOrPlay(sender: UIButton?){
        
        isPause = !isPause
    }
    
    
    @objc func nextPlay(sender: UIButton?){
        switch loopMode {
        case .shuffle:
            let count = self.playList.count
            let newIndex = Int(arc4random()) % self.soundCount
            if self.playIndex == count-1{
                self.playList.append(newIndex)
            }
            else{
                self.playList[self.playIndex+1] = newIndex
            }
            self.playIndex = self.playIndex+1
        default:
            self.mIndex = self.mIndex+1
        }
        
        isPause=false
        
        self.preLoadNextPlay()
    }
    
    // 预先load下一曲
    @objc func preLoadNextPlay(){
        switch loopMode {
        case .shuffle:
            return
        default:
            var tmpIndex = mIndex + 1
            if tmpIndex >= soundCount{
                tmpIndex = tmpIndex - soundCount;
            }
            let _ = request.addXmlData(data: &list[tmpIndex])
        }
    }
    
    //
    func setKeyboardSlider() {
        keyboardSlider.minimumValue = 0.0;
        keyboardSlider.maximumValue = Float(keyBoardView.pianoKeysWidth*52 - self.view.frame.width);
        keyboardSlider.setValue(Float(0 - keyBoardViewOffsetX), animated: false)
    }
    func setKeyboardSliderWithOffset(offsetX: CGFloat) {
        keyboardSlider.minimumValue = 0.0;
        keyboardSlider.maximumValue = Float(keyBoardView.pianoKeysWidth*52 - self.view.frame.width);
        keyboardSlider.setValue(Float(0 - offsetX), animated: false)
    }
    @objc func onThumb(sender: UIButton?){
        //print("songSlider.value \(keyboardSlider.value)  \(Float(keyBoardView.pianoKeysWidth*52 - self.view.frame.width))")
        keyBoardView.offsetX = 0 - CGFloat(keyboardSlider.value)
    }
    
    @objc func valueChanged(sender: UIButton?){
        tickIndex = Int(songSlider.value)
    }
    
    @objc func changeLoopMode(sender: UIButton?){
        if loopMode == .default{
            loopMode = .one
        }
        else if loopMode == .one{
            loopMode = .shuffle
//            playList=[Int]()   //单曲循环，播放列表只有一首
//            playList.append(mIndex)
        }
        else{
            loopMode = .default
        }
        setUDataLoopMode()
    }
    
    @objc func saveCurrentSound(sender: UIButton?){
        self.uploadView.isHidden = false;
        self.uploadView.nameTextFiled.text = loadMusicData.name;
        self.view.bringSubviewToFront(self.uploadView);
    }
    
    func setDuration(noteStr: String,duration: inout Double){
        let arr = noteStr.components(separatedBy: "$")
        if arr.count>1 { //设置音持续时间
            duration = Double(arr[1])!
            //print("duration get:",duration)
        }
        else{           //兼容旧版本，默认1
            duration = 1.0
        }
    }
    
    @objc func tickDown(){
        if isPause {
            return
        }
        if trebleStaff==nil {  //当前曲有问题，直接下一曲
            if loopMode == .default{
                mIndex=mIndex+1
            }
            else if loopMode == .one{
                mIndex = 0+mIndex
            }
            else{
                let count = playList.count
                let newIndex = Int(arc4random())%soundCount
                if playIndex>=count-1{
                    playList.append(newIndex)
                }
                else{
                    playList[playIndex+1] = newIndex
                }
                playIndex = playIndex+1
            }
            return
        }

        if tickIndex<trebleStaff.count {
            if trebleStaff[tickIndex]=="0"{
                //keyBoardView.pressStopALLKeys()
                keyBoardView.pressStopKeys(keys: pressedKeysTreble)
                pressedKeysTreble = Set<PianoKey>()
            }
            else if trebleStaff[tickIndex] != "" {
                //                if !(playBassStaff && tickIndex<bassStaff.count) {
                //                    keyBoardView.pressRemoveALLKeys()
                //                }
                keyBoardView.pressRemoveKeys(keys: pressedKeysTreble)
                pressedKeysTreble = Set<PianoKey>()
                var arr = trebleStaff[tickIndex].components(separatedBy: "$")
                if arr.count>1{
                    arr = arr[0].components(separatedBy: "|")
                    setDuration(noteStr: trebleStaff[tickIndex],duration: &duration)
                }
                else{
                    arr = trebleStaff[tickIndex].components(separatedBy: "|")
                }
                if playTrebleStaff {
                    for item in arr {
                        ////print("item=========\(item)")
                        if item != ""{
                            let soundNameTheme:String = (musicParser.getNoteStringBy(noteString: item))
                            let btn:PianoKey = keyBoardView.getBtn(soundName: soundNameTheme)
                            if btn.soundFileName == soundNameTheme {
                                if keyBoardView.adjustSelfWithCurrentPressKey(key: btn) {
                                    
                                }
                                pressedKeysTreble.insert(btn)
                                //DispatchQueue.main.async(execute: {
                                //ALPlayer?.setSteroType(1)
                                self.keyBoardView.pressAddKey(btn, immediately: false)
                                //})
                            }
                        }
                    }
                }
            }
        }
        
        if playBassStaff && tickIndex<bassStaff.count {
            if  bassStaff[tickIndex]=="0"{
                keyBoardView.pressStopKeys(keys: pressedKeysBass)
                pressedKeysBass = Set<PianoKey>()
            }
            else if bassStaff[tickIndex] != "" {
                //let arr = bassStaff[tickIndex].components(separatedBy: "|")
                var arr = bassStaff[tickIndex].components(separatedBy: "$")
                if arr.count>1{
                    arr = arr[0].components(separatedBy: "|")//辅音不重新设置timer周期
                    //setDuration(noteStr: bassStaff[tickIndex],duration: &duration)
                }
                else{
                    arr = bassStaff[tickIndex].components(separatedBy: "|")
                }
                keyBoardView.pressRemoveKeys(keys: pressedKeysBass)
                pressedKeysBass = Set<PianoKey>()
                for item in arr {
                    if item != ""{
                        let soundNameTheme:String = (musicParser.getNoteStringBy(noteString: item))
                        let btn:PianoKey = keyBoardView.getBtn(soundName: soundNameTheme)
                        if btn.soundFileName == soundNameTheme {
                            if keyBoardView.adjustSelfWithCurrentPressKey(key: btn) {
                                
                            }
                            pressedKeysBass.insert(btn)
                            //DispatchQueue.main.async(execute: {
                            //ALPlayer?.setSteroType(0)
                                self.keyBoardView.pressAddKey(btn, immediately: false)
                            //})
                        }
                    }
                }
            }
        }
        tickIndex = (tickIndex+1)
        if tickIndex > soundLen{
            //print("tickIndex >= soundLen--------")
            if loopMode == .default{
                mIndex=mIndex+1
            }
            else if loopMode == .one{
                mIndex = 0+mIndex
            }
            else{
                let count = playList.count
                let newIndex = Int(arc4random())%soundCount
                if playIndex==count-1{
                    playList.append(newIndex)
                }
                else{
                    playList[playIndex+1] = newIndex
                }
                playIndex = playIndex+1
            }
        }
    }
    
    //去掉音符集合里面的"["、"]"字符
    func deleteDirtyChar(strArray:inout [String]!){
        var sArray = [String]()
        for str in strArray {
            var tmpStr = str.replacingOccurrences(of: "[", with: "")
            tmpStr = tmpStr.replacingOccurrences(of: "]", with: "")
            sArray.append(tmpStr)
        }
        strArray = sArray
    }
    
    @objc func showMenueRight() {
        //print("--------------------showMenueRight")
        //let drawerVC = UIApplication.shared.keyWindow?.rootViewController as! DrawerMenuController
//        drawerVC.currentType = .rightMenu
//        rightVC.setLabelsText()
//        drawerVC.showShadow(true)
    }
    @objc func showMenueLeft() {
        //print("--------------------showMenueLeft")
        //self.isPause = true
        //let drawerVC = UIApplication.shared.keyWindow?.rootViewController as! DrawerMenuController
//        drawerVC.currentType = .leftMenu
//        drawerVC.showShadow(true)
    }
    
    @objc func kbFrameChanged(_ notification : Notification){
        let info = notification.userInfo  
        let kbRect = (info?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue  
        uploadView.snp.updateConstraints{(make) -> Void in
            make.top.equalToSuperview()
            make.right.equalToSuperview().offset(-200*(isIpad ? 2.0:1))
            make.left.equalToSuperview().offset(200*(isIpad ? 2.0:1))
            make.bottom.equalToSuperview().offset(0-kbRect.height)
        }
    } 
    
    //耳机插拔
    @objc func audioRouteChangeListenerCallback(notification: NSNotification){
        let interuptionDict = notification.userInfo
        let routeChangeReason = interuptionDict![AVAudioSessionRouteChangeReasonKey] as! UInt
        switch routeChangeReason {
        case AVAudioSession.RouteChangeReason.newDeviceAvailable.rawValue:
            print("-------charu ")
        case AVAudioSession.RouteChangeReason.oldDeviceUnavailable.rawValue:
            //print("-------bachu ")
            //ALPlayer?.stopAllSource();
            DispatchQueue.main.async {
                self.isPause = true
            }
        default:
            break
        }
    }
    @objc func AVAudioSessionInterruptionNotification(notification: NSNotification){
//        DispatchQueue.main.async(execute: {
//            self.doPlaySilentMusic()
//        })
        let interuptionDict = notification.userInfo
        let interruptType = interuptionDict![AVAudioSessionInterruptionTypeKey] as! UInt
        //print("to AVAudioSessionInterruptionNotification",interruptType)
        if interruptType == AVAudioSession.InterruptionType.began.rawValue {
            //print("AVAudioSessionInterruptionType.began.rawValue")
//            ALPlayer?.stopAllSource();
            newPlayer?.destory();
            self.isPause = true;
        }
        else{
            //print("AVAudioSessionInterruptionType= ",interruptType)
        }
    }
    
    func setSessionActive(){
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setActive(true)
            try session.setCategory(AVAudioSession.Category.playback)
        } catch {
            //print(error)
        }
    }
    //处理耳机事件
    override func remoteControlReceived(with event: UIEvent?) {
        switch (event?.subtype)! {
        case .remoteControlTogglePlayPause:
            isPause = !isPause
        case .remoteControlNextTrack:
            nextPlay(sender: nil)
        case .remoteControlPreviousTrack:
            prePlay(sender: nil)
        default:
            print("...")
        }
    }
   
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //页面展示时，保证能收到耳机的事件
    override func viewDidAppear(_ animated: Bool) {
        UIApplication.shared.beginReceivingRemoteControlEvents()
        self.becomeFirstResponder()
        if appIsFirstLaunch(flag: 3){
            self.newUserGuide();
        }
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.endReceivingRemoteControlEvents()
        self.resignFirstResponder()
    }
    
    override var canBecomeFirstResponder: Bool{
        return true
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if (event?.subtype == UIEvent.EventSubtype.motionShake){
            if (drawerVC.g_lockScreen){
                //self.nextPlay(sender: nil)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
