//
//  SqlManager.swift
//  SQLiteTest
//
//  Created by mengyun2 on 2024/11/4.
//

import Foundation
import FMDB


struct ScorePartWiseData {
    var id = 0
    var name = ""
    var file = ""
    var type = 0
    var category = "" // 类目
    var author = ""  // 作者
    var measureCount = 0
}

class SQLiteManager {


    static let shared = SQLiteManager()

    let tableName = "score_partwise"
    var db: FMDatabase?


    private init() {

        if let path = Bundle.main.path(forResource: "scores", ofType: "sqlite") {
            db = FMDatabase(url: URL(fileURLWithPath: path))
        }

        if db?.open() == true {
            print("数据库打开成功")
        }
    }
    
    func createTable() {
            let createTableString = "CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, age INTEGER);"
            do {
                try db?.executeUpdate(createTableString, values: nil)
                print("Table created successfully.")
            } catch {
                print("Error creating table: \(error.localizedDescription)")
            }
        }

    func fetchAllScores(key: String, val: String) -> [ScorePartWiseData] {

        var scores = [ScorePartWiseData]()
        do {
            var query = "SELECT * FROM \(tableName)"
            if !key.isEmpty {
                query = "SELECT * FROM \(tableName) WHERE \(key)=\(val)"
            }
            let resultSet = try db?.executeQuery(query, values: nil)
            while resultSet?.next() == true {
                let id = resultSet?.int(forColumn: "id") ?? 0
                let name = resultSet?.string(forColumn: "name") ?? ""
                let file = resultSet?.string(forColumn: "file") ?? ""
                
                let score = ScorePartWiseData(id: Int(id), name: name, file: file)
                scores.append(score)
            }
        } catch {
            print("Failed to fetch scores: \(error)")
        }
        return scores
    }

//    func insertScore(file: String, name: String) {
//        do {
//            if let r = try db?.executeUpdate("INSERT INTO \(tableName) (file, name) VALUES (?, ?)", values: [file, name]) {
//                print("数据新增成功")
//                db?.close()
//            }
//            
//        } catch {
//            print("Failed to insert score: (error)")
//        }
//    }
//    
//    func updateData() {
//          let updateSQL = "UPDATE  \(tableName) SET name = '花生' WHERE id = 1"
//          
//        let result = db?.executeUpdate(updateSQL, withArgumentsIn: []) ?? false
//          
//          if result {
//              print("修改成功")
//          }
//      }
}
