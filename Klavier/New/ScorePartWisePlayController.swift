//
//  ScoreWisePlayController.swift
//  Klavier
//
//  Created by mengyun2 on 2024/11/2.
//  Copyright © 2024 mengyun. All rights reserved.
//

import UIKit

class ScorePartWisePlayController: UIViewController {
    var data: ScorePartWiseData
    var scoreManager: ScoreManager?
    var titleLabel = UILabel()
    var measuresScrollView = UIScrollView()
    var measureViews = [MeasureProgressView]()
    var keyBoardView = PianoKeyboard()
    let rowCount = 17  // 每行展示小节数目
    let titleViewHeight: CGFloat = 40
    let measureViewHeight: CGFloat = 30
    var page: CGFloat = 0  // 第几页了，一页有 rowCount*3 个小节
    var backButton = UIButton()
    var nextButton = UIButton()
    
    init(data: ScorePartWiseData) {
        self.data = data
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        view.backgroundColor = UIColor.customDarkGray()
        
        titleLabel.frame = CGRect(x: 100, y: 0, width: mainSize.width-200, height: titleViewHeight)
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.boldSystemFont(ofSize: 22)
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.numberOfLines = 2
        titleLabel.textColor = .systemBlue
        view.addSubview(titleLabel)
        
        measuresScrollView = UIScrollView(frame: CGRect(x: 0, y: titleViewHeight+2, width: mainSize.width, height: measureViewHeight*3))
        measuresScrollView.isScrollEnabled = true
        measuresScrollView.backgroundColor = .white.withAlphaComponent(0.2)
        view.addSubview(measuresScrollView)
        
        let keyboardWidth = g_defaultKeysWidth * 52;
        keyBoardView.createKeysWithWidth(frame: CGRect(x: 0, y: titleViewHeight + measureViewHeight*3 + 4,
                                                       width: keyboardWidth, height: mainSize.height-(titleViewHeight + measureViewHeight*3 + 4)),
                                         pianoKeysWidth: g_defaultKeysWidth, offSet: keyBoardViewOffsetX)
        self.view.addSubview(keyBoardView)
        
        backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 66, height: titleViewHeight))
        backButton.backgroundColor = UIColor.systemRed
        backButton.addTarget(self, action: #selector(pop(sender:)), for: .touchUpInside)
        view.addSubview(backButton)
        
        nextButton = UIButton(frame: CGRect(x: mainSize.width-66, y: 0, width: 66, height: titleViewHeight))
        nextButton.backgroundColor = UIColor.systemRed
        nextButton.addTarget(self, action: #selector(pop(sender:)), for: .touchUpInside)
        view.addSubview(nextButton)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.loadData()
        }
    }
    
    @objc func pop(sender: UIButton?){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func tap(_ sender:UITapGestureRecognizer ){
        let mv = sender.view as? MeasureProgressView
        let index = mv?.tag ?? 0
    
        measureViews.forEach { v in
            v.isCurrent = false
        }
        measureViews[index].isCurrent = true
        
        scoreManager?.setStartMeasureNumber(measureNumber: mv?.numberLabel.text ?? "")
        beginToPlay()
    }
    
    func beginToPlay(){
        keyBoardView.isAutoPlay = true
        scoreManager?.tryPlay(callback: { [weak self] measureNumber  in
            guard let self = self else {
                return
            }
            self.measureViews.forEach { v in
                if v.text == measureNumber {
                    v.isCurrent = true
                    if let index = self.measureViews.firstIndex(of: v) {
                        let pageHeight = self.measureViewHeight*3
                        let page = CGFloat(index/(self.rowCount*3))
                        if page != self.page {
                            self.measuresScrollView.contentOffset = CGPoint(x: 0, y: page*pageHeight)
                            self.page = page
                        }
                    }
                    
                    if let scManger = self.scoreManager, let count = scManger.partMap["P1"]?.noteList[scManger.measureIndex].staff1.count {
                        let progress = CGFloat(scManger.noteIndex)/CGFloat(count)
                        v.progress = progress
                        self.backButton.alpha = (1-progress*0.7)
                        self.nextButton.alpha = (1-progress*0.7)
                        self.titleLabel.textColor = .systemBlue.withAlphaComponent(1-progress*0.7)
                        
                        self.keyBoardView.releaseAllKeys(index: 1)
                        for noteString in scManger.partMap["P1"]!.noteList[scManger.measureIndex].staff1[scManger.noteIndex] {
                            let arr: [String] = noteString.components(separatedBy: "|")
                            let noteStr = musicParser.getNoteStringBy(noteString: arr[1])
                            let key: PianoKey = self.keyBoardView.getBtn(soundName: noteStr)
                            _ = self.keyBoardView.adjustSelfWithCurrentPressKey(key: key)
                            self.keyBoardView.autoPressKey(key, index: 1)
                        }
                        
                        self.keyBoardView.releaseAllKeys(index: 2)
                        for noteString in scManger.partMap["P1"]!.noteList[scManger.measureIndex].staff2[scManger.noteIndex] {
                            let arr: [String] = noteString.components(separatedBy: "|")
                            let noteStr = musicParser.getNoteStringBy(noteString: arr[1])
                            let key: PianoKey = self.keyBoardView.getBtn(soundName: noteStr)
                            _ = self.keyBoardView.adjustSelfWithCurrentPressKey(key: key)
                            self.keyBoardView.autoPressKey(key, index: 2)
                        }
                    }
                    
                } else {
                    v.isCurrent = false
                    v.progress = 0
                }
                
            }
            
        })
    }
      
    func loadData() {
        scoreManager = ScoreManager(file: data.file)
        
        measureViews.forEach { btn in
            btn.isHidden = true
        }
        
        var measuresCount = 0
        if let measures = scoreManager?.score?.parts.first?.measures {
            measuresCount = measures.count
            let numberOfRows = 1 + measuresCount/rowCount
            let w = mainSize.width/CGFloat(rowCount)
            let h = measureViewHeight
            measureViews.forEach { v in
                v.removeFromSuperview()
            }
            measureViews = []
            for i in 0..<numberOfRows {
                for j in 0..<rowCount {
                    let x = CGFloat(j) * w
                    let y = CGFloat(i) * h
                    let v = MeasureProgressView(frame: CGRect(x: x+1, y: y+1, width: w-2, height: h-2))
                    measuresScrollView.addSubview(v)
                    v.tag = i*10 + j
                    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tap(_:)))
                    v.addGestureRecognizer(tapGesture)
                    measureViews.append(v)
                }
            }
            measuresScrollView.contentSize = CGSize(width: mainSize.width, height: h*CGFloat(numberOfRows))
            
            for i in 0..<measuresCount {
                measureViews[i].isHidden = false
                measureViews[i].text = measures[i].number
                measureViews[i].isCurrent = false
            }
            
            beginToPlay()
        }
        
        titleLabel.text = "曲谱:\(data.name) | 乐器数:\(scoreManager?.score?.parts.count ?? 0) | 小节数:\(measuresCount)"
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        scoreManager = nil
    }

}

class MeasureProgressView: UIView {
    
    var progress = 0.0 {
        didSet {
            indexView.snp.updateConstraints { make in
                make.height.centerY.equalToSuperview()
                make.width.equalTo(2)
                make.centerX.equalTo(self.snp.left).offset(self.bounds.width*progress)
            }
            backgroundColor = getBackgroundColor()
        }
    }
    
    var isCurrent = false {
        didSet {
            indexView.isHidden = !isCurrent
        }
    }
    
    var text: String? {
        didSet {
            numberLabel.text = text
        }
    }
    
    let numberLabel = UILabel()
    let indexView = UIView()
    let crossLine = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = getBackgroundColor()
        layoutSubViews()
    }
    
    func layoutSubViews() {
        self.addSubview(crossLine)
        crossLine.snp.makeConstraints { make in
            make.center.width.equalToSuperview()
            make.height.equalTo(2)
        }
        crossLine.backgroundColor = .systemBlue.withAlphaComponent(0.8)
         crossLine.isHidden = true
        
        self.addSubview(numberLabel)
        numberLabel.textAlignment = .center
        numberLabel.textColor = .white.withAlphaComponent(0.7)
        numberLabel.font = UIFont.systemFont(ofSize: 12)
        numberLabel.snp.makeConstraints { make in
            make.left.right.height.width.equalToSuperview()
        }
        self.addSubview(indexView)
        indexView.snp.makeConstraints { make in
            make.height.centerY.equalToSuperview()
            make.width.equalTo(2)
            make.centerX.equalTo(self.snp.left).offset(self.bounds.width*progress)
        }
        indexView.backgroundColor = .systemGreen
        indexView.isHidden = true
    }
    
    func getBackgroundColor() -> UIColor {
        var alpha = 0.3
        var r = 1.0
        var g = 1.0
        var b = 1.0
        if isCurrent {
            alpha = 0.3 + progress*0.7
            r = 1.0 - progress * 0.2
            g = 1.0 - progress
            b = 1.0 - progress
        } else {
            
        }
        return UIColor(red: r, green: g, blue: b, alpha: alpha)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
