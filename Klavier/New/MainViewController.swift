//
//  AllViewController.swift
//  Klavier
//
//  Created by mengyun2 on 2024/11/3.
//  Copyright © 2024 mengyun. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    var selectScrollView = UIScrollView()
    var selectButtons = [UIButton]()
    var selectedIndex = 0 {
        didSet {
            for i in 0..<selectButtons.count {
                selectButtons[i].backgroundColor = i==selectedIndex ? UIColor.systemBlue : UIColor.lightGray
            }
            refreshDatas()
        }
    }
    
    var playListScrollView = UIScrollView()
    var playListButtons = [UIButton]()
    var datas: [ScorePartWiseData] = []
    let rowCount: Int = 8
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        
        selectScrollView.frame = CGRect(x: 50, y: 0, width: UIScreen.main.bounds.width-100, height: 48)
        view.addSubview(selectScrollView)
        for i in 0..<cats.count {
            let btn = UIButton(frame: CGRect(x: i*70, y: 0, width: 70, height: 48))
            btn.tag = i
            btn.setTitle(cats[i], for: .normal)
            btn.titleLabel?.numberOfLines = 1
            btn.titleLabel?.adjustsFontSizeToFitWidth = true
            btn.titleLabel?.minimumScaleFactor = 0.5
            btn.addTarget(self, action: #selector(onSelect(sender:)), for: .touchUpInside)
            selectScrollView.addSubview(btn)
            selectButtons.append(btn)
        }
        selectScrollView.contentSize = CGSize(width: cats.count*70, height: 48)
        self.selectedIndex = 0
        
        let paddingX: CGFloat = 50
        let scrrolViewWidth = mainSize.width - paddingX*2
        playListScrollView.frame = CGRect(x: paddingX, y: 50, width: scrrolViewWidth, height: mainSize.height - 60)
        view.addSubview(playListScrollView)
    
    }
    let cats = ["新添加", "经典", "动漫", "华语", "英文", "火影忍者", "周杰伦", "儿歌", "多part", "全部", "Test"]
    
    func refreshDatas(){
        
        getData()
        let paddingX: CGFloat = 50
        let paddingY: CGFloat = 40
        let scrrolViewWidth = mainSize.width - paddingX*2
        let scrrolViewHeight = mainSize.height - paddingY*2
        
        let w = scrrolViewWidth/CGFloat(rowCount)
        let h = scrrolViewHeight/6
        var i = 0
        playListButtons.forEach { btn in
            btn.removeFromSuperview()
        }
        playListButtons = []
        for data in datas {
            let y = CGFloat(i/(rowCount))
            let x = CGFloat(i%(rowCount))
            let btn = UIButton(frame: CGRect(x: x*w + 1, y: y*h + 2, width: w-2, height: h-4))
            btn.setTitle(data.name, for: .normal)
            btn.titleLabel?.numberOfLines = 2
            btn.titleLabel?.adjustsFontSizeToFitWidth = true
            btn.titleLabel?.minimumScaleFactor = 0.5
            btn.backgroundColor = UIColor.black.withAlphaComponent(0.1)
            btn.setTitleColor(UIColor.black.withAlphaComponent(0.3), for: .normal)
            btn.addTarget(self, action: #selector(goToScoreDetail(sender:)), for: .touchUpInside)
            btn.tag = i
            i += 1
            playListButtons.append(btn)
            playListScrollView.addSubview(btn)
        }
        playListScrollView.contentSize = CGSize(width: Int(scrrolViewWidth), height: (datas.count/rowCount)*Int(h) + 80)

    }
    
    @objc func onSelect(sender: UIButton?){
        let index = sender?.tag ?? 0
        if index != self.selectedIndex {
            self.selectedIndex = index
        }
        
    }
    
    func getData() {
        if selectedIndex == 0 {
            datas = SQLiteManager.shared.fetchAllScores(key: "type", val: "1")
        } else if selectedIndex == 1 {
            datas = SQLiteManager.shared.fetchAllScores(key: "category", val: "\"经典\"")
        } else if selectedIndex == 2 {
            datas = SQLiteManager.shared.fetchAllScores(key: "category", val: "\"动漫\"")
        } else if selectedIndex == 3 {
            datas = SQLiteManager.shared.fetchAllScores(key: "category", val: "\"华语\"")
        } else if selectedIndex == 4 {
            datas = SQLiteManager.shared.fetchAllScores(key: "category", val: "\"英文\"")
        } else if selectedIndex == 5 {
            datas = SQLiteManager.shared.fetchAllScores(key: "author", val: "\"火影忍者\"")
        } else if selectedIndex == 6 {
            datas = SQLiteManager.shared.fetchAllScores(key: "author", val: "\"周杰伦\"")
        } else if selectedIndex == 7 {
            datas = SQLiteManager.shared.fetchAllScores(key: "category", val: "\"儿歌\"")
        } else if selectedIndex == 8 {
            // datas = SQLiteManager.shared.fetchAllScores(key: "category", val: "\"经典\"")
        } else if selectedIndex == 9 {
           datas = SQLiteManager.shared.fetchAllScores(key: "", val: "")
        } else {
            let vc = PianoViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func goToScoreDetail(sender: UIButton?){
        let index = sender?.tag ?? 0
        let vc = ScorePartWisePlayController(data: datas[index])
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
