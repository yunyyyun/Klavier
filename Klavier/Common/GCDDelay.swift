//
//  GCDDelay.swift
//  PianoPlayer
//  一个简单的gcd delay方法，非线程安全
//  Created by mengyun on 2018/1/8.
//  Copyright © 2018年 mengyun. All rights reserved.
//

import Foundation

class GCDDelay : NSObject{
    var taskBlock = DispatchWorkItem {
    }
    var startTimeInterval:Double = 0;
    var numberOfSecondsBefore:Double = 0;

    func delay(numberOfMilliseconds: Int, task: @escaping ()->()) {
        taskBlock = DispatchWorkItem.init(block: task);
        startTimeInterval = NSDate().timeIntervalSince1970;
        let deadline = DispatchTime.now() + .milliseconds(numberOfMilliseconds) + Double(Int64(1*NSEC_PER_SEC))/Double(NSEC_PER_SEC);
        numberOfSecondsBefore = Double(numberOfMilliseconds);
        DispatchQueue.main.asyncAfter(deadline: deadline, execute: taskBlock);
    }
    
    func delay(numberOfSeconds: Int, task: @escaping ()->()) {
        taskBlock = DispatchWorkItem.init(block: task);
        startTimeInterval = NSDate().timeIntervalSince1970;
        let deadline = DispatchTime.now() + .seconds(numberOfSeconds) + Double(Int64(1*NSEC_PER_SEC))/Double(NSEC_PER_SEC);
        numberOfSecondsBefore = Double(numberOfSeconds);
        DispatchQueue.main.asyncAfter(deadline: deadline, execute: taskBlock);
    }

    func cancel() {
        taskBlock.cancel()
        startTimeInterval = 0;
        numberOfSecondsBefore = 0;
    }

    func delayTest(){
        delay(numberOfMilliseconds: 3, task: {print("321")})
    }
}
