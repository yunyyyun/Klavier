//
//  Utilities.swift
//  PianoPlayer
//
//  Created by mengyun on 2017/11/6.
//  Copyright © 2017年 mengyun. All rights reserved.
//

import UIKit
import Foundation

extension UIColor {
    
    //深蓝
    static func darkBlueColor() -> UIColor {
        return UIColor(red: 18.0/255.0, green: 86.0/255.0, blue: 136.0/255.0, alpha: 1.0)
    }
    //浅蓝
    static func lightBlueColor() -> UIColor {
        return UIColor(red: 0.0, green: 122.0/255.0, blue: 1.0, alpha: 1.0)
    }
    //黄昏色
    static func duskColor() -> UIColor {
        return UIColor(red: 255/255.0, green: 181/255.0, blue: 68/255.0, alpha: 1.0)
    }
    //自定义橘色
    static func customOrangeColor() -> UIColor {
        return UIColor(red: 40/255.0, green: 43/255.0, blue: 53/255.0, alpha: 1.0)
    }
    //字体白
    static func fontWhiteColor() -> UIColor {
        return UIColor(red: 200/255.0, green: 200/255.0, blue: 222/255.0, alpha: 1.0)
    }
    
    //自定义深灰
    static func customDarkGray() -> UIColor {
        return UIColor(red: 12/255.0, green: 22/255.0, blue: 12/255.0, alpha: 1.0)
        //return UIColor(red: (255-12)/255.0, green: (255-22)/255.0, blue: (255-11)/255.0, alpha: 1.0)
    }
    //自定义浅白
    static func customButtonWhite() -> UIColor {
        return UIColor(red: (255-12)/255.0, green: (255-22)/255.0, blue: (255-12)/255.0, alpha: 1.0)
        //return UIColor(red: 66/255.0, green: 77/255.0, blue: 88/255.0, alpha: 1.0)
    }
    
    //分类按钮未选中的颜色
    static func noSelectColor() -> UIColor {
        return UIColor(red: 222/255.0, green: 222/255.0, blue: 222/255.0, alpha: 1.0)
    }
    //分类按钮选中的颜色
    static func selectColor() -> UIColor {
        return UIColor(red: 244/255.0, green: 244/255.0, blue: 244/255.0, alpha: 1.0)
    }
    //选中的分类按钮文案的颜色
    static func fontBlackColor() -> UIColor {
        return UIColor(red: 88/255.0, green: 88/255.0, blue: 99/255.0, alpha: 1.0)
    }
    //当前播放的cell按钮文案的颜色
    static func fontRedColor() -> UIColor {
        return UIColor(red: 253/255.0, green: 62/255.0, blue: 81/255.0, alpha: 1.0)
    }
    //新手引导页面颜色
    static func maskColor() -> UIColor {
        return UIColor(red: 223/255.0, green: 223/255.0, blue: 244/255.0, alpha: 0.6)
    }
    //新手引导文案颜色
    static func noticeColor() -> UIColor {
        return UIColor(red: 77/255.0, green: 33/255.0, blue: 93/255.0, alpha: 0.9)
    }
    static func detailColor() -> UIColor {
        return UIColor(red: 136/255.0, green: 136/255.0, blue: 136/255.0, alpha: 1.0)
    }
    static func cellLineColor() -> UIColor {
        return UIColor(red: 170/255.0, green: 170/255.0, blue: 170/255.0, alpha: 0.2)
    }
}
