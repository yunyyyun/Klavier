//
//  Common.swift
//  PianoPlayer
//
//  Created by mengyun on 2017/7/16.
//  Copyright © 2017年 mengyun. All rights reserved.
//

import UIKit
import Foundation
import StoreKit

let mainSize = UIScreen.main.bounds
var con = Controll()
var isIphoneX = false;
var isIpad = false;

let spaceWidth:CGFloat = 2.0    //间隙
let cellHeight:CGFloat = 55
let tableViewWidth:CGFloat = 288
let g_defaultKeysWidth:CGFloat = 55    //默认琴键宽度
let minXmlDataCount = 66
let host = "http://111.231.221.195"
var alreadyGetListCount = 0 //已经请求过网络列表的次数
var isBackgroundMode = false
let keyBoardViewOffsetX:CGFloat = -1100

var loadMusicData = MusicData()
var musicParser = MusicParser()                  //音符解释器
let newPlayer = SoundBankPlayer.sharedPlayer(withID: 0)
var defaultList: [[String: Any]] = []//[furElise,mzdhl,shuoHaoDeXinFuNe,Omake,s_a_s,qingNiao,griefAndSorrow,mleckdsj]
// var defaultList = [furElise]
var udata: UData!
var drawerVC: DrawerMenuController!
var pianoVC: PianoViewController!
var leftVC: PalyListViewController!
var rightVC: SetViewController!
var pianoVCTwoHandle:PianoViewController2!
let iOSAppLink = "https://itunes.apple.com/cn/app/piano-player/id1360641074?mt=8"
let UM_PUSH_APP_KEY = "5bd02b20b465f5fa4300009e";
let UM_PUSH_APP_Secret = "rovf4k76necdusc0l6vy4i714ygflzbx";
var versionStr = "";

// 完全阻塞方式添加数据
func addXmlData(data: inout Notd)->Int{
    //xml文件解析
    let fileName = data.file
    let soundName = data.name
    let duration = data.duration

    let iscached = data.iscached
    // 读内存的曲谱
    if iscached == "reserved" {
        for note in defaultList {
            if fileName == note["file"] as! String {
                data.trebleStaffArr = note["trebleStaff"] as! Array
                data.bassStaffArr = note["bassStaff"] as! Array
                data.unittTime = note["unittime"] as! String
                data.duration = duration
                return 2;
            }
        }
    }

    // 读缓存的曲谱
    var tmpData = MusicData()
    let semaphore = DispatchSemaphore(value: 0)
    MC.getObj(fileName){(obj) -> () in
        if obj == nil {
            //let xmlData = getData(fid: fileName)
            let path = Bundle.main.path(forResource: fileName, ofType: "xml");
            let url = URL(fileURLWithPath: path!)
            var xmlData:String?
            do{
                xmlData = try! String(contentsOf: url)
            }
            //print("xmlData is ",xmlData!)
            if xmlData==nil || (xmlData?.count)! < minXmlDataCount { //小于66的当作不正确的xml
                //return -1
            }
            else{
//                let xmlDataStr = (xmlData! as NSString).utf8String
//                let xmlDataMutablePointer:UnsafeMutablePointer = UnsafeMutablePointer(mutating: xmlDataStr!)
//                let l = LuaFunc()
//                let rlt = l?.getNoteInfo(fromXMLFile: xmlDataMutablePointer) as! [String : Any]
//                tmpData.name = soundName
//                tmpData.duration = duration
//                tmpData.unittime = rlt["unittime"] as! String
//                tmpData.trebleStaff = rlt["trebleStaff"]as! String
//                tmpData.bassStaff = rlt["bassStaff"]as! String
//                MC.saveObj(fileName, value: tmpData)
//                udata.refreshStatus()
            }
            semaphore.signal()
        }
        else{
            tmpData = obj as! MusicData
            semaphore.signal()
        }
    }
    let waitTime = DispatchTime.now() + .seconds(99)
    _ = semaphore.wait(timeout: waitTime)
    //_ = semaphore.wait(timeout: DispatchTime.distantFuture)
    if (tmpData.bassStaff.count+tmpData.trebleStaff.count) < minXmlDataCount{
        return -1
    }
    let tmp = Notd()
    tmp.name = soundName
    tmp.file = fileName
    tmp.unittTime = tmpData.unittime
    tmp.duration = duration
    tmp.trebleStaffArr = (tmpData.trebleStaff).components(separatedBy: ",") as Array
    tmp.bassStaffArr = (tmpData.bassStaff).components(separatedBy: ",") as Array
    data = tmp
    return 1
}
func getData(fid: String)->String?{
    var xmlData:String = "error"
    let semaphore = DispatchSemaphore(value: 0)
    let urlString = "\(host)/getMusicXmlData.php?file=\(fid)&version=\(versionStr)"
    let url: URL = URL(string: urlString)!
    let request:NSURLRequest=NSURLRequest(url: url)
    let session = URLSession.shared
    let dataTask = session.dataTask(with: request as URLRequest,completionHandler: {(data, response, error) ->Void in
        if error != nil {
            semaphore.signal()
        }else{
            //MC.saveVoc(fid, data: data!)
            xmlData = String(data:data! ,encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
            semaphore.signal()
        }
    })as URLSessionTask
    dataTask.resume()
    let waitTime = DispatchTime.now() + .seconds(88)
    _ = semaphore.wait(timeout: waitTime)
    //_ = semaphore.wait(timeout: DispatchTime.distantFuture)
    return xmlData
}

func getCurrentLanguage() -> String {
    let preferredLang = Bundle.main.preferredLocalizations.first! as NSString
    
    switch String(describing: preferredLang) {
    case "en-US", "en-CN":
        return "us"//英文
    case "zh-Hans-US","zh-Hans-CN","zh-Hant-CN","zh-TW","zh-HK","zh-Hans":
        return "cn"//中文
    default:
        return "us"
    }
}

// 读取网络琴谱文件列表
func getDataList(type: classIDType){
    
    let serverVersionNumber = (con.version as NSString).intValue
    if (udata.version >= serverVersionNumber){
        print("udata.musicDataMap version control no  \(udata.version)  \(serverVersionNumber)")
        return
    }
    if alreadyGetListCount>3{
        return
    }

    print("udata.musicDataMap alreadyGetListCount yes \(alreadyGetListCount)  \(udata.version)  \(con.version)")
    let lan = getCurrentLanguage();
    let urlString = "\(host)/getScores.php?&type=\(type.rawValue)&lan=\(lan)&version=\(versionStr)"
    let url: URL = URL(string: urlString)!
    let request:NSURLRequest=NSURLRequest(url: url)
    let session = URLSession.shared
    let dataTask = session.dataTask(with: request as URLRequest,completionHandler: {(data, response, error) ->Void in
        if error != nil {
        }else{
            
            guard let arrayJSONString = String(data: data!, encoding: String.Encoding.utf8) else {
                return
            }
            udata.serverData = arrayJSONString;
            if let notes = [Notd].deserialize(from: arrayJSONString), notes.count>0 {
                udata.serverList = notes as! [Notd];
            }
            udata.version = Int(serverVersionNumber)
            saveUData();
            udata.refreshStatus()
            DispatchQueue.main.async {
                leftVC.updateList()
                leftVC.mListTableView.reloadData()
                setMListIsCached()
            }
        }
    })as URLSessionTask
    dataTask.resume()
}

func setUDataMindex(){
    udata.lastFile = pianoVC.currentScore.file
    //print("ssssssSave \(udata.lastFile)")
    saveUData()
}
func setUDataLoopMode(){
    udata.loopMode = pianoVC.loopMode
    saveUData()
}
func setUDataKeysWidth(){
    udata.keysWidth = pianoVC.keyBoardView.pianoKeysWidth
    saveUData()
}
func setUDataOffset(){
    udata.upOffset = pianoVCTwoHandle.upKeyBoardView.offsetX
    udata.downOffset = pianoVCTwoHandle.downKeyBoardView.offsetX
    saveUData()
}
func saveUData(){
    MC.saveObj("UDATA", value: udata)
}

func getUData(){
    MC.getObj("UDATA") {
        (obj) -> () in
        if obj == nil {
            DispatchQueue.main.async(execute: {
                // pianoVC.pList = udata.uList
                pianoVC.mIndex = 1
                pianoVC.playList = [1]
            })
        }else{
            udata = obj as? UData
            DispatchQueue.main.async(execute: {
                pianoVC.keyBoardView.pianoKeysWidth = udata.keysWidth
                pianoVC.mIndex = udata.lastIndex
                pianoVC.loopMode = udata.loopMode
                pianoVC.playList = [udata.lastIndex]
                
                pianoVCTwoHandle.upKeyBoardView.offsetX = udata.upOffset
                pianoVCTwoHandle.downKeyBoardView.offsetX = udata.downOffset
                
                //print("ssssssGet \(udata.lastFile)")
            })
        }
    }
}

// 获取指定语言的文案
func getInternationalizedStrings(from key: String)-> String{
    return NSLocalizedString(key, comment: "");
}

// 判断string是否包含中文
func isIncludeChinese(string: String) -> Bool {
    for (_, value) in string.enumerated() {
        if ("\u{4E00}" <= value  && value <= "\u{9FA5}") {
            return true
        }
    }
    return false
}

// 汉字转拼音
func transformToPinyin(hanzi: String) -> String {
    let stringRef = NSMutableString(string: hanzi) as CFMutableString
    CFStringTransform(stringRef,nil, kCFStringTransformToLatin, false) // 转换为带音标的拼音
    CFStringTransform(stringRef, nil, kCFStringTransformStripCombiningMarks, false) // 去掉音标
    let pinyin = stringRef as String
    return pinyin
}

//判断字符串占位长度
func roughLen(string: String) -> Int {
    if isIncludeChinese(string: string){
        return 2*string.count;
    }
    return string.count;
}

// 判断app是首次启动
func appIsFirstLaunch(flag: Int)->Bool{
    struct Temp{
        static var _oneTracker:Int = 1;
    }
    if (Temp._oneTracker%flag)==0 {
        return false;
    }
    Temp._oneTracker=Temp._oneTracker*flag;
    return UserDefaults.standard.bool(forKey: "firstLaunch")
}

func mlistIsCached()->Bool{
    if UserDefaults.standard.bool(forKey: "mlistIsCached") == false {
        print("mlistIsCached no")
        return false;
    }
    else{
//        let date = NSDate()
//        let currentTime = date.timeIntervalSince1970
//        let lastTime = UserDefaults.standard.double(forKey: "mlistCacheTime")
//        print("mlistIsCached yes", lastTime, currentTime, currentTime-lastTime)
        return true;//currentTime-lastTime<7*24*60*60;
    }
}
func setMListIsCached(){
    UserDefaults.standard.set(true, forKey: "mlistIsCached");
//    let date = NSDate()
//    let currentTime = date.timeIntervalSince1970
//    print("mlistIsCached set", currentTime)
//    UserDefaults.standard.set(currentTime, forKey: "mlistCacheTime");
}

// 请求app内评分
//func requestView(){
//    var loadTimes =  UserDefaults.standard.object(forKey: "load_times") as? Int ?? 0;
//    loadTimes = loadTimes+1;
//    if loadTimes > 3{
//        if #available(iOS 10.3, *) {
//            SKStoreReviewController.requestReview()
//        } else {
//            // Fallback on earlier versions
//        };
//        loadTimes = -9
//    }
//    UserDefaults.standard.set(loadTimes, forKey: "load_times")
//}

func iPhoneNotchScreen()->Bool {
    if #available(iOS 11.0, *) {
        var iPhoneNotchDirectionSafeAreaInsets:CGFloat = 0;
        let safeAreaInsets = UIApplication.shared.windows[0].safeAreaInsets;
        switch(UIApplication.shared.statusBarOrientation){
        case .unknown:
            iPhoneNotchDirectionSafeAreaInsets = safeAreaInsets.top;
        case .portrait:
            iPhoneNotchDirectionSafeAreaInsets = safeAreaInsets.top;
        case .portraitUpsideDown:
            iPhoneNotchDirectionSafeAreaInsets = safeAreaInsets.bottom;
        case .landscapeLeft:
            iPhoneNotchDirectionSafeAreaInsets = safeAreaInsets.left;
        case .landscapeRight:
            iPhoneNotchDirectionSafeAreaInsets = safeAreaInsets.right;
        @unknown default:
            iPhoneNotchDirectionSafeAreaInsets = safeAreaInsets.top;
        }
        return iPhoneNotchDirectionSafeAreaInsets > 20;
    }
    return false;
}


func gcd(_ m: Int, _ n: Int) -> Int {
  var a = 0
  var b = max(m, n)
  var r = min(m, n)

  while r != 0 {
    a = b
    b = r
    r = a % b
  }
  return b
}
