//
//  OcclusionViewController.swift
//  PianoPlayer
//
//  Created by mengyun on 2017/7/16.
//  Copyright © 2017年 mengyun. All rights reserved.
//

import UIKit

class OcclusionViewController: UIViewController {
    func Unlock(_ sender: AnyObject) {
        self.view.removeFromSuperview()
    }
    @IBOutlet var lockNoticeView: UIImageView!
    @IBOutlet var timeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let tapPan = UITapGestureRecognizer(target: self, action: #selector(hiddenSwitchView(_:)))
        tapPan.numberOfTouchesRequired = 1; //手指数
        tapPan.numberOfTapsRequired = 1; //tap次数
        self.view.addGestureRecognizer(tapPan)
        self.lockNoticeView.alpha = 0.0;
        self.timeLabel.alpha = 0.0;
    }
    
    @objc func hiddenSwitchView(_ sender:UITapGestureRecognizer ){
        self.lockNoticeView.alpha = 1.0;
        self.timeLabel.alpha = 1.0;
        
        let date = Date()
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HH:mm:ss"
        let strNowTime = timeFormatter.string(from: date) as String
        self.timeLabel.text = strNowTime
        
        UIView.animate(withDuration: 0.7, delay: 0.3, animations: { () -> Void in
            self.lockNoticeView.alpha = 0.0;
            self.timeLabel.alpha = 0.0;
        }, completion: nil);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}
