//
//  HttpRequest.swift
//  PianoPlayer
//
//  Created by mengyun on 2018/2/26.
//  Copyright © 2018年 mengyun. All rights reserved.
//

import Foundation
import UIKit

//定义协议
protocol callBackDelegate {
    func callbackDelegatefuc(backMsg:String)
}
class HttpRequest: NSObject{
    //定义一个符合协议的代理对象
    var delegate:callBackDelegate?
//    func request(fid: String?){
//        DispatchQueue.global().async {
//            let _ = self.requestData(fid: fid!)
//        }
//    }
    //
    func addXmlData(data: inout Notd)->Int{
        //xml文件解析
        let fileName = data.file
        let soundName = data.name
        let duration = data.duration
        
        let iscached = data.iscached
        if iscached == "reserved" {
            for note in defaultList {
                if fileName == note["file"] as! String {
                    data.trebleStaffArr = note["trebleStaff"] as! [String]
                    data.bassStaffArr = note["bassStaff"] as! [String]
                    data.unittTime = note["unittime"] as! String
                    data.duration = duration
                    if((self.delegate) != nil){
                        DispatchQueue.main.async {
                            self.delegate?.callbackDelegatefuc(backMsg: "yes")
                        }
                    }
                    return 2;
                }
            }
        }
        var tmpData = MusicData()
        MC.getObj(fileName){(obj) -> () in
            if obj == nil {
                // let xmlData = self.syncRequestData(fid: fileName)
                let path = Bundle.main.path(forResource: fileName, ofType: "xml");
                let url = URL(fileURLWithPath: path!)
                var xmlData:String?
                do{
                    xmlData = try! String(contentsOf: url)
                }
                //print("xmlData is ",xmlData!)
                if xmlData==nil || (xmlData?.count)! < minXmlDataCount { //小于66的当作不正确的xml
                    //return -1
                }
                else{
//                    let xmlDataStr = (xmlData! as NSString).utf8String
//                    let xmlDataMutablePointer:UnsafeMutablePointer = UnsafeMutablePointer(mutating: xmlDataStr!)
//                    let l = LuaFunc()
//                    let rlt = l?.getNoteInfo(fromXMLFile: xmlDataMutablePointer) as! [String : Any]
//                    tmpData.name = soundName
//                    tmpData.duration = duration
//                    tmpData.unittime = rlt["unittime"] as! String
//                    tmpData.trebleStaff = rlt["trebleStaff"]as! String
//                    tmpData.bassStaff = rlt["bassStaff"]as! String
//                    MC.saveObj(fileName, value: tmpData)
//                    udata.refreshStatus()
                }
            }
            else{
                tmpData = obj as! MusicData
            }
            
            if (tmpData.bassStaff.count+tmpData.trebleStaff.count) < minXmlDataCount{
                //return -1
            }
            var tmp: [String : Any] = [String : Any]()
            tmp["name"] = soundName
            tmp["file"] = fileName
            tmp["unittime"] = tmpData.unittime
            tmp["duration"] = duration
            tmp["trebleStaff"] = (tmpData.trebleStaff).components(separatedBy: ",")
            tmp["bassStaff"] = (tmpData.bassStaff).components(separatedBy: ",")
            
            if((self.delegate) != nil){
                DispatchQueue.main.async {
                    self.delegate?.callbackDelegatefuc(backMsg: "yes")
                }
            }
            //data = tmp
        }
        return 1
    }
    
//    func requestData(fid: String){
//        var xmlData:String = "error"
//        let urlString = "\(host)/getMusicXmlData.php?file=\(fid)&version=\(versionStr)"
//        let url: URL = URL(string: urlString)!
//        let request:NSURLRequest=NSURLRequest(url: url)
//        let session = URLSession.shared
//        let dataTask = session.dataTask(with: request as URLRequest,completionHandler: {(data, response, error) ->Void in
//            if error != nil {
//                xmlData = "request failed;"
//            }else{
//                xmlData = String(data:data! ,encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
//
//            }
//            if((self.delegate) != nil){
//                DispatchQueue.main.async {
//                    self.delegate?.callbackDelegatefuc(backMsg: xmlData)
//                }
//            }
//        })as URLSessionTask
//        dataTask.resume()
//        return
//    }
    
    func syncRequestData(fid: String)->String?{
        var xmlData:String = "error"
        let semaphore = DispatchSemaphore(value: 0) // 使用信号量实现同步
        let urlString = "\(host)/getMusicXmlData.php?file=\(fid)&version=\(versionStr)"
        let url: URL = URL(string: urlString)!
        let request:NSURLRequest=NSURLRequest(url: url)
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest,completionHandler: {(data, response, error) ->Void in
            if error != nil {
                semaphore.signal()
            }else{
                xmlData = String(data:data! ,encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
                semaphore.signal()
            }
        })as URLSessionTask
        dataTask.resume()
        let waitTime = DispatchTime.now() + .seconds(88) //超时时间88s
        _ = semaphore.wait(timeout: waitTime)
        //_ = semaphore.wait(timeout: DispatchTime.distantFuture)
        return xmlData
    }
    
    func likeOrCancel(fid: String, liked: Bool){
        let extend = liked ?"add" : "reduce"
        let urlString = "\(host)/likeOrCancel.php?file\(extend)=\(fid)&version=\(versionStr)"
        let url: URL = URL(string: urlString)!
        let request:NSURLRequest=NSURLRequest(url: url)
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest,completionHandler: {(data, response, error) ->Void in
        })as URLSessionTask
        dataTask.resume()
        return
    }
}
