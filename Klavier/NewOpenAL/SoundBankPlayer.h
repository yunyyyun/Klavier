
#import <Foundation/Foundation.h>
#import <OpenAL/al.h>
#import <OpenAL/alc.h>


@interface SoundBankPlayer: NSObject

//@property (nonatomic, assign) BOOL loopNotes;

@property (nonatomic, assign) ALfloat pitchRate;

// @property (nonatomic, assign) int noteOffset;

//@property (nonatomic, strong) NSArray *soundsCategory;

// @property (nonatomic, assign) BOOL needPlay;  //可以将一组音加入队列，在下个循环播放，通过needplay来控制


// 0 钢琴
// 1 电子琴
// 2 古筝
// 3 巴斯
// 4 吉他
// 5 竖琴
// 6 木琴
+ (SoundBankPlayer *)sharedPlayerWithID: (NSInteger)index;

- (void) resume;
- (void) destory;

// 将note音放入播放数组等待播放，用于同时播放多个音
- (void) pushNoteToQueueOn: (int)midiNoteNumber gain: (float)gain duration: (float)duration;

// 立即停止某个音
- (void) noteOff: (int)midiNoteNumber;

@end
