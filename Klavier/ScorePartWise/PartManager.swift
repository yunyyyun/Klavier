//
//  PartManager.swift
//  Klavier
//
//  Created by mengyun2 on 2024/10/30.
//  Copyright © 2024 mengyun. All rights reserved.
//

import Foundation

// 一个 part 代表一种乐器
class PartManager {
    
    var part: Part?
    
    var ts: [Double] = []
    var tempo: Double = 0 // 一分钟多少拍
    var divisions: Double = 0 // 一个四分音符长度（四分音符=1拍）
    var measureIndex = 0  // 第几个小节了
    var noteIndex = 0  // 第几个音了
    var noteList: [MeasureNotesData] = []
    
    var isMute = 0 //
    var player: SoundBankPlayer?
    
    var gcdTimerVailed = true
    var gcdTimer: DispatchSourceTimer?
    var ti: Double = 1.1                         //定时器周期
    {
        didSet{
            print(" ti 更新 \(part?.id ?? "") \(ti)")
            gcdTimer?.cancel()        // 取消上一个定时器
            let queue = DispatchQueue(label: "gcdTimer", attributes: .concurrent)
            gcdTimer = DispatchSource.makeTimerSource(queue: queue)

            var count = 0
            gcdTimer?.schedule(deadline: .now(), repeating: ti, leeway: .milliseconds(1))
            gcdTimer?.setEventHandler { [weak self] in  // 防止强引用
                if count == 0 || self?.gcdTimerVailed == false {
                    // 定时器初始化时不执行
                    // print("定时器初始化时不执行 \(self?.ti) \(self?.gcdTimerVailed)")
                } else {
                    DispatchQueue.main.async {
                        self?.tickDown(); // UI刷新放主线程
                    }
                }
                count += 1
            }
            gcdTimer?.resume()
        }
    }

    var endCallback: ((String) -> Void)?
    var progressCallback: ((Int) -> Void)?
    
    init(part: Part, divisions: Double, tempo: Double) {
        self.part = part
        self.divisions = divisions
        self.tempo = tempo
        self.handleMeasures()
    }
    
    @objc func tickDown(){
        print("tick--- \(noteIndex)->\(noteList[measureIndex].staff1.count)")
        if noteIndex == noteList[measureIndex].staff1.count {
            print("___part_id: \(part?.id ?? "")第 \(measureIndex+1) 小节播放完成！！！！")
            self.endCallback?(part?.id ?? "")
            // gcdTimerVailed = false
            return
        }
        
        if isMute == 0 {
            // staff1 高音谱号
            // staff2 低音谱号
            let notes = noteList[measureIndex].staff1[noteIndex] + noteList[measureIndex].staff2[noteIndex]
            for note in notes {
                let arr: [String] = note.components(separatedBy: "|")
                let noteStr = musicParser.getNoteStringBy(noteString: arr[1])
                let midiId: Int32 = Int32(musicParser.getfileIDByNoteString(noteString: noteStr))
                let dynamics = Float(arr[0]) ?? 0
                let gain = dynamics/100.0
                player?.pushNoteToQueue(on: midiId, gain: gain, duration: 1)
            }
        } else {
            print("--------------------已经被静音")
        }
        progressCallback?(noteIndex)
        noteIndex += 1
    }
    
    func handleMeasures(){
        
        guard let part = part else {
            return
        }
        
        var repeatDatas: [MeasureNotesData] = []  // 有的小节要重复

        for i in 0..<part.measures.count {
            let m = part.measures[i]
            print("______开始解析 measure_number: \(m.number ?? "")")
            let data = handleMeasure(measure: m)
            noteList.append(data)
            print("____________完成解析 measure_number: \(m.number ?? "") \(m.datas.count)")

            if m.datas.first?.barline?.repeat_direction == "forward" {
                repeatDatas = []
            }
            
            if m.datas.last?.barline?.repeat_direction == "backward" {
                noteList = noteList + repeatDatas
            }
            repeatDatas.append(data)
        }
        
    }
    
    // 预处理一个小节
    func handleMeasure (measure: Measure) -> MeasureNotesData {
        var allTimes = 0 // 当前小节一共所要的 duration 个数
        var unit: Int? // 所有 note duration 的公约数
//        var firstNode = true
        for c in measure.datas {
            var duration: Int?
            if c.name == "attributes" {
                let a = divisions
                divisions = Double(c.attributes?.divisions ?? "0") ?? 0 // 一个四分音符长度（四分音符=1拍）
                if divisions == 0 {
                    divisions = a
                }
                print("________content_name_attributes 一个四分音符长度 \(c.attributes?.divisions ?? "")")
            } else if c.name == "direction" {
                let a = tempo
                tempo = Double(c.direction?.tempo ?? "0") ?? 0 // 一分钟多少拍
                if tempo == 0 {
                    tempo = a
                }
                // print("________content_name_direction 一分钟 \(c.direction?.tempo ?? "") 拍， 一拍 \(60.0/(Double(c.direction?.tempo ?? "1") ?? 1)) s 等分成\(divisions) 个duration")
            } else if c.name == "note" {
//                if firstNode {
//                    firstNode = false
//                    let secondPreBeat = 60.0/tempo // 一拍多少秒
//                }
                duration = Int(c.note?.duration ?? "")
                if c.note?.chord == nil, let d = duration {
                    allTimes += d
                }
                print("________content_name \(c.name ?? "") _staff \(c.note?.staff ?? "")  \(c.note?.pitch?.desc() ?? "") |dot_\(c.note?.dot ?? "0")| _duration \(c.note?.duration ?? "") _t \(allTimes)")
            } else if c.name == "backup" {
                // let duration = Int(c.note?.duration ?? "") ?? 0
                duration = Int(c.backup_duration ?? "")
                allTimes -= duration ?? 0
                print("________content_name \(c.name ?? "") _duration \(c.backup_duration ?? "") _t \(allTimes)")
            } else if c.name == "forward" {
                // let duration = Int(c.note?.duration ?? "") ?? 0
                duration = Int(c.forward_duration ?? "")
                allTimes += duration ?? 0
                print("________content_name \(c.name ?? "") _duration \(c.forward_duration ?? "") _t \(allTimes)")
            } else if c.name == "print" {
            } else if c.name == "barline" {
            } else {
                print("warning!!!💢 未知name！！！: \(c.name ?? "")")
            }
            
            if unit == nil {
                unit = duration
            } else if let d = duration, let g = unit {
                unit = gcd(d, g)
            }
        }
        print("______measures_end_with_t \(allTimes) \(unit ?? 0) ")
        
        var count = allTimes/unit!
        let secondPreBeat: Double = 60.0/tempo // 一拍多少秒
        let secondPreDuration: Double = secondPreBeat/divisions // 一个 duration 多少秒
        var unitTime = Double(unit ?? 0) * secondPreDuration
        var scale = 1.0
        if unitTime < 0.01 {
            print("warning!!!💢 \(measure.number ?? "") \(unitTime) \(unit ?? 0) \(count)")
            scale = 2
            count = count / Int(scale)
            unit = unit! * Int(scale)
            unitTime = unitTime *  scale
        }
        
        
        var notes1 = [[String]](repeating: [], count: count)
        var notes2 = [[String]](repeating: [], count: count)
        
        var timeIndex = 0
        for c in measure.datas {
            if c.name == "note" {
                let duration = Int(c.note?.duration ?? "")
                if let d = duration {
                    let isChord = c.note?.chord != nil
                    if isChord { // 合旋律
                        timeIndex -= d
                    }
                    
                    if let p = c.note?.pitch?.format() {
                        print("----\(timeIndex) \(c.note?.pitch?.desc() ?? "")  \(c.note?.pitch?.format() ?? "")")
                        var d = Double(c.note?.dynamics ?? "") ?? 100
                        if c.note?.tie_type == "continue" {
                            d = d/64
                        } else if c.note?.tie_type == "stop" {
                            d = d/128
                        }
                        if let staff = c.note?.staff, staff == "2" {
                            notes2[timeIndex/unit!].append("\(d)|\(p)")
                        } else {
                            notes1[timeIndex/unit!].append("\(d)|\(p)")
                        }
                    }
                    timeIndex += d
                }
               
            } else if c.name == "backup" {
                if let duration = Int(c.backup_duration ?? "") {
                    timeIndex -= duration
                    if timeIndex < 0 {
                        timeIndex = 0
                    }
                }
            } else if c.name == "forward" {
                if let duration = Int(c.forward_duration ?? "") {
                    timeIndex += duration
                }
            }
        }
        
        
        return MeasureNotesData(unitTime: unitTime, measureNumber: measure.number ?? "", staff1: notes1, staff2: notes2)
    }
    
    // 播放对应小节
    func beginToPlay(measureIndex: Int, progressCallback: @escaping ((Int)->Void), endCallback: @escaping ((String) -> Void)) {
        // 开始播放
        self.measureIndex = measureIndex
        noteIndex = 0
        
        if measureIndex == 0 {
            noteList[0].staff1.insert([], at: 0) // 最前面插入一个空音
            noteList[0].staff2.insert([], at: 0)
        }

        gcdTimerVailed = true
        if ti != noteList[measureIndex].unitTime { // 重新设置 ti 会直接触发一次 tickDown ?
            ti = noteList[measureIndex].unitTime
        } else {
            
        }
        
        if measureIndex > 0 {
            // 上一个小节最后一个 tickDown return 了, 新的小节需要立刻 tickDown 一次
            tickDown()
        }
        
        if part?.id == "P1" {
            self.endCallback = endCallback
            self.progressCallback = progressCallback
        }
       
        
        
    }
    
    // 用于播放的数据
    struct MeasureNotesData {
        var unitTime: Double
        var measureNumber: String?
        var staff1: [[String]] = []
        var staff2: [[String]] = []
    }
}
