//
//  ScorePartwise.swift
//
//
//  Created by mengyun2 on 2024/10/27.
//  Copyright © 2024 miao. All rights reserved.
//

import Foundation



// 目前只解析 scorewise 类型的， 另外一种类型是 timewise， 暂不考虑
struct ScorePartWise {
    var partList: [ScorePart] = []
    var parts: [Part] = []
}

// 乐器列表
struct ScorePart {  // (分谱)乐器
    var id: String?  // 编号 P1 P2 P3
    var partName: String? // 小提琴、钢琴等
    var partAbbreviation: String? // name 缩写 Pno.
}

// 一个乐器对应的所有小节
struct Part {
    var id: String? // 编号 P1 P2 P3 和 ScorePart 对应
    var measures: [Measure] = []
}

// 一个小节，包含多个音符
struct Measure {
    var number: String?
    var datas: [MeasureContentData] = []
}

struct MeasureContentData {
    var name: String? // 可能是 barline print attributes direction note backup forward， 不同 name 又不同的类型
    var print: PPrint?
    var attributes: Attributes?
    var barline: BarLine?
    var direction: Direction?
    var note: Note?
    var backup_duration: String?
    var forward_duration: String?
}

struct BarLine{
    var repeat_direction: String? // [forward, backward] 之间的小节内容重复
}

// layout信息用于五线谱绘制，暂不需要
struct PPrint{
    var content: String?
}

struct Attributes{
    // 全音符=4拍、二分音符=2拍、四分音符=1拍、八分音符=半拍、十六分音符=四分之一拍
    var divisions: String? // 小节中的一个四分音符的标准长度，和 duration 对应
}

// 位置信息，用于五线谱绘制
struct Direction{
    var staff: String?  // 所属行
    var tempo: String? // 一分钟 tempo 拍
}

// 一个音符
struct Note {
    var dynamics: String? // 弹奏力度，对应声音大小
    var pitch: Pitch?
    var duration: String? // 例如 divisions为120，当前 note 的音符的 duration 为 30，这就表明，当前这个音的演奏时间为标准时间的 1/4
    var type: String? // eighth, 16th表示1/8音符 1/16音符
    var stem: String?
    var staff: String?
    var rest: String? // 休止符，表示停顿
    var chord: String? // 和旋标记，表示当前 note 和上一个 note 要合并在一起
    var dot: String? // 附点音符, 对应音的时长延长一半， 1/4 = 1/4 + 1/8
    var accidental: String?  // 和alter对应，仅仅做显示用，音调高低，取值：sharp（对应#）、natural（还原，之前升调或降调，还原为不升（降）的调）、flat（对应b）
    var voice: String? // 声音是一系列在时间上线性进行的音乐事件（如音符、和弦、休止符）。语音元素用于区分各个部分中的多个语音（MuseData称之为曲目）。由于它在MusicXML模式中的多种用途，它是在组内定义的。
    var tie_type: String?   // 连音线标记，一般为 start\continue 和 stop，例如要表示3/4的G4，则可以用 1/2的G4（start）和1/4的G4（stop）
    var beam: String?  // 连音线，用作显示
}

// 一个音, 如 G4、B2、 C4# 等
struct Pitch {
    var step: String? // CDEFGAB
    var octave: String?  //  1-8度
    var alter: String? // 等于1时表示升降调， 对应右上角的黑键
    
    func desc() -> String {
        return " \(octave ?? "")\(step ?? "")\(alter ?? "")"
    }
    
    // D5m -> 121
    func format() -> String {
        if let step = step,
           let octave = Int(octave ?? "0") {
            var r = "\(octave - 4)\(noteMap[step]!)0"
            if let alter = Int(alter ?? "0"), alter != 0 {
                let index = abc.firstIndex { k in
                    k == r
                }
                if let i = index {
                    r = abc[i + alter]
                }
            }
            return r
        }
        return ""
    }
}

let noteMap = [
    "C": 1,
    "D": 2,
    "E": 3,
    "F": 4,
    "G": 5,
    "A": 6,
    "B": 7,
]

let abc = [                                                                        "-460", "-471", "-470",
           "-310", "-311", "-320", "-321", "-330", "-340", "-341", "-350", "-351", "-360", "-361", "-370",
           "-210", "-211", "-220", "-221", "-230", "-240", "-241", "-250", "-251", "-260", "-261", "-270",
           "-110", "-111", "-120", "-121", "-130", "-140", "-141", "-150", "-151", "-160", "-161", "-170",
            "010",  "011",  "020",  "021",  "030",  "040",  "041",  "050",  "051",  "060",  "061",  "070",
            "110",  "111",  "120",  "121",  "130",  "140",  "141",  "150",  "151",  "160",  "161",  "170",
            "210",  "211",  "220",  "221",  "230",  "240",  "241",  "250",  "251",  "260",  "261",  "270",
            "310",  "311",  "320",  "321",  "330",  "340",  "341",  "350",  "351",  "360",  "361",  "370",
            "410", ]

//extension ScorePartWise {
//    
//    func _gcd(_ m: Int, _ n: Int) -> Int {
//      var a = 0
//      var b = max(m, n)
//      var r = min(m, n)
//
//      while r != 0 {
//        a = b
//        b = r
//        r = a % b
//      }
//      return b
//    }
//    
////    func printSelf() {
////        for p in partList {
////            print("__partList_id \(p.id ?? "") _partName \(p.partName ?? "") \(p.partAbbreviation ?? "")")
////        }
////        
////        var tempo: Double = 0 // 一分钟多少拍
////        var divisions: Double = 0 // 一个四分音符长度（四分音符=1拍）
////        for p in parts {
////            print("____parts_id \(p.id ?? "") _measures.count  \(p.measures.count) ")
////            for m in p.measures {
////                print("______measures_number \(m.number ?? "")  _datas.count \(m.datas.count)")
////                var t = 0
////                var gcd: Int?
////                var firstNode = true
////                for c in m.datas {
////                    var duration: Int?
////                    if c.name == "attributes" {
////                        let a = divisions
////                        divisions = Double(c.attributes?.divisions ?? "0") ?? 0 // 一个四分音符长度（四分音符=1拍）
////                        if divisions == 0 {
////                            divisions = a
////                        }
////                        print("________content_name_duration 一个四分音符长度 \(c.attributes?.divisions ?? "")")
////                    } else if c.name == "direction" {
////                        let a = tempo
////                        tempo = Double(c.direction?.tempo ?? "0") ?? 0 // 一分钟多少拍
////                        if tempo == 0 {
////                            tempo = a
////                        }
////                        // print("________content_name_direction 一分钟 \(c.direction?.tempo ?? "") 拍， 一拍 \(60.0/(Double(c.direction?.tempo ?? "1") ?? 1)) s 等分成\(divisions) 个duration")
////                    } else if c.name == "note" {
////                        if firstNode {
////                            firstNode = false
////                            let secondPreBeat = 60.0/tempo // 一拍多少秒
////                            print("________content_name_direction2 一分钟 \(tempo) 拍， 一拍 \(secondPreBeat)s 等分成 \(divisions) 个duration")
////                        }
////                        duration = Int(c.note?.duration ?? "")
////                        if c.note?.chord == nil, let d = duration {
////                            t += d
////                        }
////                        print("________content_name \(c.name ?? "") _staff \(c.note?.staff ?? "")  \(c.note?.pitch?.desc() ?? "") |dot\(c.note?.dot ?? "0")| _duration \(c.note?.duration ?? "") _t \(t)")
////                    } else if c.name == "backup" {
////                        // let duration = Int(c.note?.duration ?? "") ?? 0
////                        duration = Int(c.backup_duration ?? "")
////                        t -= duration ?? 0
////                        print("________content_name \(c.name ?? "") _duration \(c.backup_duration ?? "") _t \(t)")
////                    } else if c.name == "forward" {
////                        // let duration = Int(c.note?.duration ?? "") ?? 0
////                        duration = Int(c.forward_duration ?? "")
////                        t += duration ?? 0
////                        print("________content_name \(c.name ?? "") _duration \(c.forward_duration ?? "") _t \(t)")
////                    }
////                    
////                    if gcd == nil {
////                        gcd = duration
////                    } else if let d = duration, let g = gcd {
////                        gcd = _gcd(d, g)
////                    }
////                    
////                }
////                print("______measures_end_with_t \(t) \(gcd ?? 0)")
////            }
////        }
////        
////    }
//}

/*
 https://www.bilibili.com/read/cv5771332/
 https://usermanuals.musicxml.com/MusicXML/Content/EL-MusicXML.htm
 https://www.himachi.cn/2018/10/18/music-xml.html
 https://www.w3.org/2021/06/musicxml40/musicxml-reference
 https://www.w3.org/2021/06/musicxml40/musicxml-reference/examples/
 
 https://musicxmlhub.com/main
 */
/*
标签含义说明：
<part-list>------------------乐谱中对应声部（乐器），用id标识
<part>-----------------------对应声部（乐器）乐谱，通过id与part-list中声部对应
<measure>--------------------小节标识，通过number属性标识是第几个小节
    <attritute>--------------小节共同遵守的一些规则
        <divisions>----------一个四分音符标准时长
        <key>----------------调号
        <time>---------------拍号，一个小节为几几拍
        <staves>-------------五线谱数，可以理解为有几个音轨（进程）
        <clef>---------------谱号，G2为高音谱号，F4为低音谱号
---------------------------------------------------------------------------------------------------------------------------
音符相关：
    <note>------------------------------音符标识
        <pitch>-------------------------音阶信息
            <step>----------------------音阶（abcdefg）
            <octave>--------------------位于哪个8度，（可借助第一部分内容钢琴音阶划分内容理解）
              <alter>---------------------表示升高音调（数值为正表示升高音调对应#，数值为负表示降低音调对应b）
        <duration>----------------------表示持续时长，与divisions结合理解
        <voice>-----------------------
        <type>--------------------------音符类型（whole：全音符、half：二分音符、quarter：四分音符、eighth：8分音符、16th：16分音符）
        <accidental>--------------------音调高低，取值：sharp（对应#）、natural（还原，之前升调或降调，还原为不升（降）的调）、flat（对应b）
        <stem>--------------------------符干方向，down（朝下）、up（朝上）
        <staff>-------------------------表示当前音符位于哪个五线谱上（高音谱号or低音谱号）
        </dot>--------------------------附点
        <beam>--------------------------连杆标签（标签内容为begin-end）根据number确定一个连杆
        <rest>--------------------------休止符
        <notations>---------------------音符的一些额外信息
            <tied>----------------------延音符，type=start（stop）根据number确定一个延音线
            <slur>----------------------连音线，type=start(stop)根据number确定一个连音线
            <arpeggiate/>---------------连滑
---------------------------------------------------------------------------------------------------------------------------
速度情绪相关：
<direction>
    <direction-type>
        <metronome>-------------------------节拍器
            <beat-unit>---------------------一拍单元，即哪个音符时长对应一拍
            <per-minute>--------------------节拍是以一分钟多少拍定义的
    <sound tempo="133.00"/>--------------    一分钟133拍
---------------------------------------------------------------------------------------------------------------------------
声音渐弱渐强相关：
<direction>
    <direction-type>
        <wedge>-------------------------------标识声音强弱，type="diminuendo"（声音渐弱)或者type="crescendo"(声音渐强)type=stop（声音渐弱（强）停止）type="diminuendo"（"crescendo"）与 type=stop搭配使用
                                           
*/
