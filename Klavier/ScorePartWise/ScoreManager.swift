//
//  ScoreManager.swift
//  Klavier
//
//  Created by mengyun2 on 2024/10/29.
//  Copyright © 2024 mengyun. All rights reserved.
//

import Foundation
import SWXMLHash


// 一个 score 代表一个完整的谱，看你包含多个 part
class ScoreManager {
    var file: String
    var score: ScorePartWise?
    
    var tempo: Double = 0 // 一分钟多少拍
    var divisions: Double = 0 // 一个四分音符长度（四分音符=1拍）
    var measureIndex = 0  // 第几个小节了
    var noteIndex = 0  // 第几个note了
    
    var partMap: [String: PartManager] = [String: PartManager] ()  // pid -> [Measure]
    // var switchs = [Int]() // 第 index 个 part 是否播放
    var loop = true
    
    init(file: String) {
        self.file = file
        self.score = self.parseXml(fileName: file)
        if let parts = self.score?.parts {
            for part in parts {
                let partId = part.id!
                let pm = PartManager(part: part, divisions: divisions, tempo: tempo)
                pm.player = getPlayer(partId: partId)
                if part.id == "P1" {
                    if divisions == 0, pm.divisions > 0 {
                        divisions = pm.divisions
                    }
                    if tempo == 0, pm.tempo > 0 {
                        tempo = pm.tempo
                    }
                }
                partMap[partId] = pm
            }
        }
    }
    
    func getPlayer(partId: String?) ->SoundBankPlayer? {
        guard let partList = score?.partList else {
            return nil
        }
        for part in partList {
            if part.id == partId {
                let partName = part.partName ?? ""
                let partAbbreviation = part.partAbbreviation ?? ""
                if partName.isPiano() || partAbbreviation.isPiano() {
                    return SoundBankPlayer.sharedPlayer(withID: 0)
                } else if partName.isGuitar() || partAbbreviation.isGuitar() {
                    return SoundBankPlayer.sharedPlayer(withID: 4)
                } else if partName.isBass() || partAbbreviation.isBass() {
                    return SoundBankPlayer.sharedPlayer(withID: 3)
                }  else if partName.isHarp() || partAbbreviation.isHarp() {
                    return SoundBankPlayer.sharedPlayer(withID: 5)
                } else if partName.isMarimba() || partAbbreviation.isMarimba() {
                    return SoundBankPlayer.sharedPlayer(withID: 5)
                }
            }
        }
        return SoundBankPlayer.sharedPlayer(withID: 0)
    }
    
    func swtSwitchs(index: Int, value: Int) {
        let key = "P\(index+1)"
        if partMap[key] != nil {
            partMap[key]!.isMute = 1 - partMap[key]!.isMute
        }
    }
    
    // 解析 xml 文件
    private func parseXml(fileName: String, ofType: String = "musicxml") -> ScorePartWise {
        var _spw = ScorePartWise()
        if let xmlPath = Bundle.main.path(forResource: fileName, ofType: ofType),
           let data = try? Data(contentsOf: URL(fileURLWithPath: xmlPath)) {
            let xml = XMLHash.config {
                          config in
                          // set any config options here
                      }.parse(data)
            //print(xml)
            
            let scorePartwise = xml["score-partwise"]
            let partList = scorePartwise["part-list"]
            
            var _spList: [ScorePart] = []
            for i in 0..<partList.children.count {
                var _sp = ScorePart()
                _sp.partName = partList.children[i]["part-name"].element?.text ?? ""
                _sp.partAbbreviation = partList.children[i]["part-abbreviation"].element?.text ?? ""
                _sp.id = partList.children[i].element?.allAttributes["id"]?.text
                _spList.append(_sp)
            }
            _spw.partList = _spList
            
            let parts = scorePartwise.filterChildren { elem, index in
                return elem.name == "part"
            }
            
            var _parts: [Part] = []
            for j in 0..<parts.children.count {
                var _part = Part()
                _part.id = parts.children[j].element?.allAttributes["id"]?.text
                var _measures: [Measure] = []
                let measures = parts.children[j].filterChildren { elem, index in
                    return elem.name == "measure"
                }
                for i in 0..<measures.children.count {
                    let measure = measures.children[i]
                    var _measure = Measure()
                    _measure.number = measure.element?.allAttributes["number"]?.text
                    var _contentDatas: [MeasureContentData] = []
                    for k in 0..<measure.children.count {
                        var _contentData = MeasureContentData()
                        let content = measure.children[k]
                        _contentData.name = content.element?.name
                        if _contentData.name == "note" {
                            var _note = Note()
                            _note.dynamics = content.element?.allAttributes["dynamics"]?.text
                            _note.duration = content["duration"].element?.text
                            _note.stem = content["stem"].element?.text
                            _note.type = content["type"].element?.text
                            _note.voice = content["voice"].element?.text
                            _note.staff = content["staff"].element?.text
                            _note.rest = content["rest"].element?.text
                            _note.chord = content["chord"].element?.text
                            _note.dot = content["dot"].element?.text
                            _note.accidental = content["accidental"].element?.text
                            
//                            if content["accidental"].element != nil {
//                                print("_______accidental\(content["accidental"].element)")
//                            } else {
//                                
//                            }
                            
                            _note.tie_type = content["tie"].element?.allAttributes["type"]?.text
                            if content["tie"].all.count == 1  {
                                _note.tie_type = content["tie"].all.first?.element?.allAttributes["type"]?.text
                            } else if content["tie"].all.count > 1 {
                                _note.tie_type = "continue"
                            }
                            
                            if content["pitch"].children.count > 0 {
                                var _pitch = Pitch()
                                _pitch.step = content["pitch"]["step"].element?.text
                                _pitch.alter = content["pitch"]["alter"].element?.text
                                _pitch.octave = content["pitch"]["octave"].element?.text
                                _note.pitch = _pitch
                            } else {
                                // print("no pitch")
                            }
                            
                            _contentData.note = _note
                        } else if _contentData.name == "print" {
                            var _print = PPrint()
                            _print.content = content.description
                            _contentData.print = _print
                        } else if _contentData.name == "attributes" {
                            var _attributes = Attributes()
                            _attributes.divisions = content["divisions"].element?.text
                            _contentData.attributes = _attributes
                        } else if _contentData.name == "direction" {
                            var _direction = Direction()
                            _direction.staff = content["staff"].element?.text
                            _direction.tempo = content["sound"].element?.allAttributes["tempo"]?.text
                            _contentData.direction = _direction
                        } else if _contentData.name == "forward" {
                            _contentData.forward_duration = content["duration"].element?.text
                        } else if _contentData.name == "backup" {
                            _contentData.backup_duration = content["duration"].element?.text
                        } else if _contentData.name == "barline" {
                            var _barline = BarLine()
                            _barline.repeat_direction = content["repeat"].element?.allAttributes["direction"]?.text
                            _contentData.barline = _barline
                        }
                        _contentDatas.append(_contentData)
                    }
                    _measure.datas = _contentDatas
                    _measures.append(_measure)
                    
                }
                _part.measures = _measures
                _parts.append(_part)
            }
            _spw.parts = _parts
        } else {
            print("打开 |\(fileName)|\(ofType)| 失败")
        }
        
        return _spw
    }
    
    func setStartMeasureNumber(measureNumber: String){
        for (k, pm) in partMap {
            for i in 0..<pm.noteList.count {
                if pm.noteList[i].measureNumber == measureNumber {
                    measureIndex = i
                    noteIndex = 0
                    return
                }
            }
        }
    }
    
    func tryPlay(callback: @escaping ((String) -> Void)) {
        
        print("开始播放第 \(measureIndex) 小节 \(partMap["P1"]?.noteList[measureIndex].staff1.count)")
        callback(partMap["P1"]?.noteList[measureIndex].measureNumber ?? "")
        var count = 0
        for (partId, partM) in partMap {
            partM.beginToPlay(measureIndex: measureIndex) { [weak self] noteIndex in
                guard let self = self else {
                    return
                }
                self.noteIndex = noteIndex
                callback(self.partMap["P1"]?.noteList[self.measureIndex].measureNumber ?? "")
                
            } endCallback: {  [weak self] parId in
                guard let self = self else {
                    return
                }
                count += 1
                if count == self.score?.partList.count || count >= 1 {
                    partM.gcdTimerVailed = false
                    self.measureIndex += 1
                    self.noteIndex = 0
                    if self.measureIndex >=  partM.noteList.count {
                        print("播放结束！！")
                        partM.gcdTimerVailed = false
                        callback("end")
                        if self.loop {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 10) { // 延迟10s，重新播放
                                if !self.loop {
                                    return
                                }
                                self.measureIndex = 0
                                partM.gcdTimerVailed = true
                                self.tryPlay(callback: callback)
                                callback(partM.noteList[0].measureNumber ?? "")
                            }
                        }
                        
                    } else {
                        self.tryPlay(callback: callback)
                        callback(partM.noteList[self.measureIndex].measureNumber ?? "")

                    }
//                    callback(partMap["P1"]?.noteList[measureIndex].measureNumber ?? "", Double(noteIndex)/Double(self.partMap["P1"]?.noteList.count ?? 1))
                }
            }

        }
        
    }
    
    deinit {
        for (_, v) in partMap {
            v.gcdTimerVailed = false
            v.gcdTimer = nil
        }
        score = nil
        
    }
    
    
}





extension String {
    
    // 钢琴
    func isPiano() -> Bool {
        return contains("Piano") || contains("piano") || contains("Pno") || contains("pno") || contains("Cel.")
    }
    
    // 吉他
    func isGuitar() -> Bool {
        return contains("Guitar") || contains("guitar") || contains("Guit") || contains("guit")
    }
    
    // 巴斯
    func isBass() -> Bool {
        return contains("Bass") || contains("bass") || contains("El. B.")
    }
    
    // 竖琴
    func isHarp() -> Bool {
        return contains("Harp") || contains("harp") || contains("Hrp.")
    }
    
    // 木琴
    func isMarimba() -> Bool {
        return contains("Marimba") || contains("marimba") || contains("Dlc.")
    }
    
}
