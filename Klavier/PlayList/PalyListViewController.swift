//
//  PalyListViewController.swift
//  PianoPlayer
//
//  Created by mengyun on 2017/10/15.
//  Copyright © 2017年 mengyun. All rights reserved.
//

import UIKit

class PalyListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate, UIGestureRecognizerDelegate {
    
    //var listView=UIView()
    var searchBar: UISearchBar!
    var isKeyboardVisible = false;
    var keyText = ""{
        didSet{
            searchActive = keyText.count>0
            var notes = [Notd]()
            if (searchActive){
                notes = udata.list.filter { (item) -> Bool in
                    let fid = item.file
                    let name = item.name
                    let author = item.author
                    let key = transformToPinyin(hanzi: keyText.lowercased())
                    var isContains = false;
                    if fid=="" {
                    }
                    else{
                        let fidStr = fid.lowercased()
                        if(fidStr.contains(key)){
                            isContains = true;
                        }
                    }
                    
                    if name=="" {
                    }
                    else{
                        let nameStr = transformToPinyin(hanzi: name)
                        //return nameStr.contains(key);
                        if(nameStr.contains(key) || name.lowercased().contains(key) ){
                            isContains = true;
                        }
                    }
                    
                    if author=="" {
                    }
                    else{
                        let authorStr = transformToPinyin(hanzi: author)
                        //return authorStr.contains(key);
                        if(authorStr.contains(key) || author.lowercased().contains(key)){
                            isContains = true;
                        }
                    }
                    //print("\(key) \(fid) \(nameStr) \(authorStr)")
                    return isContains
                }
            }
            else{
                notes = udata.list
            }
            list = notes.sorted{
                (f,n)-> Bool in
                let file = f.file
                if (udata.likedArr.contains(file as NSString)){
                    return true
                }
                return false
            }
            self.mListTableView.reloadData();
        }
    }
    var searchActive = false
    
    var list = [Notd]()
    var mListTableView=UITableView()
    //var mListHead = UILabel()
    var searchButton = UIButton()
    
    var currentPlayingFid = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        //getDataList(type: .CLASS_ALL)

        self.updateList()
        _ = view.frame.width
        // let btnWidth:CGFloat = 48
        let h = view.frame.height
        // view.backgroundColor = UIColor.red
        let headHight:CGFloat = 44

        self.searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: tableViewWidth, height: headHight))
        self.searchBar.delegate = self
        self.searchBar.placeholder = getInternationalizedStrings(from: "search");
        view.addSubview(searchBar)
        
        mListTableView.frame = CGRect(x: 0, y: headHight, width: tableViewWidth, height: h-headHight);
        view.addSubview(mListTableView)
        mListTableView.dataSource = self
        mListTableView.delegate = self
        
//        searchButton.frame = CGRect(x: tableViewWidth-headHight, y: 0, width: headHight, height: headHight);
//        searchButton.addTarget(self, action: #selector(doSearch), for:.touchUpInside)
//        // searchButton.setTitle("OK", for: .normal)
//        searchButton.setImage(UIImage(named: "search"), for: .normal)
//        searchButton.backgroundColor = .noSelectColor()
//        searchButton.alpha = 0.9
//        view.addSubview(searchButton)
        
        view.backgroundColor = UIColor.darkGray
        mListTableView.separatorStyle = .none;
    }
    
    func updateList(){
        let path = Bundle.main.path(forResource: "ok_list", ofType: "json");
        let url = URL(fileURLWithPath: path!)
        do{
            let arrayJSONString = try! String(contentsOf: url)
            udata.serverData = arrayJSONString;
            if let notes = [Notd].deserialize(from: arrayJSONString), notes.count>0 {
                udata.serverList = notes as! [Notd];
            }
            udata.version = 1;//Int(serverVersionNumber)
            udata.refreshStatus()
            list = udata.list.sorted{
                (f,n)-> Bool in
                let file = f.file
                if (udata.likedArr.contains(file as NSString)){
                    return true
                }
                return false
            }
            self.mListTableView.reloadData()
        }
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //self.mListTableView .reloadData();
        self.keyText = searchText;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.dissmissKeyBoard()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    //table的cell的高度
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseID = "CELLID\(indexPath.row)"
        var cell = tableView.dequeueReusableCell(withIdentifier: reuseID) as? MYPlayListCell
        if (cell == nil) {
            cell = MYPlayListCell(style: .subtitle, reuseIdentifier: reuseID)
        }
        let index = indexPath.row
        cell?.index = index
        let note = list[index]

        cell?.title = note.name
        cell?.author = note.author
        cell?.fid = note.file
        
        let isCached = note.iscached
        cell?.isDownloaded = (isCached == "yes" || isCached == "reserved")
        var isliked = false;//(note.isliked as NSString).boolValue
        if (udata.likedArr.contains(note.file as NSString)){
            isliked = true
        }
        cell?.isOnPlay = cell?.fid == currentPlayingFid
        cell?.isLiked = isliked
        return cell!
    }
    
    @objc func doSearch() {
        if (isKeyboardVisible){
            self.dissmissKeyBoard()
        }
        else{
            self.showKeyBoard()
        }
    }
    
    @objc func showMenueMiddle() {
        drawerVC.currentType = .middleMenu;
        self.dissmissKeyBoard()
    }

    func dissmissKeyBoard(){
        view.endEditing(true);
        isKeyboardVisible = false
    }
    
    func showKeyBoard(){
        searchBar.resignFirstResponder()
        isKeyboardVisible = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
