//
//  MYPlayListCell.swift
//  PianoPlayer
//
//  Created by mengyun on 2017/10/16.
//  Copyright © 2017年 mengyun. All rights reserved.
//

import UIKit

class MYPlayListCell: UITableViewCell,callBackDelegate {
    
    let request = HttpRequest()
    let playButton = UIButton()
    let indexLabel = UILabel()
    let titleLabel = UILabel()
    let authorLabel = UILabel()
    let ifLikeButton = UIButton()
//    let likedNumLabel = UILabel()
    let downloadState = UIImageView()
    let downloadButton = UIButton()
    let separatorView = UIView()
    var isOnPlay = false{
        didSet{
            if (isOnPlay){
                authorLabel.textColor = .lightBlueColor()
                titleLabel.textColor = .lightBlueColor()
                indexLabel.textColor = .lightBlueColor()
            }
            else{
                authorLabel.textColor = UIColor.detailColor()
                titleLabel.textColor = UIColor.black
                indexLabel.textColor = UIColor.black
            }
        }
    }
    //let vc: PalyListViewController?
    
    var isDownloaded = false{
        didSet{
            if isDownloaded{
                downloadState.image = #imageLiteral(resourceName: "Downloaded")
            }
            else{
                downloadState.image = #imageLiteral(resourceName: "Notdownloaded")
            }
        }
    }
    var isLiked = false
    {
        didSet{
            if isLiked{
                ifLikeButton.setImage(UIImage(named: "liked"), for: .normal)
            }
            else{
                ifLikeButton.setImage(UIImage(named: "like"), for: .normal)
            }
        }
    }
//    var numOfLiked = 0{
//        didSet{
//            if numOfLiked>999 {
//                likedNumLabel.text = "(999+)"
//            }
//            else{
//                likedNumLabel.text = "(\(numOfLiked))"
//            }
//        }
//    }
    var index = 1{
        didSet{
            indexLabel.text = stringWithIndex(i: index) //String(index)
        }
    }
    
    var title = ""{
        didSet{
            titleLabel.text = title
        }
    }
    
    var author = ""{
        didSet{
            authorLabel.text = author
        }
    }
    
    var fid = "";

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        indexLabel.textAlignment = .center
        indexLabel.numberOfLines = 3;
        indexLabel.backgroundColor = UIColor.selectColor()
        indexLabel.font = UIFont.systemFont(ofSize: 9)
        playButton.addTarget(self, action:#selector(clickBtnAction(sender:)), for:.touchUpInside)
        ifLikeButton.addTarget(self, action:#selector(likeOrUnlike(sender:)), for:.touchUpInside)
        contentView.addSubview(indexLabel)
        contentView.addSubview(titleLabel)
        contentView.addSubview(authorLabel)
        contentView.addSubview(playButton)
        contentView.addSubview(ifLikeButton)
        ifLikeButton.isHidden = true// 且先去掉喜欢按钮
        contentView.addSubview(downloadState)
        
        //playButton.alpha = 0.2
        self.adjustLayers()
        titleLabel.font = UIFont.systemFont(ofSize: 15)
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.minimumScaleFactor = 0.6
        authorLabel.font = UIFont.systemFont(ofSize: 11)
        authorLabel.textColor = UIColor.detailColor();
        self.isDownloaded = false
        //numOfLiked = 0
        downloadState.image = #imageLiteral(resourceName: "Notdownloaded")
        
        separatorView.backgroundColor = UIColor.cellLineColor()
        contentView.addSubview(separatorView)
        request.delegate = self;
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews();
        if isIphoneX {
            separatorView.frame = CGRect(x: 18, y: bounds.size.height-1, width: bounds.size.width-20, height: 1)
        }
        else{
            separatorView.frame = CGRect(x: 18, y: bounds.size.height-1, width: bounds.size.width-20, height: 1)
        }
        
    }
    
    func stringWithIndex(i: Int)-> String{
        let n1 = i%10;
        let n2 = (i/10)%10;
        let n3 = (i/100)%10;
        return "\(n3)\n\(n2)\n\(n1)";
    }
    
    //添加约束
    func adjustLayers(){
        indexLabel.snp.makeConstraints{(make) -> Void in
            make.top.bottom.left.equalToSuperview()
            make.width.equalTo(18)
        }
        playButton.snp.makeConstraints{(make) -> Void in
            make.left.top.bottom.equalTo(contentView)
            make.right.equalTo(ifLikeButton.snp.left)
        }
        
        titleLabel.snp.makeConstraints{(make) -> Void in
            make.top.equalToSuperview().offset(8)
            make.left.equalTo(indexLabel.snp.right).offset(4)
            make.right.equalTo(ifLikeButton.snp.left)
            make.height.equalTo(18)
        }
        downloadState.snp.makeConstraints{(make) -> Void in
            make.left.equalTo(indexLabel.snp.right).offset(4)
            make.top.equalTo(titleLabel.snp.bottom).offset(4)
            make.height.width.equalTo(12)
        }
        authorLabel.snp.makeConstraints{(make) -> Void in
            make.bottom.equalToSuperview().offset(-6)
            make.centerY.equalTo(downloadState)
            make.left.equalTo(downloadState.snp.right).offset(2)
            make.right.equalTo(ifLikeButton.snp.left)
        }
        
        ifLikeButton.snp.makeConstraints{(make) -> Void in
            make.right.equalToSuperview().offset(-4)
            make.centerY.equalTo(titleLabel)
            make.height.width.equalTo(20)
        }
//        likedNumLabel.snp.makeConstraints{(make) -> Void in
//            make.right.equalTo(ifLikeButton.snp.left).offset(-2)
//            make.centerY.equalTo(ifLikeButton)
//            make.height.equalTo(20)
//            //make.width.equalTo(24)
//        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    @objc func clickBtnAction(sender:UIButton) {
        pianoVC.startAnimation()
        pianoVC.isPause = true;
        // pianoVC.pList = udata.uList
        drawerVC.currentType = .middleMenu
        pianoVC.uploadList = nil
        let curIndex = self.getIndex()
        pianoVC.playList = [curIndex]//[index]
        pianoVC.mIndex = curIndex
        pianoVC.loadButtonIsHidden = true
        pianoVC.musicNameLabel.text = pianoVC.list[curIndex].name
        let _ = request.addXmlData(data: &(pianoVC.list[curIndex]))
        leftVC.dissmissKeyBoard();
    }
    
    func getIndex() -> Int{
        for (index, value) in pianoVC.list.enumerated() {
            let tmp = value.file as String
            if (tmp == fid){
                return index;
            }
        }
        return 0;
    }
    
    func callbackDelegatefuc(backMsg: String) {
        drawerVC.stopAnimation();
        pianoVC.playIndex = 0  // 触发播放
        pianoVC.isPause = false
    }
    @objc func likeOrUnlike(sender:UIButton) {
        if isLiked{
            isLiked=false;
            udata.likedArr.remove(fid as NSString)
        }
        else{
            isLiked=true;
            udata.likedArr.insert(fid as NSString)
        }
        // request.likeOrCancel(fid: fid, liked: isLiked);
        udata.refreshStatus()
        saveUData();
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
